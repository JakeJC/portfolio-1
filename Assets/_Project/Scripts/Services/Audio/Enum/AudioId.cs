namespace _Project.Scripts.Services.Audio.Enum
{
public enum AudioId
{
	Default,
	Karaoke_Button,
	Karaoke_Buy,
	Karaoke_Lose,
	Karaoke_Message,
	Karaoke_OST_Girls,
	Karaoke_OST_Novel,
	Karaoke_ProgressBar,
	Karaoke_Win,
}
}
