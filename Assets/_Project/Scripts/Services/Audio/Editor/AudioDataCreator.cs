using System.Collections.Generic;
using System.IO;
using System.Linq;
using _Project.Scripts.Services.Audio.Data;
using _Project.Scripts.Services.Audio.Enum;
using UnityEditor;
using UnityEngine;
using AudioType = _Project.Scripts.Services.Audio.Enum.AudioType;

namespace _Project.Scripts.Services.Audio.Editor
{
  public class AudioDataCreator : AssetPostprocessor
  {
    private const string EnumPath = "Assets/_Project/Scripts/Services/Audio/Enum/";
    private const string EnumNamespace = "_Project.Scripts.Services.Audio.Enum";
    private const string EnumName = "AudioId";
    private const string AudioFilesPath = "_Project/Sounds";
    private const string AudioPrefabsPath = "_Project/Resources/Sounds";

    [MenuItem("Tools/Audio Data Creator/Generate Enum")]
    public static void GenerateEnum()
    {
      string[] enumEntries = GetEnumNames();
      string filePathAndName = EnumPath
        .Replace('/', Path.DirectorySeparatorChar) + EnumName + ".cs";

      if (File.Exists(filePathAndName))
      {
        Debug.Log("Exist");
        File.Delete(filePathAndName);
      }

      using (StreamWriter streamWriter = new StreamWriter(filePathAndName))
      {
        streamWriter.WriteLine("namespace " + EnumNamespace);
        streamWriter.WriteLine("{");
        streamWriter.WriteLine("public enum " + EnumName);
        streamWriter.WriteLine("{");

        for (int i = 0; i < enumEntries.Length; i++)
          streamWriter.WriteLine("\t" + enumEntries[i] + ",");

        streamWriter.WriteLine("}");
        streamWriter.WriteLine("}");
      }

      AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
      Debug.Log("Generate Audio Enum");
    }

    /*[MenuItem("Tools/Audio Data Creator/Generate Prefabs")]
    public static void GeneratePrefabs() =>
      GeneratePrefabs(false);*/

    [MenuItem("Tools/Audio Data Creator/Generate Only New Prefabs")]
    public static void GenerateOnlyNewPrefabs() =>
      GeneratePrefabs(true);

    private static void GeneratePrefabs(bool onlyNew)
    {
      string[] files = ExtractFiles();

      for (int i = 0; i < System.Enum.GetNames(typeof(AudioId)).Length; i++)
        CreateAsset((AudioId)i);

      void CreateAsset(AudioId type)
      {
        if(type == AudioId.Default)
          return;
        
        GameObject prefab = new GameObject(GetAssetName(type));
        prefab.AddComponent<AudioData>();
        prefab.GetComponent<AudioSource>().clip = null;

        string fullPath = GetFullPath(AudioPrefabsPath) + "/" + GetAssetName(type) + ".prefab";
        string localPath = GetLocalPath(AudioPrefabsPath) + "/" + GetAssetName(type) + ".prefab";

        if (File.Exists(fullPath) && onlyNew)
        {
          Debug.Log($"Audio Prefab {GetAssetName(type)} Exist");
          Object.DestroyImmediate(prefab);
          return;
        }
        else
          Debug.Log($"Audio Prefab {GetAssetName(type)} New");

        string file = files.FirstOrDefault(s => Path.GetFileNameWithoutExtension(s) == type.ToString());

        AudioClip clip = (AudioClip)AssetDatabase.LoadAssetAtPath(GetLocalPath(AudioFilesPath) + "/" + file, typeof(AudioClip));
        if (file != null)
        {
          AudioSource audioSource = prefab.GetComponent<AudioSource>();
          audioSource.clip = clip;
          audioSource.playOnAwake = false;

          prefab.GetComponent<AudioData>().Type = AudioType.Sound;
        }

        PrefabUtility.SaveAsPrefabAssetAndConnect(prefab, localPath, InteractionMode.UserAction);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        Object.DestroyImmediate(prefab);
      }

      string[] ExtractFiles()
      {
        List<string> extractedFiles = new List<string>();
        string[] data = Directory.GetFiles(GetFullPath(AudioFilesPath));
        foreach (string path in data)
        {
          if (Path.GetExtension(path) != ".meta")
          {
            string filename = Path.GetFileName(path);
            extractedFiles.Add(filename);
          }
        }

        return extractedFiles.ToArray();
      }
    }

    private static string GetAssetName(AudioId id) =>
      $"Audio-{id.ToString()}";

    private static string[] GetEnumNames()
    {
      string dirPath = GetFullPath(AudioFilesPath);

      IEnumerable<string> filePaths = Directory.GetFiles(dirPath)
        .Where(s => s.EndsWith(".mp3") || s.EndsWith(".wav"));

      string[] names = new string[filePaths.ToArray().Length + 1];

      for (int i = 0; i < names.Length; i++)
      {
        if (i == 0)
        {
          names[i] = "Default";
          continue;
        }

        names[i] = Path.GetFileNameWithoutExtension(filePaths.ToArray()[i - 1]);
      }

      return names;
    }

    private static string GetFullPath(string path) =>
      Application.dataPath + "/" + path;

    private static string GetLocalPath(string path) =>
      $"Assets/{path}";
  }
}