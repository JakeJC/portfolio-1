using System;
using _Project.Scripts.Services.Unlocker;

namespace _Project.Scripts.Services._Interfaces
{
  public interface IMoneyService
  {
    public uint MoneyCount {get;}
    public event Action<uint, float> OnCountChange;
    public void Earn(uint count, float delay = 0);
    public void Spend(uint count, Action onUnlock, Action<UnlockType> onFail = null);
  }
}