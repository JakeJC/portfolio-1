using System.Collections.Generic;
using _Project.Scripts.Features.Data;

namespace _Project.Scripts.Services._Interfaces
{
  public interface IGameMetaProvider
  {
    public string CurrentGirl { get; }
    public void SetCurrentGirl(string guid);
    public List<string> UpgradesInUse { get; }
    public bool IsContentUnlocked(string guid);
    public List<T> GetContent<T>() where T : BaseContentData;
  }
}