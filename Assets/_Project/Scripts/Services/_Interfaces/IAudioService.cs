using _Project.Scripts.Services.Audio.Enum;

namespace _Project.Scripts.Services._Interfaces
{
  public interface IAudioService
  {
    float MusicIsMute();
    float SoundsIsMute();
    void MuteMusic(float isMusicDisabled);
    void MuteSounds(float isSoundDisabled);
    void Play(AudioId audioId);
    void Stop(AudioId audioId);
    void Clean();
  }
}