using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.MiniGamesScreen.Serialized;
using _Project.Scripts.Features.StoriesScreen;
using _Project.Scripts.Features.StoriesScreen.Serialized;
using ModestTree;
using UnityEngine;
using XNode;

namespace _Project.Scripts.Services._data
{
  public class GameSaveData
  {
    public bool IsFirstSession = true;

    public int DisableAdsProgress;
    public bool DisableAdsIsActive;
    
    public bool IsPolicyAccepted;

    public float IsMusicDisabled = 1f;
    public float IsSoundDisabled = 1f;
    public bool IsVibrationDisabled;

    public bool MoneyCountFirstLaunch = true;
    public uint MoneyCount = 0;

    public List<string> ChosenPassiveContent = new List<string>();
    public List<string> UnlockedPassiveContent = new List<string>();

    public List<string> UnlockedStoryContent = new List<string>();

    public List<string> UnlockedPhotoContent = new List<string>();
    public List<string> UnlockedStoriesContent = new List<string>();

    public List<string> UpgradeContentChosenMoreThenOnce = new List<string>();
    public List<string> ChosenUpgradeContent = new List<string>();
    public List<string> UnlockedUpgradeContent = new List<string>();

    public List<string> UnlockedIapContent = new List<string>();
    public List<string> CompletedMiniGamesLevelContent = new List<string>();

    [SerializeField] private List<StoryProgress> _unlockedStoryProgressContent = new List<StoryProgress>();
    [SerializeField] private List<MiniGamesProgressSaver> _miniGamesProgressSaver = new List<MiniGamesProgressSaver>();

    public bool MissClickBubbleWasActivated = false;
    public bool GreatBubbleWasActivated = false;
    
    public bool QuizAssistantHelpIsUsed = false;
    public bool FiftyFiftyHelpIsUsed = false;

    public bool RewardedLockedStory = false;

    public bool SecretarySpeakBubbleUsed = false;

    public void OnSaveStoryProgress(StoryData storyData, Node node)
    {
      StoryProgress storyProgress = GetFirstUnlockedStoryProgress(storyData);

      if (storyProgress != null)
        storyProgress.Index = storyData.DialogGraph.nodes.IndexOf(node);
      else
        _unlockedStoryProgressContent.Add(new StoryProgress(storyData.Id, 0));
    }

    public Node OnLoadStoryProgress(StoryData storyData)
    {
      StoryProgress storyProgress = GetFirstUnlockedStoryProgress(storyData);
      if (storyProgress == null)
        _unlockedStoryProgressContent.Add(storyProgress = new StoryProgress(storyData.Id, 0));

      return storyData.DialogGraph.nodes[storyProgress.Index];
    }

    public void OnSaveMiniGameProgress(MiniGameType type, int index)
    {
      MiniGamesProgressSaver miniGamesProgressSaver = GetFirstUnlockedMiniGameProgress(type);
      if (miniGamesProgressSaver != null)
        miniGamesProgressSaver.Index = index;
      else
        _miniGamesProgressSaver.Add(new MiniGamesProgressSaver(type, index));
    }

    public int OnLoadMiniGameProgress(MiniGameType type)
    {
      MiniGamesProgressSaver miniGamesProgressSaver = GetFirstUnlockedMiniGameProgress(type);

      if (miniGamesProgressSaver == null)
        _miniGamesProgressSaver.Add(miniGamesProgressSaver = new MiniGamesProgressSaver(type, 0));

      return miniGamesProgressSaver.Index;
    }

    private StoryProgress GetFirstUnlockedStoryProgress(StoryData storyData) =>
      _unlockedStoryProgressContent.FirstOrDefault(p => p.Id == storyData.Id);

    private MiniGamesProgressSaver GetFirstUnlockedMiniGameProgress(MiniGameType type) =>
      _miniGamesProgressSaver.FirstOrDefault(p => p.Type == type);
  }
}