﻿using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;
using Newtonsoft.Json;

namespace _Project.Scripts.Services.Localization.Logic
{
  public class LocaleSystem : ILocaleSystem
  {
    private const string ActiveLanguageKey = "ActiveLanguageKey";

    private const string PathMain = "Main";   
    private const string PathQuests = "Quests";
    private const string PathGirl1Story1 = "Girl_1_Story_1";
    private const string PathGirl1Story2 = "Girl_1_Story_2";

    private Dictionary<string, Dictionary<string, Dictionary<string, string>>> _dictionary;

    private string ActiveLanguage
    {
      get => PlayerPrefs.GetString(ActiveLanguageKey, "");
      set => PlayerPrefs.SetString(ActiveLanguageKey, value);
    }

    public LocaleSystem(IAssetProvider assetProvider) =>
      Initialize(assetProvider);

    private void Initialize(IAssetProvider assetProvider)
    {
      string jsonMain = assetProvider.GetResource<TextAsset>(PathMain).text;
      string jsonQuests = assetProvider.GetResource<TextAsset>(PathQuests).text;
      string jsonGirl1Story1 = assetProvider.GetResource<TextAsset>(PathGirl1Story1).text;
      string jsonGirl1Story2 = assetProvider.GetResource<TextAsset>(PathGirl1Story2).text;

      _dictionary = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>
      {
        {PathMain, JsonToDictionary(jsonMain)},
        {PathQuests, JsonToDictionary(jsonQuests)},
        {PathGirl1Story1, JsonToDictionary(jsonGirl1Story1)},
        {PathGirl1Story2, JsonToDictionary(jsonGirl1Story2)},
      };

      FetchLanguage(Application.systemLanguage);
    }

    public void SetLanguage(SystemLanguage language) =>
      FetchLanguage(language);
    
    public string GetText(string name, string key)
    {
      if (string.IsNullOrEmpty(key))
      {
        Debug.LogWarning("Empty Key");
        return "Empty Key";
      }
      
      _dictionary[name][ActiveLanguage].TryGetValue(key, out string text);
      
      if(string.IsNullOrEmpty(text) == false && text.Contains("\\n")) 
        text = text.Replace("\\n", "\n"); 
      
      if(string.IsNullOrEmpty(text) == false && text.Contains("\\n\\n"))
        text = text.Replace("\\n\\n", "\n\n"); 
      
      return text;
    }

    public string GetKey(string name, string text) => 
      _dictionary.ContainsKey(name) ? _dictionary[name]["ru"].First(x => x.Value == text).Key : "";

    private Dictionary<string, Dictionary<string, string>> JsonToDictionary(string json) =>
      JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string>>>(json);

    private void FetchLanguage(SystemLanguage language)
    {
      ActiveLanguage = language switch
      {
        SystemLanguage.English => "en",
        SystemLanguage.Russian => "ru",
        SystemLanguage.Portuguese => "pt",
        SystemLanguage.Spanish => "es",
        SystemLanguage.French => "fr",
        _ => "en"
      };
    }
  }
}