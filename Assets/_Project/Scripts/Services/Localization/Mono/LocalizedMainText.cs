using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using TMPro;
using UnityEngine;

namespace _Project.Scripts.Services.Localization.Mono
{
  [RequireComponent(typeof(TMP_Text))]
  public class LocalizedMainText : MonoBehaviour
  {
    private const string LocalizeName = "Main";
    
    [SerializeField] private string key;
    [SerializeField] private bool makeUppercase;
    
    private ILocaleSystem _localeSystem;

    public void Construct(ILocaleSystem localeSystem)
    {
      _localeSystem = localeSystem;
       Localize();
    }

    public void Localize()
    {
      var textComponent = GetComponentInChildren<TMP_Text>(true);
      
      if (_localeSystem != null && textComponent != null)
      {
        string text = _localeSystem.GetText(LocalizeName, key);
        textComponent.text = makeUppercase? text.ToUpper() : text;
      }
    }
    
    public void SetNewKey(string newKey)
    {
      key = newKey;
      Localize();
    }
  }
}