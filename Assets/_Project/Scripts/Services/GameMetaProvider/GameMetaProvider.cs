using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Services._Interfaces;

namespace _Project.Scripts.Services.GameMetaProvider
{
  public class GameMetaProvider : IGameMetaProvider
  {
    private readonly IAssetProvider _assetProvider;
    public string CurrentGirl { get; private set; }
    public List<string> UpgradesInUse { get; }

    public GameMetaProvider(IAssetProvider assetProvider)
    {
      _assetProvider = assetProvider;
    }
    
    public bool IsContentUnlocked(string guid) => 
      UpgradesInUse.Contains(guid);

    public void SetCurrentGirl(string guid) =>
      CurrentGirl = guid;
    
    public void AddUpgradesInUse(string guid) =>
      UpgradesInUse.Add(guid);

    public List<T> GetContent<T>() where T : BaseContentData => 
      _assetProvider.GetAllResources<T>("").ToList();
  }
}