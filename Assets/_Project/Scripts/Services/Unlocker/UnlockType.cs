namespace _Project.Scripts.Services.Unlocker
{
  public enum UnlockType
  {
    Money,
    Advertising,
    Empty,
    Progress,
    IAP
  }
}
