using System;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Unlocker;
using UnityEngine;
using IService = Common.IService;

namespace _Project.Scripts.Services.Money
{
  public class MoneyService : IMoneyService, IService
  {
    public uint MoneyCount { get; private set; }
    public event Action<uint, float> OnCountChange;

    private readonly IGameSaveSystem _gameSaveSystem;

    public MoneyService(IGameSaveSystem gameSaveSystem)
    {
      _gameSaveSystem = gameSaveSystem;
      MoneyCount = _gameSaveSystem.Get().MoneyCount;
    }

    public void Earn(uint count, float delay = 0)
    {
      CheckedMethod(count);
      SaveMoney(delay);
    }

    public void Spend(uint count, Action onSpend, Action<UnlockType> onFail)
    {
      if (MoneyCount >= count)
      {
        MoneyCount -= count;
        SaveMoney(0);
        onSpend?.Invoke();
      }
      else
      {
        Debug.Log("Money isn't enough");
        onFail?.Invoke(UnlockType.Money);
      }
    }

    private void SaveMoney(float delay)
    {
      OnCountChange?.Invoke(MoneyCount, delay);
      
      _gameSaveSystem.Get().MoneyCount = MoneyCount;
      _gameSaveSystem.Save();
    }

    private void CheckedMethod(uint count)
    {
      try
      {
        MoneyCount = checked(MoneyCount + count);
      }
      catch (OverflowException e)
      {
        Console.WriteLine("CHECKED and CAUGHT:  " + e);
      }
    }
  }
}