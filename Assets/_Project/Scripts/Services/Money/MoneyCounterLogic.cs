using _Project.Scripts.Features;
using _Project.Scripts.Features.IapWindow;
using _Project.Scripts.Features.NotLoadedWindow;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using _Project.Scripts.Services.Money.Mono;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Services.Money
{
  public class MoneyCounterLogic : IMoneyCounterLogic
  {
    private readonly MoneyCounter _moneyCounter;

    private readonly IAdsService _adsService;
    private readonly IRemote _remote;
    private readonly IUnlockService _unlockService;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IAnalytics _analytics;
    private readonly IMoneyService _moneyService;
    private readonly IAudioService _audioService;
    private readonly IUiFactory _uiFactory;

    private readonly IAssetProvider _assetProvider;
    private readonly ILocaleSystem _localeSystem;
    
    private readonly AddMoneyView _addMoneyWindow;
    
#if !DISABLE_INAPPS
    private IapLogic _iapLogic;
#endif

    public MoneyCounterLogic(IAssetProvider assetProvider, IUiFactory uiFactory, ILocaleSystem localeSystem, IMoneyService moneyService,
     IAudioService audioService, IAdsService adsService, IRemote remote, IUnlockService unlockService, IGameSaveSystem gameSaveSystem, IAnalytics analytics)
    {
      _localeSystem = localeSystem;
      _assetProvider = assetProvider;
      _adsService = adsService;
      _remote = remote;
      _unlockService = unlockService;
      _gameSaveSystem = gameSaveSystem;
      _analytics = analytics;
      _moneyService = moneyService;
      _audioService = audioService;

      _uiFactory = uiFactory;
      _moneyCounter = _uiFactory.SpawnMoneyCounter();
      _addMoneyWindow = _uiFactory.SpawnAddMoneyWindow();
      
      _moneyCounter.Construct(remote, assetProvider, localeSystem, moneyService, _addMoneyWindow, OnClickAddMoney, ToIapShop, analytics);

      CreateSounds(_moneyCounter);

      IapDailyRewardLogic.OnUpdateView += ShowDailyRewardEffect;
    }

    public void Clear()
    {
#if !DISABLE_INAPPS
      _iapLogic?.Clear();
#endif
      
      IapDailyRewardLogic.OnUpdateView -= ShowDailyRewardEffect;
      
      _uiFactory?.ClearMoneyCounter();

      if(_moneyCounter)
        _moneyCounter.Clear();  
      
      if(_addMoneyWindow)
        Object.Destroy(_addMoneyWindow.gameObject);
    }

    private void IAPLazyInitialization()
    {
      if (_uiFactory.IapWindow == null)
      {
#if !DISABLE_INAPPS
        _iapLogic = new IapLogic(_assetProvider, _adsService, _uiFactory, _localeSystem, _unlockService, _moneyService,
          _gameSaveSystem, _audioService, _remote, _analytics);
#endif
      }
    }

    public void ShowMoneyCounter(bool isActive) =>
      _moneyCounter.Show(isActive);

    public void ShowAddMoneyIfNotEnough() => 
      _addMoneyWindow.OpenWindow(true);

    private void ShowDailyRewardEffect(bool value) =>
      _moneyCounter.ShowEffect(value);

    private void OnClickAddMoney() => 
      _adsService.Rewarded.ShowReward("AddMoney", () => _moneyService.Earn(100), () =>
      {
        INotLoadedWindowLogic notLoadedWindowLogic = new NotLoadedWindowLogic(_localeSystem, _assetProvider);
        notLoadedWindowLogic.Open();
      });

    private void ToIapShop()
    {
      IAPLazyInitialization();

      _uiFactory.IapWindow.OpenWindow();
    }

    private void CreateSounds(MoneyCounter moneyCounter)
    {
      new ButtonSound(_audioService, moneyCounter.AddMoneyButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, moneyCounter.GetAddMoneyView.ButtonYes.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, moneyCounter.GetAddMoneyView.ButtonNo.gameObject, AudioId.Karaoke_Button);
    }
  }
}