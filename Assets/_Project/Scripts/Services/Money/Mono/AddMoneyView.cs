using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Animation;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.Localization.Mono;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Services.Money.Mono
{
  public class AddMoneyView : MonoBehaviour
  {
    private const float Duration = .5f;
    
    [Header("Elements")]
    
    public Button ButtonYes;
    public Button ButtonNo;
    
    [Header("Animations")]
    
    public CanvasGroup Background;
    public CanvasGroup Flashlights;
    public Transform BodyTransform;
    public CanvasGroup BodyCanvasGroup;
  
    [Header("Not Enough State")]
    public TextMeshProUGUI TextDescription;
    public TextMeshProUGUI TextDescriptionMain;
    public GameObject TextNotEnough;

    private Sequence _neonSequence;
    
    private float _cachedFontSize;
    private bool _isNotEnoughMoney;

    public void Construct(ILocaleSystem localeSystem, Action onClickYes)
    {
      ButtonYes.onClick.AddListener(() => onClickYes?.Invoke());
      ButtonNo.onClick.AddListener(CloseWindow);

      Hide();
      CreateAnimations();
      
      Localize(localeSystem);

      if(TextDescription)
        _cachedFontSize = TextDescription.fontSize;
    }

    private void Hide()
    {
      _neonSequence.Kill();
      gameObject.SetActive(false);
    }

    public void OpenWindow(bool isNotEnoughMoney)
    {
      SetupState(isNotEnoughMoney);
      FlashlightsAnimation();
      AppearAnimation();
    }

    private void SetupState(bool isNotEnoughMoney)
    {
      if (TextNotEnough == false)
        return;
      
      _isNotEnoughMoney = isNotEnoughMoney;
      
      TextNotEnough.SetActive(_isNotEnoughMoney);
      TextDescription.alignment = _isNotEnoughMoney? TextAlignmentOptions.TopLeft : TextAlignmentOptions.Midline;
      TextDescription.fontSize = _isNotEnoughMoney? TextDescriptionMain.fontSize : _cachedFontSize;
    }

    private void Update()
    {
      if(_isNotEnoughMoney)
        TextDescription.fontSize = TextDescriptionMain.fontSize;
    }

    private void CloseWindow() => 
      DisappearAnimation();

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Localize();
      });
    }

    private void CreateAnimations()
    {
      new ButtonAnimation(ButtonYes.transform);
      new ButtonAnimation(ButtonNo.transform);
    }

    private void AppearAnimation()
    {    
      gameObject.SetActive(true);

      Background.alpha = 0;
      BodyTransform.localScale = Vector3.zero;
      BodyCanvasGroup.alpha = 1;
      
      Sequence sequence = DOTween.Sequence();
      sequence.Append(Background.DOFade(1, Duration));
      sequence.Join(BodyTransform.DOScale(.9f, Duration));
      sequence.Join(BodyCanvasGroup.DOFade(1, Duration));
    }  
    
    private void DisappearAnimation()
    {
      Sequence sequence = DOTween.Sequence();
      sequence.Append(Background.DOFade(0, Duration));
      sequence.Join(BodyCanvasGroup.DOFade(0, Duration));

      sequence.onComplete += () => gameObject.SetActive(false);
    }
    
    private void FlashlightsAnimation()
    {
      _neonSequence = DOTween.Sequence();
      _neonSequence.SetDelay(2f);
      _neonSequence.Append(Flashlights.DOFade(0.1f, 0.05f).SetLoops(8, LoopType.Yoyo).SetEase(Ease.Linear));
      _neonSequence.Append(Flashlights.DOFade(0, 0.07f).SetLoops(3, LoopType.Yoyo).SetEase(Ease.Linear).SetDelay(0.15f));
      _neonSequence.Append(Flashlights.DOFade(1, 0.4f).SetEase(Ease.OutBounce));
      _neonSequence.Append(Flashlights.DOFade(1, 0).SetDelay(1.5f));
      _neonSequence.SetLoops(-1);
    }
  }
}