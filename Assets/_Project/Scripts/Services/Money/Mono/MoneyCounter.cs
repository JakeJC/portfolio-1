using System;
using _Project.Scripts.Services._Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Services.Money.Mono
{
  public class MoneyCounter : MonoBehaviour
  {
    public Button AddMoneyButton;
    public Transform CounterParent;
    public GameObject Effect;

    private IMoneyService _moneyService;

    private AddMoneyView _addMoneyView;
    private Features.MoneyCounterView.Logic.MoneyCounter _moneyCounter;

    public void Construct(IRemote remote, IAssetProvider assetProvider, ILocaleSystem localeSystem, IMoneyService moneyService,
      AddMoneyView spawnAddMoneyWindow, Action onClickYes, Action toIapShop, IAnalytics analytics)
    {
      _moneyService = moneyService;
      
      _addMoneyView = spawnAddMoneyWindow;
      _addMoneyView.Construct(localeSystem, remote.ShouldEnableFeatureTest() ? toIapShop : onClickYes);

      if (remote.ShouldEnableFeatureTest())
        AddMoneyButton.onClick.AddListener(delegate { toIapShop?.Invoke(); }); 
      else
        AddMoneyButton.onClick.AddListener(delegate { _addMoneyView.OpenWindow(false); });
      
      AddMoneyButton.onClick.AddListener(delegate { analytics.Send("btn_money"); }); 

      _moneyService.OnCountChange += SetText;

      _moneyCounter = new Features.MoneyCounterView.Logic.MoneyCounter();
      _moneyCounter.Construct(assetProvider);
      _moneyCounter.Initialize(CounterParent);
     
      _moneyCounter.SetCount(_moneyService.MoneyCount, 0,true);
    }

    public void ShowEffect(bool value)
    {
      if (Effect != null)  
        Effect.SetActive(value);
    }

    public void Show(bool isActive) => 
      gameObject.SetActive(isActive);

    public AddMoneyView GetAddMoneyView =>
      _addMoneyView;

    public void Clear()
    {
      _moneyCounter.Clear();
      
      if(_addMoneyView)
        Destroy(_addMoneyView.gameObject);
      
      if(this)
        Destroy(gameObject);
    }

    private void SetText(uint money, float delay) => 
      _moneyCounter.SetCount(money, delay);
  }
}