namespace _Project.Scripts.Services.Money
{
  public interface IMoneyCounterLogic
  {
    void Clear();
    void ShowAddMoneyIfNotEnough();
    void ShowMoneyCounter(bool isActive);
  }
}