using System;
using System.Collections.Generic;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.GalleryScreen.Mono;
using _Project.Scripts.Features.ScrollView;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio;
using ModestTree;
using UnityEngine;
using System.Linq;
using _Project.Scripts.Features.GalleryScreen.Serialized;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using _Project.Scripts.Services.Localization.Logic;

namespace _Project.Scripts.Features.GalleryScreen
{
  public class GalleryWatching
  {
    private const string LocalePath = "Main";

    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;
    private readonly IUnlockService _unlockService;
    private readonly IGameSaveSystem _gameSaveSystem;

    private readonly Action _checkUnlocked;
    private readonly GalleryLogic _galleryLogic;
    private readonly GalleryWatchingView _galleryWatchingView;

    private GalleryData _openedButton;

    public GalleryWatching(GalleryLogic galleryLogic, IGameSaveSystem gameSaveSystem, IAudioService audioService,
      IUnlockService unlockService,
      GalleryWatchingView galleryWatchingView, Action checkUnlocked, ILocaleSystem localeSystem)
    {
      _galleryLogic = galleryLogic;
      _checkUnlocked = checkUnlocked;

      _audioService = audioService;
      _localeSystem = localeSystem;
      _unlockService = unlockService;
      _gameSaveSystem = gameSaveSystem;

      _galleryWatchingView = galleryWatchingView;
      _galleryWatchingView.Construct(OnClickWatchingPrevious, OnClickWatchingNext);

      CreateSounds(_galleryWatchingView);
    }

    public void OnClickItem(GalleryData item)
    {
      Debug.Log("OnClick " + item.Description);
      _openedButton = item;

      SetTitle(item);

      // CheckShowingAd(item);
      _galleryWatchingView.WatchingLocker(!IsUnlocked(item.Id));
      _galleryWatchingView.StartWatching(item);
    }

    private void SetTitle(GalleryData item)
    {
      var maxValue = GetAllSameTypesGirlContent(item).Length;
      var currentValue = GetAllSameTypesGirlContent(item).IndexOf(item) + 1;

      _galleryWatchingView.SetTitle(currentValue + "/" + maxValue + " " +
                                    _localeSystem.GetText(LocalePath, item.Type.ToString()));
    }

    private void CheckShowingAd(GalleryData item)
    {
      if (!IsUnlocked(item.Id) && item.Type == GalleryType.Stories)
        _galleryWatchingView.ShowAdButton(() => Unlock(item));
      else
        _galleryWatchingView.HideAdButton();
    }

    private void OnClickWatchingPrevious()
    {
      int index = GetGirlContent(_openedButton).IndexOf(_openedButton) - 1;
      index = index < 0 ? GetGirlContent(_openedButton).Length - 1 : index;

      OnClickItem(GetGirlContent(_openedButton)[index]);
    }

    private void OnClickWatchingNext()
    {
      int index = GetGirlContent(_openedButton).IndexOf(_openedButton) + 1;
      index = index >= GetGirlContent(_openedButton).Length ? 0 : index;

      OnClickItem(GetGirlContent(_openedButton)[index]);
    }

    private void Unlock(GalleryData item)
    {
      if (!_gameSaveSystem.Get().UnlockedStoriesContent.Contains(item.Id))
        _unlockService.Unlock(item.Id, () =>
        {
          UnlockStories(item);
          OnClickItem(item);
        });
    }

    private void UnlockStories(GalleryData item)
    {
      if (_gameSaveSystem.Get().UnlockedStoriesContent.Contains(item.Id)) return;

      _gameSaveSystem.Get().UnlockedStoriesContent.Add(item.Id);
      _gameSaveSystem.Save();

      _checkUnlocked?.Invoke();
    }

    private void CreateSounds(GalleryWatchingView galleryWatchingView)
    {
      new ButtonSound(_audioService, galleryWatchingView.CloseWatchingButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, galleryWatchingView.PreviousSlideButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, galleryWatchingView.NextSlideButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, galleryWatchingView.WatchAdButton.gameObject, AudioId.Karaoke_Button);
    }

    private bool IsUnlocked(string itemId) =>
      _gameSaveSystem.Get().UnlockedPhotoContent.Contains(itemId) ||
      _gameSaveSystem.Get().UnlockedStoriesContent.Contains(itemId);

    private GalleryData[] GetAllSameTypesGirlContent(GalleryData item) =>
      GetGirlContent(item).ToList().FindAll(itemGallery => itemGallery.Type == item.Type).ToArray();

    private GalleryData[] GetGirlContent(GalleryData item) =>
      _galleryLogic.GalleryDatasMap.FirstOrDefault(t => t.Value.Contains(item)).Value;
  }
}