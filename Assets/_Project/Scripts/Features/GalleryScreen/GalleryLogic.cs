using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Core_Logic.Screens;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Core_Logic.StatesMachine.States;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.GalleryScreen.Mono;
using _Project.Scripts.Features.GalleryScreen.Serialized;
using _Project.Scripts.Features.IapWindow;
using _Project.Scripts.Features.ScrollView;
using _Project.Scripts.Features.ScrollView.Serialized;
using _Project.Scripts.Features.ShopScreen.Content.Extra;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using ModestTree;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.GalleryScreen
{
  public class GalleryLogic
  {
    private const string GirlsDataPath = "Features/Data/GameGirlsData/Game Girls Data";

    private readonly IUiFactory _uiFactory;
    private readonly IGalleryFactory _galleryFactory;

    private readonly IAnalytics _analytics;
    private readonly IAdsService _adsService;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;
    private readonly ICurrentScreen _currentScreen;
    private readonly IUnlockService _unlockService;
    private readonly IRemote _remote;
    private readonly IMoneyService _moneyService;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IGameStateMachine _gameStateMachine;
    private readonly IAssetProvider _assetProvider;

    private readonly GameGirlsData _gameGirlsData;

    private readonly Dictionary<string, ScrollViewItemsView> _galleryItemsView = new Dictionary<string, ScrollViewItemsView>();

    public readonly Dictionary<string, GalleryData[]> GalleryDatasMap = new Dictionary<string, GalleryData[]>();
    public readonly Dictionary<string, ScrollViewGalleryItemButton[]> GalleryItemButtons = new Dictionary<string, ScrollViewGalleryItemButton[]>();

#if !DISABLE_INAPPS
    private IapLogic _iapLogic;
#endif

    private GalleryView _galleryView;

    private GalleryWatching _galleryWatching;
    private GalleryWatchingView _galleryWatchingView;

    private List<ScrollViewSectionButton> _sections = new List<ScrollViewSectionButton>();

    private readonly IExtraContentViewLogic _extraContentViewLogic;

    public GalleryLogic(IGameStateMachine gameStateMachine, IAssetProvider assetProvider, ICurrentScreen currentScreen,
      IGameSaveSystem gameSaveSystem, IAdsService adsService, IAnalytics analytics, ILocaleSystem localeSystem,
      IAudioService audioService, IUnlockService unlockService, IRemote remote, IMoneyService moneyService)
    {
      _analytics = analytics;
      _adsService = adsService;
      _localeSystem = localeSystem;
      _audioService = audioService;
      _currentScreen = currentScreen;
      _unlockService = unlockService;
      _remote = remote;
      _moneyService = moneyService;
      _gameSaveSystem = gameSaveSystem;
      _gameStateMachine = gameStateMachine;
      _assetProvider = assetProvider;

      _uiFactory = new UiFactory(assetProvider, remote, localeSystem);
      _galleryFactory = new GalleryFactory(assetProvider, _remote);

      _gameGirlsData = assetProvider.GetResource<GameGirlsData>(GirlsDataPath);

      _currentScreen.SetCurrentScreen(ScreenNames.Gallery);

      _extraContentViewLogic = new ExtraContentViewLogic();
      _extraContentViewLogic.Construct(_gameSaveSystem, assetProvider);
    }
    
    private void IAPLazyInitialization()
    {
      if (_uiFactory.IapWindow == null)
      {
#if !DISABLE_INAPPS
        _iapLogic = new IapLogic(_assetProvider, _adsService, _uiFactory, _localeSystem, _unlockService, _moneyService,
          _gameSaveSystem, _audioService, _remote, _analytics);
#endif
      }
    }

    public void CreateGalleryView()
    {
      _galleryView = _galleryFactory.CreateGalleryView();
      _galleryView.Construct(_localeSystem, CloseStoryView);

      _galleryWatchingView = _galleryFactory.CreateGalleryWatchingView();
      _galleryWatching = new GalleryWatching(this, _gameSaveSystem, _audioService, _unlockService,
        _galleryWatchingView, CheckUnlocked, _localeSystem);

      ExtraLookData extraLookData = _extraContentViewLogic.GetCurrentLook();
      if (extraLookData)
        _galleryView.SetGirlView(extraLookData.DefaultStatic);

      CreateGalleryContent();

      CheckUnlocked();

      CreateSounds(_galleryView);

      if (_remote.ShouldEnableFeatureTest())
      {
        IAPLazyInitialization();

        _galleryView.IAPOfferPanel.gameObject.SetActive(true);
        _iapLogic.OpenIapOffer(IapLogic.OfferJoyPackId, _galleryView.IAPOfferPanel,
          delegate(bool result)
          {
            if (result) 
              _gameStateMachine.Enter<LoadGalleryState>();
          }, delegate { _galleryView.IAPOfferPanel.gameObject.SetActive(false); });
      }
    }

    public void ClearGalleryView()
    {
#if !DISABLE_INAPPS
      _iapLogic?.Clear();
#endif

      if (!_galleryView)
        return;

      _sections.Clear();
      Object.Destroy(_galleryWatchingView.gameObject);
      Object.Destroy(_galleryView.gameObject);
    }

    private void Select(int index)
    {
      for (int i = 0; i < _sections.Count; i++)
        _sections[i].Select(i == index);
    }

    private void CreateGalleryContent()
    {
      foreach (GirlData t in _gameGirlsData.GirlDatas)
      {
        ScrollViewItemsView itemsView = _galleryFactory.CreateItemsView(_galleryView.ItemViewsParent);
        _galleryItemsView.Add(t.Name, itemsView);

        ScrollViewSectionButton shopButton = _galleryFactory.CreateSectionButton(_galleryView.SectionsParent);
        _sections.Add(shopButton);

        shopButton.Construct(_localeSystem, t.Name, () =>
        {
          int index = _sections.IndexOf(shopButton);
          Select(index);
          OnClickGirl(t);
        }, t.Icon);

        var galleryDatas = new List<GalleryData>();
        var galleryItemButton = new List<ScrollViewGalleryItemButton>();

        List<GalleryData> datas = t.GalleryDatas.ToList();

        datas = SortGalleryDatas(datas);

        foreach (GalleryData item in datas)
        {
          ScrollViewGalleryItemButton scrollViewItemButton = _galleryFactory.CreateItemButton(itemsView.ContentParent);
          scrollViewItemButton.Construct(_localeSystem, _unlockService,
            new ScrollViewItemData() { Id = item.Id, NameKey = item.Name, DescriptionKey = item.Description, Icon = item.GalleryLink.Preview },
            () => OnClickItem(item), _audioService, String.Empty, _remote);
          scrollViewItemButton.IsStories = item.Type == GalleryType.Stories;
          scrollViewItemButton.SetPreviewRectangle(item.PreviewPosition, item.PreviewScale);

          galleryDatas.Add(item);
          galleryItemButton.Add(scrollViewItemButton);
        }

        GalleryDatasMap.Add(t.Name, galleryDatas.ToArray());
        GalleryItemButtons.Add(t.Name, galleryItemButton.ToArray());
      }

      Select(0);
      OnClickGirl(_gameGirlsData.GirlDatas[0]);
    }

    private List<GalleryData> SortGalleryDatas(List<GalleryData> datas)
    {
      if (_remote.ShouldEnableFeatureTest())
        datas = Sort(datas);
      else
        datas.Sort(UnlockedOrder);
      return datas;
    }

    private List<GalleryData> Sort(List<GalleryData> datas)
    {
      List<GalleryData> unlocked = new List<GalleryData>();
      List<GalleryData> locked = new List<GalleryData>();

      foreach (var d in datas)
      {
        if (_gameSaveSystem.Get().UnlockedPhotoContent.Contains(d.Id) ||
            _gameSaveSystem.Get().UnlockedStoriesContent.Contains(d.Id))
          unlocked.Add(d);
        else
          locked.Add(d);
      }
      
      unlocked.Sort( (l, r) => l.SortPriority.CompareTo(r.SortPriority) );

      List<GalleryData> result = new List<GalleryData>(unlocked);
      foreach (var l in locked) result.Add(l);

      return result;
    }

    private int UnlockedOrder(GalleryData p1, GalleryData p2)
    {
      return GetUnlocked(p2) - GetUnlocked(p1);

      int GetUnlocked(GalleryData p) =>
        _gameSaveSystem.Get().UnlockedPhotoContent.Contains(p.Id) ||
        _gameSaveSystem.Get().UnlockedStoriesContent.Contains(p.Id)
          ? 1
          : 0;
    }

    private void OnClickGirl(BaseContentData item)
    {
      foreach (KeyValuePair<string, ScrollViewItemsView> itemsView in GetDeactivatedItemsViews(item.Id))
        itemsView.Value.Deactivate();

      _galleryItemsView[item.Name].Activate();
    }

    private void OnClickItem(GalleryData item) =>
      _galleryWatching.OnClickItem(item);

    private void CheckUnlocked()
    {
      foreach (ScrollViewItemButton itemButton in GetAllItemButtons())
      {
        if (IsUnlocked(itemButton.GetId()))
        {
          itemButton.OpenContent();
          itemButton.SetLockButton(false);
        }
        else
          itemButton.SetLockButton(true);
      }
    }

    private void CloseStoryView()
    {
      if (_remote.ShouldEnableFeatureTest())
      {
        _gameStateMachine.Enter<LoadHomeState>();
        return;
      }

      _adsService.CurrentInterstitial.ShowInterstitial(ScreenNames.Home,
        _gameStateMachine.Enter<LoadHomeState>,
        _gameStateMachine.Enter<LoadHomeState>);
    }

    private void CreateSounds(GalleryView galleryView)
    {
      new ButtonSound(_audioService, galleryView.BackButton.gameObject, AudioId.Karaoke_Button);
    }

    private IEnumerable<KeyValuePair<string, ScrollViewItemsView>> GetDeactivatedItemsViews(string id) =>
      _galleryItemsView.Where(item => item.Key != id);

    private IEnumerable<ScrollViewItemButton> GetAllItemButtons() =>
      GalleryItemButtons.SelectMany(item => item.Value);

    private bool IsUnlocked(string itemId) =>
      _gameSaveSystem.Get().UnlockedPhotoContent.Contains(itemId) || _gameSaveSystem.Get().UnlockedStoriesContent.Contains(itemId);
  }
}