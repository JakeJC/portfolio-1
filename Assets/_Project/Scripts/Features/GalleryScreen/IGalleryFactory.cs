using _Project.Scripts.Features.GalleryScreen.Mono;
using _Project.Scripts.Features.ScrollView;
using UnityEngine;

namespace _Project.Scripts.Features.GalleryScreen
{
  public interface IGalleryFactory
  {
    GalleryView CreateGalleryView();
    ScrollViewSectionButton CreateSectionButton(Transform parent);
    ScrollViewItemsView CreateItemsView(Transform parent);
    ScrollViewGalleryItemButton CreateItemButton(Transform parent);
    GalleryWatchingView CreateGalleryWatchingView();
  }
}