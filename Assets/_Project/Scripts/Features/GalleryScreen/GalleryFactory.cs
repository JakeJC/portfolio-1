using _Project.Scripts.Features.GalleryScreen.Mono;
using _Project.Scripts.Features.ScrollView;
using _Project.Scripts.Services._Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.GalleryScreen
{
  public class GalleryFactory : IGalleryFactory
  {
    private const string GalleryViewPath = "Features/GalleryScreen/Gallery Window";
    private const string GalleryWatchingViewPath = "Features/GalleryScreen/Watching Window";

    private const string ItemsViewPath = "Features/Other/ScrollViewPrefabs/Gallery/Scroll View Gallery Items View";
    private const string ItemButtonPath = "Features/Other/ScrollViewPrefabs/Gallery/Scroll View Gallery Item Button";
    private const string SectionButtonPath = "Features/Other/ScrollViewPrefabs/Scroll View Shop Section Button";

    private const string BaseCanvas = "Canvas";

    private readonly IAssetProvider _assetProvider;
    private readonly IRemote _remote;

    public GalleryFactory(IAssetProvider assetProvider, IRemote remote)
    {
      _assetProvider = assetProvider;
      _remote = remote;
    }

    public GalleryView CreateGalleryView()
    {
      Transform parent = GameObject.Find(BaseCanvas).transform;

      var homeScreen = _assetProvider.GetResource<GalleryView>(GalleryViewPath + (_remote.ShouldEnableFeatureTest() ? "-AB" : string.Empty));
      return Object.Instantiate(homeScreen, parent);
    }

    public ScrollViewSectionButton CreateSectionButton(Transform parent)
    {
      var homeScreen = _assetProvider.GetResource<ScrollViewSectionButton>(SectionButtonPath  + (_remote.ShouldEnableFeatureTest() ? "-AB" : string.Empty));
      return Object.Instantiate(homeScreen, parent);
    }

    public ScrollViewItemsView CreateItemsView(Transform parent)
    {
      var homeScreen = _assetProvider.GetResource<ScrollViewItemsView>(ItemsViewPath);
      return Object.Instantiate(homeScreen, parent);
    }

    public ScrollViewGalleryItemButton CreateItemButton(Transform parent)
    {
      var homeScreen = _assetProvider.GetResource<ScrollViewGalleryItemButton>(ItemButtonPath);
      return Object.Instantiate(homeScreen, parent);
    }

    public GalleryWatchingView CreateGalleryWatchingView()
    {
      Transform parent = GameObject.Find(BaseCanvas).transform;
      
      var homeScreen = _assetProvider.GetResource<GalleryWatchingView>(GalleryWatchingViewPath);
      GalleryWatchingView galleryWatchingView = Object.Instantiate(homeScreen, parent);
      
      galleryWatchingView.gameObject.SetActive(false);

      return galleryWatchingView;
    }
  }
}