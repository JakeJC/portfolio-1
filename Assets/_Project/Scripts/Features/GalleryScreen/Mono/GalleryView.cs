using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Animation;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Mono;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.GalleryScreen.Mono
{
  public class GalleryView : MonoBehaviour
  {
    public Button BackButton;

    public Transform SectionsParent;
    public Transform ItemViewsParent;
   
    public Image Girl;

    public RectTransform IAPOfferPanel;

    public void Construct(ILocaleSystem localeSystem, Action onCloseStories)
    {
      BackButton.onClick.AddListener(() => onCloseStories());

      CreateAnimation();

      Localize(localeSystem);
    }

    public void SetGirlView(Sprite girlSprite) => 
      Girl.sprite = girlSprite;

    private void CreateAnimation()
    {
      new ButtonAnimation(BackButton.transform);
    }
    
    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Localize();
      });
    }
  }
}