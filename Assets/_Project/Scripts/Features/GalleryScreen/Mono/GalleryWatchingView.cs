using System;
using _Project.Scripts.Animation;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.GalleryScreen.Serialized;
using DG.Tweening;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.GalleryScreen.Mono
{
  public class GalleryWatchingView : MonoBehaviour
  {
    private const float Duration = .5f;
    
    public Button CloseWatchingButton;
    public Button PreviousSlideButton;
    public Button NextSlideButton;

    public Button WatchAdButton;

    public CanvasGroup WatchAdGroup;

    public CanvasGroup LiveGroup;
    public CanvasGroup LockGroup;
    public CanvasGroup PhotoGroup;
    public CanvasGroup StoriesGroup;

    public TextMeshProUGUI Title;

    [Header("Animation")] 
    
    public CanvasGroup Background;
    public Transform Body;
    
    public RectTransform Blink;

    public Image InBlur;
    public Image Photo;
    public SkeletonGraphic SkeletonAnimation;

    public void Construct(Action onClickWatchingPrevious, Action onClickWatchingNext)
    {
      CloseWatchingButton.onClick.AddListener(DisappearAnimation);

      PreviousSlideButton.onClick.AddListener(() => onClickWatchingPrevious?.Invoke());
      NextSlideButton.onClick.AddListener(() => onClickWatchingNext?.Invoke());

      CreateAnimation();
      AnimateBlink();
    }
    
    private void AnimateBlink()
    {
      if (!Blink) 
        return;
      
      Sequence sequence = DOTween.Sequence();
      sequence.AppendInterval(7);
      sequence.Append(Blink.DOAnchorPos(new Vector2(60, 0), 1f));

      sequence.SetLoops(-1, LoopType.Restart);
    }
    
    private void CreateAnimation()
    {
      new ButtonAnimation(WatchAdButton.transform);
      new ButtonAnimation(CloseWatchingButton.transform);
      new ButtonAnimation(PreviousSlideButton.transform);
      new ButtonAnimation(NextSlideButton.transform);
    }

    public void WatchingLocker(bool isLock)
    {
      PhotoGroup.DOKill();
      StoriesGroup.DOKill();
      PhotoGroup.alpha = 0f;
      StoriesGroup.alpha = 0f;
      LockGroup.alpha = isLock ? 1f : 0f;
    }

    public void StartWatching(GalleryData data)
    {
      InBlur.sprite = data.GalleryLink.InBlur;
      
      if (data.Type == GalleryType.Photo)
      {
        HideLive();
        Photo.sprite = data.GalleryLink.Image;
        PhotoGroup.DOFade(LockGroup.alpha > 0 ? 0f : 1f, Duration);
        Photo.transform.localScale = data.ShowingScale;
      }
      else
      {
        ShowLive();
        SkeletonAnimation.skeletonDataAsset = data.GalleryLink.SkeletonData;
        SkeletonAnimation.Initialize(true);
        StoriesGroup.DOFade(LockGroup.alpha > 0 ? 0f : 1f, Duration);
        SkeletonAnimation.transform.localScale = data.ShowingScale;
      }
      AppearAnimation();
    }

    public void ShowAdButton(Action onClickShowAd)
    {
      WatchAdButton.onClick.RemoveAllListeners();
      WatchAdButton.onClick.AddListener(() => onClickShowAd?.Invoke());

      WatchAdGroup.DOFade(1f, Duration);
      WatchAdGroup.blocksRaycasts = true;
    }

    public void HideAdButton()
    {
      WatchAdGroup.DOFade(0f, Duration);
      WatchAdGroup.blocksRaycasts = false;
    }

    private void ShowLive() =>
      LiveGroup.DOFade(1f, Duration);

    private void HideLive() =>
      LiveGroup.DOFade(0f, Duration);
    
    private void AppearAnimation()
    {    
      if(gameObject.activeSelf)
        return;
      
      gameObject.SetActive(true);

      Background.alpha = 0;
      Body.localScale = Vector3.zero;
      
      Sequence sequence = DOTween.Sequence();
      sequence.Append(Background.DOFade(1, Duration));
      sequence.Join(Body.DOScale(1, Duration));
    }  
    
    private void DisappearAnimation()
    {
      Sequence sequence = DOTween.Sequence();
      sequence.Append(Background.DOFade(0, Duration));
      sequence.Join(Body.DOScale(0, Duration));

      sequence.onComplete += () => gameObject.SetActive(false);
    }
    
    public void SetTitle(string information) => 
      Title.text = information;
  }
}