using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.GalleryScreen.Mono
{
  public class AutoGrid : MonoBehaviour
  {
    public Vector2 Size = new Vector2(207, 207);
    private void Awake()
    {
      var factor = (float)Screen.width / Screen.height;
      
      if (factor <= .5f)
        GetComponent<GridLayoutGroup>().cellSize = Size;
    }
  }
}