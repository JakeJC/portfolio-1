﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Animation;
using _Project.Scripts.Core_Logic.Screens;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Features.Policy;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.Localization.Mono;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.SettingsWindow
{
  public class Settings : MonoBehaviour
  {   
    private const float Duration = .5f;

    private const string IconMusicOnPath = "icon_music_on";
    private const string IconMusicOffPath = "icon_music_off";
    private const string IconSoundOnPath = "icon_sound_on";
    private const string IconSoundOffPath = "icon_sound_off";
    
    [Header("Main")]
    
    public Button ButtonBack;
    public Button ButtonPrivacyPolicy;
    public Button ButtonRate;
    public Button ButtonGallery;
    public Button ButtonBackground;

    public Slider ButtonMusic;
    public Slider ButtonSound;

    public Button Screen;

    public Image IconMusic;
    public Image IconSound;
    public Transform SwitcherMusic;
    public Transform SwitcherSound;

    public Sprite Active;
    public Sprite Inactive;
    public Color InactiveColor;

    [Header("Animation")]

    public CanvasGroup Background;
    public Transform BodyTransform;

    private Sprite _musicOn;
    private Sprite _musicOff;
    private Sprite _soundOn;
    private Sprite _soundOff;
    private Sprite _switcherOn;
    private Sprite _switcherOff;

    private IGameSaveSystem _gameSaveSystem;
    private IAudioService _audioService;
    private ICurrentScreen _screenManager;

    private PrivacyPolicy _privacyPolicy;
    private IPrivacyPolicyFactory _privacyPolicyFactory;

    private IGameStateMachine _gameStateMachine;

    private IAnalytics _analytics;
    private IAdsService _adsService;
    private IRemote _remote;
    
    private string _cachedScreen;
    private Action _onClose;

    public void Construct(
      IGameStateMachine gameStateMachine,
      IGameSaveSystem gameSaveSystem,
      IAudioService audioService,
      ILocaleSystem localeSystem,
      IAnalytics analytics,
      IPrivacyPolicyFactory privacyPolicyFactor,
      ICurrentScreen screenManager,
      IAdsService adsService,
      IRemote remote)
    {
      _remote = remote;
      _adsService = adsService;
      _screenManager = screenManager;
      _gameStateMachine = gameStateMachine;

      _gameSaveSystem = gameSaveSystem;
      _audioService = audioService;
      _analytics = analytics;

      _privacyPolicyFactory = privacyPolicyFactor;
      _privacyPolicy = _privacyPolicyFactory.SpawnPrivacyPolicy();
      _privacyPolicy.Construct(_gameStateMachine, _gameSaveSystem, _audioService, localeSystem, analytics, remote, adsService);

      Localize(localeSystem);
      LoadSprites();
      AssignListeners();

      MuteMusic(_gameSaveSystem.Get().IsMusicDisabled);
      MuteSounds(_gameSaveSystem.Get().IsSoundDisabled);

      gameObject.SetActive(false);

      new ButtonAnimation(ButtonBack.transform);
      new ButtonAnimation(ButtonPrivacyPolicy.transform);

      SoundCreator();
    }

    private void SoundCreator()
    {
      new ButtonSound(_audioService, ButtonBack.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, ButtonMusic.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, ButtonSound.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, ButtonPrivacyPolicy.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, ButtonRate.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, ButtonGallery.gameObject, AudioId.Karaoke_Button);
    }

    public void OpenWindow(Action onClose = null)
    {
      _onClose = onClose;
      AppearAnimation();
    }

    public void Clear() => 
      _privacyPolicyFactory.Clear();

    private void CloseWindow()
    {
      _onClose?.Invoke();
      DisappearAnimation();
    }

    private void Back() =>
      CloseWindow();

    private void PrivacyPolicy()
    {
      _privacyPolicy.ClosingStarted += OnClosingStarted;
      _privacyPolicy.OpenWindow();
      CloseWindow();
      _cachedScreen = _screenManager.GetCurrentScreen();
      _screenManager.SetCurrentScreen(ScreenNames.Privacy);

      void OnClosingStarted()
      {
        OpenWindow();
        _privacyPolicy.ClosingStarted -= OnClosingStarted;
        _screenManager.SetCurrentScreen(_cachedScreen);
      }
    }
    
    private void AppearAnimation()
    {    
      gameObject.SetActive(true);

      Background.alpha = 0;
      BodyTransform.localScale = Vector3.zero;
      
      Sequence sequence = DOTween.Sequence();
      sequence.Append(Background.DOFade(1, Duration));
      sequence.Join(BodyTransform.DOScale(1, Duration));
    }  
    
    private void DisappearAnimation()
    {
      Sequence sequence = DOTween.Sequence();
      sequence.Append(Background.DOFade(0, Duration));
      sequence.Join(BodyTransform.DOScale(0, Duration));

      sequence.onComplete += () => gameObject.SetActive(false);
    }
    
    private void SwitchMusic(float volume) =>
      MuteMusic(volume);

    private void SwitchSound(float volume) => 
      MuteSounds(volume);

    private void LoadSprites()
    {
      _musicOn = LoadSprite(IconMusicOnPath);
      _musicOff = LoadSprite(IconMusicOffPath);
      _soundOn = LoadSprite(IconSoundOnPath);
      _soundOff = LoadSprite(IconSoundOffPath);
    }

    private Sprite LoadSprite(string path) =>
      Resources.Load<Sprite>(path);

    private void AssignListeners()
    {
      ButtonBackground.onClick.AddListener(Back);
      ButtonBack.onClick.AddListener(Back);
      ButtonPrivacyPolicy.onClick.AddListener(PrivacyPolicy);
      ButtonMusic.onValueChanged.AddListener(SwitchMusic);
      ButtonSound.onValueChanged.AddListener(SwitchSound);
    }
    
    private void MuteMusic(float mute)
    {
      SwitchAudio(SoundType.Music, mute);

      _gameSaveSystem.Get().IsMusicDisabled = mute;
      _gameSaveSystem.Save();
    }

    private void MuteSounds(float mute)
    {
      SwitchAudio(SoundType.Sound, mute);

      _gameSaveSystem.Get().IsSoundDisabled = mute;
      _gameSaveSystem.Save();
    }

    private void SwitchAudio(SoundType type, float mute)
    {
      switch (type)
      {
        case SoundType.Music:
          IconMusic.sprite = mute > 0 ? _musicOn : _musicOff;
          ButtonMusic.value = mute;
          var music = SwitcherMusic.parent.GetComponent<Image>();
          music.sprite = mute > 0 ? Active : Inactive;
          music.color = mute > 0 ? Color.white : InactiveColor;
          _audioService.MuteMusic(mute);
          break;
        case SoundType.Sound:
          IconSound.sprite = mute > 0 ? _soundOn : _soundOff;
          ButtonSound.value = mute;
          var sound = SwitcherSound.parent.GetComponent<Image>();
          sound.sprite = mute > 0 ? Active : Inactive;
          sound.color = mute > 0 ? Color.white : InactiveColor;
          _audioService.MuteSounds(mute);
          break;
      }
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Localize();
      });
    }

    private enum SoundType
    {
      Music,
      Sound
    }
  }
}