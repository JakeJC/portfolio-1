using System;
using _Features.Disable_Ads_Window.Timer;
using _Features.Disable_Ads_Window.Timer.Interfaces;
using _Project.Scripts.Core_Logic.Screens;
using _Project.Scripts.Features.DisableAds.Mono;
using _Project.Scripts.Features.DisableAdsScreen.Timer.Interfaces;
using _Project.Scripts.Features.NotLoadedWindow;
using _Project.Scripts.Services._data;
using _Project.Scripts.Services._Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.DisableAds
{
  public class DisableAdsLogic : ICallbackExecutor
  {
    private const int TargetAds = 2;
    private const string TimerGuid = "DisableAdsLogic24Timer";
    private const int DelaySec = 86400;

    private readonly IGameSaveSystem _saveSystem;
    private readonly IAnalytics _analytics;
    private readonly IAdsService _adsService;
    private readonly IUiFactory _factory;
    private readonly ILocaleSystem _localeSystem;

    private readonly Action<bool> _onDisableAds;
    private readonly IRemote _remote;

    private readonly DisableAdsWindow _disableAdsWindow;

    private readonly ITimerController _timerController;
    private IAssetProvider _assetProvider;


    private bool WasDisabled
    {
      get => PlayerPrefs.GetInt("DisableAdsLogic.WasDisabled", 0) == 1;
      set => PlayerPrefs.SetInt("DisableAdsLogic.WasDisabled", value ? 1 : 0);
    }
    
    public DisableAdsLogic(IAssetProvider assetProvider, IGameSaveSystem saveSystem, IAnalytics analytics, 
      IAdsService adsService, ILocaleSystem localeSystem, IUiFactory factory, Action<bool> onDisableAds, IRemote remote)
    {
      _assetProvider = assetProvider;
      _localeSystem = localeSystem;
      _saveSystem = saveSystem;
      _analytics = analytics;
      _adsService = adsService;
      _factory = factory;
      _onDisableAds = onDisableAds;
      _remote = remote;
      _factory.SpawnDisableAdsWindow();
      _disableAdsWindow = _factory.DisableAdsWindow;
      _factory.DisableAdsWindow.Construct(localeSystem, analytics, OnShowAds, OnClose, OnContinue);

      _timerController = new TimerController();
      _timerController.LaunchService();

      Debug.Log($"IsUnderTimer {_timerController.IsUnderTimer(TimerGuid)}");

      if (_timerController.IsUnderTimer(TimerGuid))
      {
        _timerController.AddCallback(this);
        DisableAds(true);
      }
      else
      {
        if (WasDisabled)
        {
          EnableAds();
        }
      }
    }

    public void Cleanup() =>
      _factory.ClearDisableAdsWindow();

    public void Open()
    {
      SetDisableAdsProgress();
      _disableAdsWindow.ShowInfoPanelBefore();
      _disableAdsWindow.OpenWindow();
    }

    private void Close() =>
      _disableAdsWindow.CloseWindow();

    private void SetDisableAdsProgress() => 
      _disableAdsWindow.UpdateAdsWatchedProgress(_saveSystem.Get().DisableAdsProgress, TargetAds);

    private void OnContinue() => 
      Close();

    private void OnClose()
    {
      _analytics.Send("ad_offer_skipped");
      Close();

      if (_remote.ShouldEnableFeatureTest())
        return;

      _adsService.CurrentInterstitial.ShowInterstitial(ScreenNames.Home, () => { }, () => {});
    }

    private void OnShowAds()
    {
      _analytics.Send("ad_offer_accepted");
      _adsService.Rewarded.ShowReward("DisableAds", OnReward, OnNotLoadedAds);
    }

    private void OnReward()
    {
      GameSaveData save = _saveSystem.Get();
      save.DisableAdsProgress = Mathf.Clamp(save.DisableAdsProgress + 1, 0, TargetAds);
      _saveSystem.Save();

      SetDisableAdsProgress();

      if (save.DisableAdsProgress == TargetAds)
      {
        DisableAds();
        CreateTimer(TimerGuid, DelaySec);
      }
    }

    private void OnNotLoadedAds()
    {
      INotLoadedWindowLogic notLoadedWindowLogic = new NotLoadedWindowLogic(_localeSystem, _assetProvider);
      notLoadedWindowLogic.Open();
    }

    private void EnableAds()
    {
      GameSaveData save = _saveSystem.Get();
      save.DisableAdsIsActive = false;
      save.DisableAdsProgress = save.DisableAdsProgress >= TargetAds ? 0 : save.DisableAdsProgress;
      _saveSystem.Save();

      _onDisableAds?.Invoke(false);

      SetDisableAdsProgress();
      _disableAdsWindow.ShowInfoPanelBefore();
      
      if(!WasDisabled)
        return;
      
      WasDisabled = false;

      _adsService.EnableAds(true);
      _adsService.Banner.HideBanner();
    }

    private void DisableAds(bool fromConstruct = false)
    {
      WasDisabled = true;

      GameSaveData save = _saveSystem.Get();
      save.DisableAdsIsActive = true;
      _saveSystem.Save();

      _onDisableAds?.Invoke(true);

      _disableAdsWindow.ShowInfoPanelAfter();

      _adsService.DisableAds();
    }

    private void CreateTimer(string guid, int seconds)
    {
      DateTime targetDatetime = _timerController.TimeProvider.GetCurrentTime().AddSeconds(seconds);
      _timerController.AddCallback(this);
      _timerController.CreateTimer(guid, targetDatetime);
    }

    public string GetTimerGuid() =>
      TimerGuid;

    public void ExecuteWhenTick(int seconds) => 
      _disableAdsWindow.UpdateDisableAdsTimer(seconds);

    public void ExecuteWhenComplete() =>
      EnableAds();

    public void SetLock(bool isLocked)
    {
    }

    public void Destroy()
    {
    }
  }
}