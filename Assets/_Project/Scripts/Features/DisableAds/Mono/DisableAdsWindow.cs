using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Animation;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Mono;
using AppacheAnalytics.Scripts.Interfaces;
using Sirenix.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.DisableAds.Mono
{
  public class DisableAdsWindow : MonoBehaviour
  {
    private const string ShowAdsTextKey = "DisableAds.Show";
    private const string LocalizeName = "Main";

    public Button ShowBtn;
    public Button CloseBtn;
    public Button ContinueBtn;

    public TextMeshProUGUI AdsWatchedProgress;
    public TextMeshProUGUI DisableAdsTimer;

    public GameObject InfoPanelBefore, InfoPanelAfter;

    private ILocaleSystem _localeSystem;
    private IAnalytics _analytics;

    public void Construct(ILocaleSystem localeSystem, IAnalytics analytics, Action onShowAds, Action onClose, Action onContinue)
    {
      gameObject.SetActive(false);

      _localeSystem = localeSystem;
      _analytics = analytics;

      ShowBtn.onClick.AddListener(delegate { onShowAds?.Invoke(); });
      CloseBtn.onClick.AddListener(delegate { onClose?.Invoke(); });
      ContinueBtn.onClick.AddListener(delegate { onContinue?.Invoke(); });

      Localize(localeSystem);
      
      new ButtonAnimation(ShowBtn.transform);
      new ButtonAnimation(CloseBtn.transform);
      new ButtonAnimation(ContinueBtn.transform);
    }
    
    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Localize();
      });
    }

    public void UpdateAdsWatchedProgress(int progress, int max) => 
      AdsWatchedProgress.text = $"{_localeSystem.GetText(LocalizeName, ShowAdsTextKey)} {progress + "/" + max}";
 
    public void UpdateDisableAdsTimer(int sec)
    {
      TimeSpan t = TimeSpan.FromSeconds( sec );
      string hours = t.Hours < 10 ? $"0{t.Hours}" : t.Hours.ToString();
      string minutes = t.Minutes < 10 ? $"0{t.Minutes}" : t.Minutes.ToString();
      string seconds = t.Seconds < 10 ? $"0{t.Seconds}" : t.Seconds.ToString();
      DisableAdsTimer.text = $"{hours}:{minutes}:{seconds}";
    }

    public void OpenWindow()
    {
      _analytics.Send("ad_offer_show");
      gameObject.SetActive(true);
      // SwitchSlotShadow(false);
    }

    public void CloseWindow()
    {
      gameObject.SetActive(false);
      // SwitchSlotShadow(true);
    }

    public void ShowInfoPanelBefore()
    {
      InfoPanelBefore.SetActive(true);
      InfoPanelAfter.SetActive(false);
      
      ShowBtn.gameObject.SetActive(true);
      CloseBtn.gameObject.SetActive(true);
      ContinueBtn.gameObject.SetActive(false);
    }
    
    public void ShowInfoPanelAfter()
    {
      InfoPanelBefore.SetActive(false);
      InfoPanelAfter.SetActive(true);
      
      ShowBtn.gameObject.SetActive(false);
      CloseBtn.gameObject.SetActive(false);
      ContinueBtn.gameObject.SetActive(true);
    }

    // private void SwitchSlotShadow(bool isOn) => 
      // FindObjectsOfType<SlotModelShadow>().ForEach(s => s.Switch(isOn, true));
  }
}