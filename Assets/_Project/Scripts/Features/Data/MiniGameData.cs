using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Features.MiniGamesScreen.Serialized;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.Features.Data
{
  [CreateAssetMenu(fileName = "Mini Game Data", menuName = "Data/Mini Game Data", order = 0)]
  public class MiniGameData : BaseContentData
  {
    public Sprite Icon;
    public MiniGameType Type;
    public bool isShowingBar;
    public List<BaseContentData> Levels;
    public bool UseLightFontColor = true;

    [Button]
    public void Shuffle()
    {
      var rnd = new System.Random();
      Levels = Levels.OrderBy(item => rnd.Next()).ToList();
    }
  }
}