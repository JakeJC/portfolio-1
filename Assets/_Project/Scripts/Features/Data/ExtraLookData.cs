using UnityEngine;

namespace _Project.Scripts.Features.Data
{
  [CreateAssetMenu(fileName = "Extra Look Data", menuName = "Data/Extra Look Data", order = 0)]
  public class ExtraLookData : ScriptableObject
  {
    public string SkinId;
    
    public Sprite DefaultStatic;
    public Sprite SpeakStatic;
  }
}