using System;
using UnityEngine;

namespace _Project.Scripts.Features.Data
{
  [CreateAssetMenu(fileName = "Stories Data", menuName = "Data/Stories Data", order = 0)]
  public class StoriesData : BaseContentData
  {
    public StoriesLink StoriesLink;
  }

  [Serializable]
  public struct StoriesLink
  {
    public Sprite Preview;
    public Sprite Image;
  }
}