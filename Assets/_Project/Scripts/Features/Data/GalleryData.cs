using System;
using _Project.Scripts.Features.GalleryScreen.Serialized;
using Spine.Unity;
using UnityEngine;

namespace _Project.Scripts.Features.Data
{
  [CreateAssetMenu(fileName = "Gallery Data", menuName = "Data/Gallery Data", order = 0)]
  public class GalleryData : BaseContentData
  {
    public GalleryType Type;
    public GalleryLink GalleryLink;
    
    public Vector2 PreviewPosition;
    public Vector2 PreviewScale = Vector2.one;
    
    public Vector2 ShowingScale = Vector2.one;

    public int SortPriority = 100;
  }
  
  [Serializable]
  public struct GalleryLink
  {
    public Sprite InBlur;
    public Sprite Preview;
    public Sprite Image;
    public SkeletonDataAsset SkeletonData;
  }
}