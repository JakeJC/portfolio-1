using _Project.Scripts.Features.ShopScreen.Data;
using UnityEngine;

namespace _Project.Scripts.Features.Data
{
  [CreateAssetMenu(fileName = "Girl Data", menuName = "Data/Girl Data", order = 0)]
  public class GirlData : BaseContentData
  {
    public Sprite Icon;
    public StoryData[] StoryDatas;
    public UpgradeData[] UpgradeDatas;
    public MiniGameData[] MiniGameDatas;
    public GalleryData[] GalleryDatas;
  }
}