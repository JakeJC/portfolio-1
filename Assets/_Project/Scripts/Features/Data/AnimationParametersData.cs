using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Features.Data
{
  public class AnimationParametersData : ScriptableObject
  {
    public float Delay;
    public float TimePlay;
    public Ease Curve;
  }
}