using _Project.Scripts.Features.StoriesScreen.DialogNodeSystem;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Data
{
  [CreateAssetMenu(fileName = "Story Data", menuName = "Data/Story Data", order = 0)]
  public class StoryData : BaseContentData
  {
    public string LocalePath;
    public DialogGraph DialogGraph;
    public Sprite Icon;
    public Sprite Icon_AB;
    public bool IsInDevelopment;
  }
}