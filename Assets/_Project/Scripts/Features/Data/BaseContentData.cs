using System;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Features.Data
{
  public abstract class BaseContentData : ScriptableObject
  {
    public string Id;
    public string Name;
    public string Description;
    
#if UNITY_EDITOR
    [Button]
    public void GenerateGuid() =>
      Id = Guid.NewGuid().ToString();
#endif
  }
}