using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.Features.Data
{
  [CreateAssetMenu(fileName = "Game Girls Data", menuName = "Data/Game Girls Data", order = 0)]
  public class GameGirlsData : ScriptableObject
  {
    public List<GirlData> GirlDatas;
  }
}