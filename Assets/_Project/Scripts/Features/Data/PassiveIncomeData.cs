using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.Features.Data
{
  [CreateAssetMenu(fileName = "Passive Income Data", menuName = "Data/Passive Income Data", order = 0)]
  public class PassiveIncomeData : ScriptableObject
  {
    public uint IncreasePercent;
    public List<BaseContentData> _baseContentsData;
  }
}