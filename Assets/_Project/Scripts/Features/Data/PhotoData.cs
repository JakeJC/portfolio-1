using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.Features.Data
{
  [CreateAssetMenu(fileName = "Photo Data", menuName = "Data/Photo Data", order = 0)]
  public class PhotoData : BaseContentData
  {
    public PhotoLink PhotoLink;
  }

  [Serializable]
  public struct PhotoLink
  {
    public Sprite Preview;
    public Sprite Image;
  }
}