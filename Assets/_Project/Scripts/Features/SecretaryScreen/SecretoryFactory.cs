using System;
using _Project.Scripts.Features.ScrollView;
using _Project.Scripts.Features.SecretaryScreen.Mono;
using _Project.Scripts.Services._Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UI_Particle_System.Scripts;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.SecretaryScreen
{
  public class SecretoryFactory : ISecretaryFactory
  {
    private const string SecretaryViewPath = "Features/SecretaryScreen/Secretary Window";

    private const string ItemsViewPath = "Features/SecretaryScreen/Secretary Scroll";
    private const string ItemButtonPath = "Features/SecretaryScreen/Secretary Item";
    private const string ShopSectionButtonVPath = "Features/SecretaryScreen/Secretary Section Button V";
    private const string ShopSectionButtonHPath = "Features/SecretaryScreen/Secretary Section Button H";

    private const string MoneyEffectPath = "Money";

    private const string BaseCanvas = "Canvas";

    private readonly IAssetProvider _assetProvider;
    private readonly IRemote _remote;

    public SecretoryFactory(IAssetProvider assetProvider, IRemote remote)
    {
      _assetProvider = assetProvider;
      _remote = remote;
    }

    public SecretaryView CreateSecretaryView()
    {
      Transform parent = GameObject.Find(BaseCanvas).transform;

      var homeScreen = _assetProvider.GetResource<SecretaryView>(SecretaryViewPath + (_remote.ShouldEnableFeatureTest() ? "-AB" : string.Empty));
      return Object.Instantiate(homeScreen, parent);
    }

    public ScrollViewSectionButton CreateViewSectionButton(Transform parent)
    {
      var homeScreen = _assetProvider.GetResource<ScrollViewSectionButton>(ShopSectionButtonVPath);
      return Object.Instantiate(homeScreen, parent);
    }

    public ScrollViewSectionButton CreateShopSectionButton(Transform parent)
    {
      var homeScreen = _assetProvider.GetResource<ScrollViewSectionButton>(ShopSectionButtonHPath);
      return Object.Instantiate(homeScreen, parent);
    }

    public ScrollViewItemsView CreateItemsView(Transform parent)
    {
      var homeScreen = _assetProvider.GetResource<ScrollViewItemsView>(ItemsViewPath + (_remote.ShouldEnableFeatureTest() ? "-AB" : string.Empty));
      return Object.Instantiate(homeScreen, parent);
    }

    public ScrollViewItemButton CreateItemButton(Transform parent)
    {
      var homeScreen = _assetProvider.GetResource<ScrollViewItemButton>(ItemButtonPath);
      return Object.Instantiate(homeScreen, parent);
    }

    public MoneyEffectPlayer CreateShopEffect(Transform parent)
    {
      MoneyEffectPlayer effectGO = _assetProvider.GetResource<MoneyEffectPlayer>(MoneyEffectPath);
      return Object.Instantiate(effectGO, parent);
    }
  }
}