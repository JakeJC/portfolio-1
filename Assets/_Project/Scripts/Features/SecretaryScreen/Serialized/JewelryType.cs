using UnityEngine;

namespace _Project.Scripts.Features.SecretaryScreen.Serialized
{
  public enum JewelryType
  {
    Glasses,
    Earrings,
    Necklace,
    Blouse,
    Tights,
    Skirt,
    Shoes
  }
}