using _Project.Scripts.Services._Interfaces;
using Spine.Unity;

namespace _Project.Scripts.Features.SecretaryScreen.Content
{
  public interface ISecretaryContentViewLogic
  {
    void Construct(IGameSaveSystem gameSaveSystem, IAssetProvider assetProvider, SkeletonAnimation skeletonAnimation);
    void Display();
  }
}