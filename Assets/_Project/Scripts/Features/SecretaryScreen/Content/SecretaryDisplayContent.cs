using _Project.Scripts.Features.SecretaryScreen.Data;

namespace _Project.Scripts.Features.SecretaryScreen.Content
{
  public class SecretaryDisplayContent
  {   
    private readonly SecretarySkinCombiner _skinCombiner;

    public SecretaryDisplayContent(SecretarySkinCombiner skinCombiner) => 
      _skinCombiner = skinCombiner;

    public void Display(JewelryItemData[] upgrades) => 
      _skinCombiner.ChangeSkin(upgrades);
  }
}