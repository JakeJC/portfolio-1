using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Features.SecretaryScreen.Data;
using _Project.Scripts.Features.ShopScreen.Content;
using _Project.Scripts.Services._Interfaces;
using Spine.Unity;

namespace _Project.Scripts.Features.SecretaryScreen.Content
{
  public class SecretaryContentViewLogic : ISecretaryContentViewLogic
  {
    private const string SecretaryItemsPath = "Features/Data/SecretaryData/Jewelry Items";
    
    private IGameSaveSystem _gameSaveSystem;
    private IAssetProvider _assetProvider;
    
    private JewelryItemData[] _upgrades;
    private SecretaryDisplayContent _lookDisplayContent;

    public void Construct(IGameSaveSystem gameSaveSystem, IAssetProvider assetProvider, SkeletonAnimation skeletonAnimation)
    {
      _assetProvider = assetProvider;
      _gameSaveSystem = gameSaveSystem;

      _upgrades = _assetProvider.GetAllResources<JewelryItemData>(SecretaryItemsPath);

      var skinCombiner = new SecretarySkinCombiner();
      skinCombiner.Construct(skeletonAnimation);

      _lookDisplayContent = new SecretaryDisplayContent(skinCombiner);
    }

    public void Display() => 
      Change(_upgrades.Where(p => _gameSaveSystem.Get().ChosenPassiveContent.Contains(p.Id)).ToArray());

    private void Change(JewelryItemData[] currentUpgradeData) =>
      _lookDisplayContent.Display(currentUpgradeData);
  }
} 