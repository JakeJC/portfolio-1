using System.Text;
using _Project.Scripts.Features.SecretaryScreen.Data;
using Spine;
using Spine.Unity;
using UnityEngine;
using Attachment = _Project.Scripts.Features.SecretaryScreen.Data.Attachment;

namespace _Project.Scripts.Features.SecretaryScreen.Content
{
  public class SecretarySkinCombiner
  {
    private SkeletonAnimation _skeletonAnimation;

    private JewelryItemData[] _currentUpgrades;

    public void Construct(SkeletonAnimation skeletonAnimation) =>
      _skeletonAnimation = skeletonAnimation;

    public void ChangeSkin(JewelryItemData[] upgrades)
    {
      _currentUpgrades = upgrades;
      CreateCombinedSkin();
    }

    private void CreateCombinedSkin()
    {
      if (_skeletonAnimation == null)
      {
        Debug.LogWarning("_skeletonGraphic is null");
        return;
      }

      Skeleton skeleton = _skeletonAnimation.Skeleton;
      skeleton.SetSlotsToSetupPose();

      foreach (JewelryItemData jewelryItemData in _currentUpgrades)
      {
        foreach (Attachment attachment in jewelryItemData.Attachments)
        {
          Slot slot = skeleton.FindSlot(attachment.SlotName);
          int slotIndex = slot.Data.Index;
          Spine.Attachment skinAttachment = skeleton.Data.FindSkin(jewelryItemData.SkinName).GetAttachment(slotIndex, attachment.AttachName);
          slot.Attachment = skinAttachment;
        }
      } 
      
      _skeletonAnimation.LateUpdate();
    }
  }
}