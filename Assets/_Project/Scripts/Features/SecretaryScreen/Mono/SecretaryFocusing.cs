using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Features.SecretaryScreen.Serialized;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Features.SecretaryScreen.Mono
{
  public class SecretaryFocusing : MonoBehaviour
  {
    [System.Serializable]
    public class Data
    {
      public JewelryType Type;
      public Vector2 TargetAnchoredPosition;
      public float TargetScale;
    }

    private const float FocusDuration = 0.5f;

    public List<Data> Datas;

    private RectTransform _rectTr;
    private Vector2 _defaultAnchoredPosition;
    private float _defaultScale;

    private JewelryType _targetType;
    
    private void Awake()
    {
      _rectTr = GetComponent<RectTransform>();
      _defaultAnchoredPosition = _rectTr.anchoredPosition;
      _defaultScale = _rectTr.localScale.x;
    }

    public void SetFocus(JewelryType type) => 
      _targetType = type;

    public void AnimateToTarget() 
    {
      Data data = Datas.FirstOrDefault(x => x.Type == _targetType);

      if (data == null)
      {
        AnimateToDefault();
        return;
      }

      _rectTr.DOScale(Vector3.one * data.TargetScale, FocusDuration);
      _rectTr.DOAnchorPos(data.TargetAnchoredPosition, FocusDuration);
    }

    public void AnimateToDefault() 
    {
      _rectTr.DOScale(Vector3.one * _defaultScale, FocusDuration);
      _rectTr.DOAnchorPos(_defaultAnchoredPosition, FocusDuration);
    }
  }
}