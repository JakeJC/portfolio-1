using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Animation;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using _Project.Scripts.Services.Localization.Mono;
using DG.Tweening;
using Spine.Unity;
using TMPro;
using UI_Particle_System.Scripts;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.SecretaryScreen.Mono
{
  public class SecretaryView : MonoBehaviour
  {
    private const string LocalizeName = "Main";

    private const float Duration = .3f;
    private const float ClearDuration = .5f;
    private const float AnimDuration = .5f;
    private const float ViewWindowWindowAnimStepX = 500f;
    private const float ShopWindowWindowAnimStepY = 1000f;
    private const float GirlSizeView = 50;
    private const float GirlSizeShop = 53;

    public SkeletonAnimation SecretaryGirl;

    public Button CloseShopButton;
    public Button BackButton;
    public Button GetButton;
    public Button GetX2Button;

    public Image ProgressInfo;
    public Image ProgressGlow;
    public TextMeshProUGUI IncomeInfo;
    public TextMeshProUGUI FactorInfo;
    public TextMeshProUGUI TakeX2;

    public Transform JewelryViewParent;
    public Transform JewelryShopParent;
    public ScrollRect JewelryShopScroll;
    public Transform ItemViewsParent;

    public CanvasGroup ProgressPanel;
    public CanvasGroup ShopPanel;

    public CanvasGroup ViewLeftPanel;
    public CanvasGroup ViewRightPanel;
    public CanvasGroup ShopMainPanel;

    public RectTransform Girl;
    public Animator JewelryAnimator;
    public SecretaryFocusing SecretaryFocusing;
    public Transform EffectParent;

    public GameObject SpeakBubble;
    public Button SpeakBubbleButton;
    public CanvasGroup SpeakBubblePanel;
    public TextMeshProUGUI SpeakBubbleMessageText;
    public TextMeshProUGUI SpeakBubbleButtonText;

    private Vector3 _viewLeftWindowDefaultPos;
    private Vector3 _viewRightWindowDefaultPos;
    private Vector3 _shopWindowDefaultPos;
    private IAudioService _audioService;
    private ILocaleSystem _localeSystem;
    private Func<MoneyEffectPlayer> _createEffectPlayer;

    private Action<uint> _onClickGet;

    private Coroutine _getButtonsCooldown;

    public void Construct(Action onClickBack, Action<uint> onClickGet, IAudioService audioService, ILocaleSystem localeSystem,
      Func<MoneyEffectPlayer> createEffectPlayer, Action onClickSpeakBubble)
    {
      _onClickGet = onClickGet;

      ProgressGlow.DOFade(0, 0.1f);
      ProgressInfo.fillAmount = 0;
      IncomeInfo.text = "0%";

      _createEffectPlayer = createEffectPlayer;
      _localeSystem = localeSystem;
      _audioService = audioService;

      SpeakBubbleButton.onClick.AddListener(() => onClickSpeakBubble?.Invoke());
      BackButton.onClick.AddListener(() => onClickBack?.Invoke());
      GetButton.onClick.AddListener(() => GetMoney(1));
      GetX2Button.onClick.AddListener(() => GetMoney(2));

      CloseShopButton.onClick.AddListener(CloseShop);

      CreateSounds();

      Localize(_localeSystem);

      CreateAnimation();

      _viewLeftWindowDefaultPos = ViewLeftPanel.GetComponent<RectTransform>().anchoredPosition;
      _viewRightWindowDefaultPos = ViewRightPanel.GetComponent<RectTransform>().anchoredPosition;
      _shopWindowDefaultPos = ShopMainPanel.GetComponent<RectTransform>().anchoredPosition;

      OpenView();
    }

    public void ShowSelectEffect(Action actionAfterEffect = null)
    {
      MoneyEffectPlayer effectPlayer = _createEffectPlayer?.Invoke();
      effectPlayer.Init(actionAfterEffect);
    }

    private void CreateSounds()
    {
      new ButtonSound(_audioService, CloseShopButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, BackButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, GetButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, GetX2Button.gameObject, AudioId.Karaoke_Button);
    }

    public void OpenShop()
    {
      if (ShopPanel.alpha > 0)
        return;

      ProgressPanel.DOFade(0f, Duration);
      ProgressPanel.blocksRaycasts = false;

      ShopPanel.DOFade(1f, Duration);
      ShopPanel.blocksRaycasts = true;

      JewelryAnimator.Rebind();
      JewelryAnimator.Update(0);
      ShopMainPanel.interactable = false;
      RectTransform rect = ShopMainPanel.GetComponent<RectTransform>();
      rect.anchoredPosition = _shopWindowDefaultPos - Vector3.up * ShopWindowWindowAnimStepY;
      rect.DOAnchorPos(_shopWindowDefaultPos, AnimDuration)
        .OnComplete(delegate { ShopMainPanel.interactable = true; });

      ViewLeftPanel.interactable = false;
      rect = ViewLeftPanel.GetComponent<RectTransform>();
      rect.anchoredPosition = _viewLeftWindowDefaultPos;
      rect.DOAnchorPos(_viewLeftWindowDefaultPos - Vector3.right * ViewWindowWindowAnimStepX, AnimDuration)
        .OnComplete(delegate { ViewLeftPanel.interactable = true; });

      ViewRightPanel.interactable = false;
      rect = ViewRightPanel.GetComponent<RectTransform>();
      rect.anchoredPosition = _viewRightWindowDefaultPos;
      rect.DOAnchorPos(_viewRightWindowDefaultPos + Vector3.right * ViewWindowWindowAnimStepX, AnimDuration)
        .OnComplete(delegate { ViewRightPanel.interactable = true; });

      Girl.DOScale(Vector3.one * GirlSizeShop, AnimDuration);

      SecretaryFocusing.AnimateToTarget();
    }

    private void CloseShop()
    {
      ProgressPanel.DOFade(1f, Duration);
      ProgressPanel.blocksRaycasts = true;

      ShopPanel.DOFade(0f, Duration);
      ShopPanel.blocksRaycasts = false;

      ShopMainPanel.interactable = false;
      RectTransform rect = ShopMainPanel.GetComponent<RectTransform>();
      rect.anchoredPosition = _shopWindowDefaultPos;
      rect.DOAnchorPos(_shopWindowDefaultPos - Vector3.up * ShopWindowWindowAnimStepY, AnimDuration)
        .OnComplete(delegate { ShopMainPanel.interactable = true; });

      ViewLeftPanel.interactable = false;
      rect = ViewLeftPanel.GetComponent<RectTransform>();
      rect.anchoredPosition = _viewLeftWindowDefaultPos - Vector3.right * ViewWindowWindowAnimStepX;
      rect.DOAnchorPos(_viewLeftWindowDefaultPos, AnimDuration)
        .OnComplete(delegate { ViewLeftPanel.interactable = true; });

      ViewRightPanel.interactable = false;
      rect = ViewRightPanel.GetComponent<RectTransform>();
      rect.anchoredPosition = _viewRightWindowDefaultPos + Vector3.right * ViewWindowWindowAnimStepX;
      rect.DOAnchorPos(_viewRightWindowDefaultPos, AnimDuration)
        .OnComplete(delegate { ViewRightPanel.interactable = true; });

      Girl.DOScale(Vector3.one * GirlSizeView, AnimDuration);

      SecretaryFocusing.AnimateToDefault();
    }

    private void OpenView()
    {
      GetButton.interactable = true;
      GetX2Button.interactable = true;

      ProgressPanel.alpha = 0;
      ProgressPanel.DOFade(1f, Duration);
      ProgressPanel.blocksRaycasts = true;

      ShopPanel.alpha = 0;
      ShopPanel.blocksRaycasts = false;

      ViewLeftPanel.interactable = false;
      RectTransform rect = ViewLeftPanel.GetComponent<RectTransform>();
      rect.anchoredPosition = _viewLeftWindowDefaultPos - Vector3.right * ViewWindowWindowAnimStepX;
      rect.DOAnchorPos(_viewLeftWindowDefaultPos, AnimDuration)
        .OnComplete(delegate { ViewLeftPanel.interactable = true; });

      ViewRightPanel.interactable = false;
      rect = ViewRightPanel.GetComponent<RectTransform>();
      rect.anchoredPosition = _viewRightWindowDefaultPos + Vector3.right * ViewWindowWindowAnimStepX;
      rect.DOAnchorPos(_viewRightWindowDefaultPos, AnimDuration)
        .OnComplete(delegate { ViewRightPanel.interactable = true; });
    }

    public void AddProgress(float income)
    {
      float targetValue = income * 0.01f;
      float animSpeed = 2.5f;
      ProgressInfo.DOFillAmount(targetValue, 1).OnComplete(delegate
        {
          if (targetValue > 0.99f)
            ProgressGlow.DOFade(1, 1);
          else
            ProgressGlow.DOFade(0, 1);
        })
        .OnUpdate(() => IncomeInfo.text = $"{(int)(ProgressInfo.fillAmount * 100)}%");
    }

    public void ClearPassiveProgress()
    {
      ProgressInfo.DOFillAmount(0f, 1).OnComplete(delegate { ShowSelectEffect(); });
      ProgressGlow.DOFade(0, 0.3f);
      IncomeInfo.text = "0%";
    }

    public void SetFactorInfo(float passiveIncome) =>
      FactorInfo.text = "Passive income +" + passiveIncome + "%";

    public void SetScrollView(float ratio)
    {
      if (ShopPanel.alpha > 0)
        return;

      JewelryShopScroll.horizontalNormalizedPosition = ratio;
    }

    public void ShowSpeakBubble(string messageText, string buttonText)
    {
      SpeakBubbleMessageText.text = messageText;
      SpeakBubbleButtonText.text = buttonText;

      SpeakBubble.SetActive(true);
      SpeakBubblePanel.alpha = 0;
      SpeakBubblePanel.DOFade(1, 0.3f);

      AddTempCanvas();

      void AddTempCanvas()
      {
        Canvas cnv = ProgressPanel.GetComponent<Canvas>();
        if (cnv == null)
        {
          ProgressPanel.gameObject.AddComponent<Canvas>();
          cnv = ProgressPanel.GetComponent<Canvas>();
        }

        cnv.overrideSorting = true;
        cnv.sortingOrder = -2;
      }
    }

    public void HideSpeakBubble()
    {
      SpeakBubble.SetActive(true);
      SpeakBubblePanel.alpha = 1;
      SpeakBubblePanel.DOFade(0, 0.3f)
        .OnComplete(delegate
        {
          SpeakBubble.SetActive(false);
          DestroyTempCanvas();
        });

      void DestroyTempCanvas()
      {
        Canvas cnv = ProgressPanel.GetComponent<Canvas>();
        if (cnv != null)
          DestroyImmediate(cnv);
      }
    }

    private void GetMoney(uint factor)
    {
      if (_getButtonsCooldown != null)
        return;

      _getButtonsCooldown = StartCoroutine(GetButtonsCooldown());

      _onClickGet?.Invoke(factor);
    }

    private IEnumerator GetButtonsCooldown()
    {
      GetButton.interactable = false;
      GetX2Button.interactable = false;

      yield return new WaitForSecondsRealtime(5);

      GetButton.interactable = true;
      GetX2Button.interactable = true;

      _getButtonsCooldown = null;
    }

    private void CreateAnimation()
    {
      new ButtonAnimation(BackButton.transform);
      new ButtonAnimation(GetButton.transform);
      new ButtonAnimation(GetX2Button.transform);
      new ButtonAnimation(CloseShopButton.transform);
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Localize();
      });

      TakeX2.text += " X2";
    }
  }
}