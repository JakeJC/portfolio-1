using System;
using _Project.Scripts.Services.Timer.Interfaces;
using _Project.Scripts.Timer.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.SecretaryScreen.Mono
{
  public class PassiveIncomeTimer : ICallbackExecutor
  {
    private const string TimerPassiveIncome = "PassiveIncomeTimerGuidSaveKey";
    private const int TimeDelay = 10000;
    // private const uint MoneyByMinute = 1;

    private Action<float> _onTick;

    /*private uint _moneyCapacity = 10000;*/

    private uint _ticksToMoney;
    private SecretaryLogic _secretaryLogic;

    private ITimerController _timerController;

    public void Construct(SecretaryLogic secretaryLogic, ITimerController timerController, Action<float> onTick)
    {
      _onTick = onTick;
      _secretaryLogic = secretaryLogic;
      _timerController = timerController;

      LaunchTimer();
    }

    public void RestartTimer()
    {
      if (_timerController.IsUnderTimer(GetTimerGuid()))
      {
        _timerController.RemoveCallback(this);
        _timerController.CompleteTimer(GetTimerGuid());
      }

      LaunchTimer();
    }

    public uint GetMoney() =>
      (uint) (_ticksToMoney * _secretaryLogic.PassiveFactor);

    public string GetTimerGuid()
    {
      string guid = PlayerPrefs.GetString(TimerPassiveIncome, Guid.NewGuid().ToString());
      PlayerPrefs.SetString(TimerPassiveIncome, guid);
      return guid;
    }

    public void ExecuteWhenTick(int seconds)
    {
      TimeSpan timeSpan = _timerController.GetSubtractValue(GetTimerGuid());
      var count = (uint) Mathf.RoundToInt((float) timeSpan.TotalMinutes);

      if (_ticksToMoney == TimeDelay - count) 
        return;

      _ticksToMoney = TimeDelay - count;
      _onTick?.Invoke(_ticksToMoney * _secretaryLogic.PassiveFactor);
    }

    public void ExecuteWhenComplete()
    {
    }

    public void SetLock(bool isLocked)
    {
    }

    public void Destroy()
    {
    }

    private void LaunchTimer()
    {
      AddTimerCallback();
      _timerController.CreateTimer(GetTimerGuid(), _timerController.TimeProvider.GetCurrentTime().AddMinutes(TimeDelay));
    }

    private void AddTimerCallback() =>
      _timerController.AddCallback(this);
  }
}