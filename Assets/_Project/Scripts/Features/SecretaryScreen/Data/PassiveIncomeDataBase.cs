using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Features.Data;
using UnityEngine;

namespace _Project.Scripts.Features.SecretaryScreen.Data
{
  [CreateAssetMenu(fileName = "Passive Income Data Base", menuName = "Data/Passive Income Data Base", order = 0)]
  public class PassiveIncomeDataBase : ScriptableObject
  {
    [SerializeField] private List<PassiveIncomeData> _passiveIncomesData;
    
    public uint GetIncreasePercent(string id)
    {
      return _passiveIncomesData.Where(data => data._baseContentsData.Any(item => item.Id == id))
        .Select(data => data.IncreasePercent)
        .FirstOrDefault();
    }
  }
}