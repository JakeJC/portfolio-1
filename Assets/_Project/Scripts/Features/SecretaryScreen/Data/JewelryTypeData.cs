using _Project.Scripts.Features.SecretaryScreen.Serialized;
using UnityEngine;

namespace _Project.Scripts.Features.SecretaryScreen.Data
{
  [CreateAssetMenu(fileName = "Jewelry PassiveType", menuName = "ScriptableObjects/Secretary/Jewelry PassiveType Data", order = 0)]
  public class JewelryTypeData : ScriptableObject
  {
    public JewelryType Type => _type;
    public string NameKey => _nameKey;
    public Vector3 Position => _position;
    public Sprite IconOn;
    public Sprite IconOff;
    
    [SerializeField] private JewelryType _type;
    [SerializeField] private string _nameKey;
    [SerializeField] private Vector3 _position;
  }
}