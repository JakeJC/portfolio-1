using System;
using System.Collections.Generic;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.SecretaryScreen.Serialized;
using UnityEngine;
using UnityEngine.Serialization;

namespace _Project.Scripts.Features.SecretaryScreen.Data
{
  [CreateAssetMenu(fileName = "Jewelry Item", menuName = "ScriptableObjects/Secretary/Jewelry Item Data", order = 0)]
  public class JewelryItemData : BaseContentData
  {
    public JewelryType PassiveType => passiveType;
    public Sprite Icon => _icon;
    public Sprite IconLock => _iconLock;
    public string SkinName => _skinName;
    public List<Attachment> Attachments => _attachments;

    public bool AB;

    [SerializeField] private JewelryType passiveType;
    [SerializeField] private Sprite _icon;
    [SerializeField] private Sprite _iconLock;
    
    [SerializeField] private string _skinName;
    [SerializeField] private List<Attachment> _attachments;
  }

  [Serializable]
  public struct Attachment
  {
    public string SlotName;
    public string AttachName;
  }
}