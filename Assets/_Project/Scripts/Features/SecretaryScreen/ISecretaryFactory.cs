using System;
using _Project.Scripts.Features.ScrollView;
using _Project.Scripts.Features.SecretaryScreen.Mono;
using UI_Particle_System.Scripts;
using UnityEngine;

namespace _Project.Scripts.Features.SecretaryScreen
{
  public interface ISecretaryFactory
  {
    SecretaryView CreateSecretaryView();
    ScrollViewSectionButton CreateViewSectionButton(Transform parent);
    ScrollViewSectionButton CreateShopSectionButton(Transform parent);
    ScrollViewItemsView CreateItemsView(Transform parent);
    ScrollViewItemButton CreateItemButton(Transform parent);
    MoneyEffectPlayer CreateShopEffect(Transform secretaryViewEffectParent);
  }
}