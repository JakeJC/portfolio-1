using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Core_Logic.Screens;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Core_Logic.StatesMachine.States;
using _Project.Scripts.Features.IapWindow;
using _Project.Scripts.Features.NotLoadedWindow;
using _Project.Scripts.Features.ScrollView;
using _Project.Scripts.Features.ScrollView.Serialized;
using _Project.Scripts.Features.SecretaryScreen.Content;
using _Project.Scripts.Features.SecretaryScreen.Data;
using _Project.Scripts.Features.SecretaryScreen.Mono;
using _Project.Scripts.Features.SecretaryScreen.Serialized;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using _Project.Scripts.Services.Money;
using _Project.Scripts.Services.Timer.Interfaces;
using _Project.Scripts.Services.Unlocker;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using Sirenix.Utilities;
using Spine.Unity;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.SecretaryScreen
{
  public class SecretaryLogic
  {
    private const string JewelryTypeDataPath = "Features/Data/SecretaryData/Jewelry Types/";
    private const string JewelryItemDataPath = "Features/Data/SecretaryData/Jewelry Items/";
    private const string PassiveIncomesDataPath = "Features/Data/SecretaryData/Passive Incomes Data/Passive Income Data Base";
    private const string LocaleKey = "Main";

    private readonly IUiFactory _uiFactory;
    private readonly ISecretaryFactory _secretaryFactory;

    private readonly IRemote _remote;
    private readonly IAnalytics _analytics;
    private readonly IAdsService _adsService;
    private readonly IMoneyService _moneyService;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;
    private readonly ICurrentScreen _currentScreen;
    private readonly IUnlockService _unlockService;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly ITimerController _timerController;
    private readonly IGameStateMachine _gameStateMachine;

#if !DISABLE_INAPPS
    private IapLogic _iapLogic;
#endif

    public float PassiveFactor { get; private set; } = 1f;

    private SecretaryView _secretaryView;

    private IMoneyCounterLogic _moneyCounterLogic;

    private readonly JewelryTypeData[] _jewelryTypesData;

    private readonly Dictionary<JewelryType, JewelryItemData[]> _itemsDataMap = new Dictionary<JewelryType, JewelryItemData[]>();
    private readonly Dictionary<JewelryType, ScrollViewItemsView> _jewelryItemsView = new Dictionary<JewelryType, ScrollViewItemsView>();
    private readonly Dictionary<JewelryType, ScrollViewItemButton[]> _jewelryItemButtons = new Dictionary<JewelryType, ScrollViewItemButton[]>();

    private readonly PassiveIncomeTimer _passiveIncomeTimer;
    private readonly PassiveIncomeDataBase _passiveIncomeDataBase;

    private readonly IAssetProvider _assetProvider;

    private Dictionary<JewelryType, ScrollViewSectionButton> _sectionButtons;
    private Dictionary<JewelryType, ScrollViewSectionButton> _sectionShopButtons;

    private ISecretaryContentViewLogic _secretaryContentViewLogic;

    private struct SpeakBubbleData
    {
      public string MessageText;
      public string ButtonText;

      public SpeakBubbleData(string messageText, string buttonText)
      {
        MessageText = messageText;
        ButtonText = buttonText;
      }
    }

    private int _speakBubbleDataIndex = 0;

    private readonly SpeakBubbleData[] _speakBubbleDatas =
    {
      new SpeakBubbleData("SkinIncreaseIncome", "Ok"),
      new SpeakBubbleData("Assistant.LeaveClothesOn", "Continue")
    };

    public SecretaryLogic(IGameStateMachine gameStateMachine, IAssetProvider assetProvider, ICurrentScreen currentScreen,
      IGameSaveSystem gameSaveSystem, IAdsService adsService, IAnalytics analytics, ILocaleSystem localeSystem,
      IAudioService audioService, IRemote remote, IUnlockService unlockService, IMoneyService moneyService, ITimerController timerController)
    {
      _assetProvider = assetProvider;
      _remote = remote;
      _analytics = analytics;
      _adsService = adsService;
      _moneyService = moneyService;
      _localeSystem = localeSystem;
      _audioService = audioService;
      _currentScreen = currentScreen;
      _unlockService = unlockService;
      _gameSaveSystem = gameSaveSystem;
      _timerController = timerController;
      _gameStateMachine = gameStateMachine;

      _uiFactory = new UiFactory(assetProvider, remote, localeSystem);
      _secretaryFactory = new SecretoryFactory(assetProvider, _remote);

      _passiveIncomeDataBase = assetProvider.GetResource<PassiveIncomeDataBase>(PassiveIncomesDataPath + (_remote.ShouldEnableFeatureTest() ? "-AB" : string.Empty));

      _jewelryTypesData = assetProvider.GetAllResources<JewelryTypeData>(JewelryTypeDataPath);
      JewelryItemData[] jewelryItemsData = assetProvider.GetAllResources<JewelryItemData>(JewelryItemDataPath);

      foreach (JewelryTypeData typeData in _jewelryTypesData)
      {
        JewelryItemData[] itemsData = jewelryItemsData.Where(item => item.PassiveType == typeData.Type).ToArray();
        _itemsDataMap.Add(typeData.Type, itemsData);
      }

      _passiveIncomeTimer = new PassiveIncomeTimer();

      _currentScreen.SetCurrentScreen(ScreenNames.Secretary);
    }
    
    private void IAPLazyInitialization()
    {
      if (_uiFactory.IapWindow == null)
      {
#if !DISABLE_INAPPS
        _iapLogic = new IapLogic(_assetProvider, _adsService, _uiFactory, _localeSystem, _unlockService, _moneyService,
          _gameSaveSystem, _audioService, _remote, _analytics);
#endif
      }
    }

    public void CreateSecretaryView()
    {
      _secretaryView = _secretaryFactory.CreateSecretaryView();
      _secretaryView.Construct(CloseSecretary, GetPassiveMoney, _audioService, _localeSystem,
        () => _secretaryFactory.CreateShopEffect(_secretaryView.EffectParent), SpeakBubbleClick);

      if (_remote.ShouldEnableFeatureTest() && _gameSaveSystem.Get().SecretarySpeakBubbleUsed == false)
      {
        SpeakBubbleClick();

        _gameSaveSystem.Get().SecretarySpeakBubbleUsed = true;
        _gameSaveSystem.Save();
      }

      CreateSecretaryShopContent();

      CheckUnlocked();

      CreateSounds(_secretaryView);

      SetNewPassiveFactor();

      _passiveIncomeTimer.Construct(this, _timerController, _secretaryView.AddProgress);

      _moneyCounterLogic = new MoneyCounterLogic(_assetProvider, _uiFactory, _localeSystem, _moneyService, _audioService, _adsService, _remote, _unlockService, _gameSaveSystem, _analytics);

      _secretaryContentViewLogic = new SecretaryContentViewLogic();
      _secretaryContentViewLogic.Construct(_gameSaveSystem, _assetProvider, _secretaryView.SecretaryGirl);
      _secretaryContentViewLogic.Display();

      OpenFree();
    }

    public void ClearSecretaryView()
    {
#if !DISABLE_INAPPS
      _iapLogic?.Clear();
#endif
      
      if (!_secretaryView) 
        return;

      _moneyCounterLogic.Clear();
      Object.Destroy(_secretaryView.gameObject);
    }

    private void OpenFree()
    {
      foreach (JewelryTypeData t in _jewelryTypesData)
      foreach (JewelryItemData item in _itemsDataMap[t.Type])
        if (!IsUnlocked(item.Id) && _unlockService.GetUnlockType(item.Id) == UnlockType.Empty)
          OnClickItem(item, true);
    }

    private void CreateSecretaryShopContent()
    {
      _sectionButtons = new Dictionary<JewelryType, ScrollViewSectionButton>();
      _sectionShopButtons = new Dictionary<JewelryType, ScrollViewSectionButton>();

      foreach (JewelryTypeData t in _jewelryTypesData)
      {
        ScrollViewSectionButton viewButton = _secretaryFactory.CreateViewSectionButton(_secretaryView.JewelryViewParent);
        ScrollViewSectionButton shopButton = _secretaryFactory.CreateShopSectionButton(_secretaryView.JewelryShopParent);

        ScrollViewItemsView scrollViewItemsView = _secretaryFactory.CreateItemsView(_secretaryView.ItemViewsParent);
        _jewelryItemsView.Add(t.Type, scrollViewItemsView);

        viewButton.Construct(_localeSystem, t.NameKey, delegate
        {
          _secretaryView.SecretaryFocusing.SetFocus(t.Type);
          UnselectSectionButtons();
          OpenShop(t.Type);
          SelectSectionButtons(t.Type);
        }, t.IconOn, t.IconOff);
        viewButton.SetPosition(t.Position);

        shopButton.Construct(_localeSystem, t.NameKey, delegate
        {
          _secretaryView.SecretaryFocusing.SetFocus(t.Type);
          _secretaryView.SecretaryFocusing.AnimateToTarget();
          UnselectSectionButtons();
          OpenShop(t.Type);
          SelectSectionButtons(t.Type);
        }, t.IconOn, t.IconOff);

        var jewelryItemButton = new List<ScrollViewItemButton>();
        foreach (JewelryItemData item in _itemsDataMap[t.Type])
        {
          if (item.AB && _remote.ShouldEnableFeatureTest() == false)
            continue;

          ScrollViewItemButton scrollViewItemButton = _secretaryFactory.CreateItemButton(scrollViewItemsView.ContentParent);

          string baseDescription = _localeSystem.GetText(LocaleKey, "PassiveIncome");
          string description = string.Format(baseDescription, _passiveIncomeDataBase.GetIncreasePercent(item.Id));

          scrollViewItemButton.Construct(_localeSystem, _unlockService,
            new ScrollViewItemData() { Id = item.Id, NameKey = item.Name, DescriptionKey = item.Description, Icon = item.Icon, IconLock = item.IconLock },
            () => OnClickItem(item, false), _audioService, description, _remote);
          scrollViewItemButton.SetName(_localeSystem.GetText(LocaleKey, item.PassiveType.ToString()) + " " + _localeSystem.GetText(LocaleKey, item.Name));

          jewelryItemButton.Add(scrollViewItemButton);
        }

        _jewelryItemButtons.Add(t.Type, jewelryItemButton.ToArray());

        _sectionButtons.Add(t.Type, viewButton);
        _sectionShopButtons.Add(t.Type, shopButton);
      }

      UnselectSectionButtons();
      SelectSectionButtons(JewelryType.Glasses);
    }

    private void SpeakBubbleClick()
    {
      if (_speakBubbleDataIndex == _speakBubbleDatas.Length)
      {
        _secretaryView.HideSpeakBubble();
        return;
      }

      var data = _speakBubbleDatas[_speakBubbleDataIndex];
      _speakBubbleDataIndex++;
      _secretaryView.ShowSpeakBubble(_localeSystem.GetText("Main", data.MessageText),
        _localeSystem.GetText("Main", data.ButtonText));
    }

    private void CheckUnlocked()
    {
      foreach (ScrollViewItemButton itemButton in GetAllItemButtons())
      {
        if (IsUnlocked(itemButton.GetId()))
        {
          itemButton.OpenContent(true);
          itemButton.SetLockButton(false);
        }

        if (IsChosen(itemButton.GetId()))
          itemButton.Activate();
        else
          itemButton.SetChooseState();
      }
    }

    private void OpenShop(JewelryType type)
    {
      _secretaryView.OpenShop();

      foreach (KeyValuePair<JewelryType, ScrollViewItemsView> item in GetDeactivatedItemsViews(type))
        item.Value.Deactivate();

      _jewelryItemsView[type].Activate();
    }

    private void SelectSectionButtons(JewelryType type)
    {
      _sectionShopButtons[type].Select(true);

      _secretaryView.SetScrollView((float)_sectionShopButtons.Values.ToList().IndexOf(_sectionShopButtons[type])
                                   / _sectionShopButtons.Count);
    }

    private void UnselectSectionButtons()
    {
      _sectionButtons.Values.ForEach(b => b.Select(false));
      _sectionShopButtons.Values.ForEach(b => b.Select(false));
    }

    private void OnClickItem(JewelryItemData itemData, bool onInit)
    {
      var unlockType = _unlockService.GetUnlockType(itemData.Id);
      
      if (IsUnlocked(itemData.Id))
      {
        foreach (ScrollViewItemButton item in _jewelryItemButtons[itemData.PassiveType])
        {
          if (item.GetId() == itemData.Id)
          {
            if (!IsChosen(item.GetId()))
            {
              item.Activate();
              _gameSaveSystem.Get().ChosenPassiveContent.Add(itemData.Id);

              if (!onInit)
              {
                _analytics.Send("ev_assist_selected", new Dictionary<string, object>
                {
                  { "category_id", itemData.PassiveType.ToString().ToLowerInvariant() },
                  { "object_id", itemData.Id }
                });
              }
            }
            else
            {
              item.Deactivate();
              _gameSaveSystem.Get().ChosenPassiveContent.Remove(itemData.Id);

              if (!onInit)
              {
                _analytics.Send("ev_assist_default", new Dictionary<string, object>
                {
                  { "category_id", itemData.PassiveType.ToString().ToLowerInvariant() }
                });
              }
            }
          }
          else
          {
            item.Deactivate();
            if (IsChosen(item.GetId()))
              _gameSaveSystem.Get().ChosenPassiveContent.Remove(item.GetId());
          }

          _gameSaveSystem.Save();
        }

        SetNewPassiveFactor();
      }
      else
      {
        if (unlockType == UnlockType.IAP)
        {
          IAPLazyInitialization();
          _iapLogic.OpenIapOffer(IapLogic.OfferLisaPackId, _secretaryView.transform, delegate(bool result)
          {
            if (result)
              CheckUnlocked();
          }, null);
        }
        else
          _unlockService.Unlock(itemData.Id, () =>
          {
            Unlock();
            GetItemButton(itemData)?.OpenContent();
            OnClickItem(itemData, false);
            SendOpenEvent(itemData);
          }, type =>
          {
            if (type == UnlockType.Money)
              _moneyCounterLogic.ShowAddMoneyIfNotEnough();
          });
      }

      _secretaryContentViewLogic.Display();

      void Unlock()
      {
        _gameSaveSystem.Get().UnlockedPassiveContent.Add(itemData.Id);
        _gameSaveSystem.Save();
      }
    }

    private void SendOpenEvent(JewelryItemData itemData)
    {
      if (_unlockService.GetUnlockType(itemData.Id) == UnlockType.Empty)
        return;

      _analytics.Send("ev_assist_open", new Dictionary<string, object>
      {
        { "category_id", itemData.PassiveType.ToString().ToLowerInvariant() },
        { "object_id", itemData.Id }
      });
    }

    private void SetNewPassiveFactor()
    {
      PassiveFactor = 1;

      foreach (string chosenItem in _gameSaveSystem.Get().ChosenPassiveContent)
        PassiveFactor += (_passiveIncomeDataBase.GetIncreasePercent(chosenItem) * 0.01f);

      _secretaryView.SetFactorInfo(PassiveFactor * 100);
    }

    private void GetPassiveMoney(uint factor)
    {
      uint passiveMoney = _passiveIncomeTimer.GetMoney();

      if (passiveMoney == 0)
        return;

      if (factor == 1)
      {
        TakePassiveReward();
        _analytics.Send("btn_get");
      }
      else
      {
        _adsService.Rewarded.ShowReward("passiveBonusX2", TakePassiveReward, () =>
        {
          INotLoadedWindowLogic notLoadedWindowLogic = new NotLoadedWindowLogic(_localeSystem, _assetProvider);
          notLoadedWindowLogic.Open();
        });
        _analytics.Send("btn_get_x2");
      }

      void TakePassiveReward()
      {
        float delay = 3;
        _moneyService.Earn(passiveMoney * factor, delay);
        _secretaryView.ClearPassiveProgress();
        _passiveIncomeTimer.RestartTimer();
      }
    }

    private void CloseSecretary()
    {
      if (_remote.ShouldEnableFeatureTest())
      {
        _gameStateMachine.Enter<LoadHomeState>();
        return;
      }

      _adsService.CurrentInterstitial.ShowInterstitial(ScreenNames.Home,
        _gameStateMachine.Enter<LoadHomeState>,
        _gameStateMachine.Enter<LoadHomeState>);
    }

    private void CreateSounds(SecretaryView secretaryView)
    {
      new ButtonSound(_audioService, secretaryView.BackButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, secretaryView.GetButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, secretaryView.GetX2Button.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, secretaryView.CloseShopButton.gameObject, AudioId.Karaoke_Button);
    }


    private ScrollViewItemButton GetItemButton(JewelryItemData itemData) =>
      _jewelryItemButtons[itemData.PassiveType].FirstOrDefault(item => item.GetId() == itemData.Id);

    private IEnumerable<ScrollViewItemButton> GetAllItemButtons() =>
      _jewelryItemButtons.SelectMany(item => item.Value);

    private IEnumerable<KeyValuePair<JewelryType, ScrollViewItemsView>> GetDeactivatedItemsViews(JewelryType type) =>
      _jewelryItemsView.Where(item => item.Key != type);

    private bool IsUnlocked(string itemId) =>
      _gameSaveSystem.Get().UnlockedPassiveContent.Contains(itemId);

    private bool IsChosen(string itemId) =>
      _gameSaveSystem.Get().ChosenPassiveContent.Contains(itemId);
  }
}