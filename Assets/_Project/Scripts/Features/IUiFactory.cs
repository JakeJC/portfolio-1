using System;
using _Project.Scripts.Features.DisableAds.Mono;
using _Project.Scripts.Features.IapWindow.Data;
using _Project.Scripts.Features.IapWindow.Mono;
using _Project.Scripts.Features.Lose.Mono;
using _Project.Scripts.Features.ScrollView;
using _Project.Scripts.Features.SettingsWindow;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Money.Mono;
using UnityEngine;

namespace _Project.Scripts.Features
{
  public interface IUiFactory
  {
    // LoadWindow Loading { get; }
    IapView IapWindow { get; }
    DisableAdsWindow DisableAdsWindow { get; }
    Settings SettingsWindow { get; }

    // NoInternet NoInternetWindow { get; }
    // LoadWindow CreateLoadWindow();
    MoneyCounter SpawnMoneyCounter();
    AddMoneyView SpawnAddMoneyWindow();
    void SpawnDisableAdsWindow();
    void SpawnIapWindow(Action onOpenWindow, Action<IapDailyRewardData> onDailyRewardTake, ILocaleSystem localeSystem);
    void SpawnSettingsWindow();
    void SpawnNoInternetConnection();
    void ClearSettingsWindow();
    void ClearDisableAdsWindow();
    ScrollViewIapItemButton CreateIapItemSmallButton(Transform iapWindowContentParent);
    ScrollViewIapItemButton CreateIapItemLargeButton(Transform iapWindowContentParent);
    void ClearMoneyCounter();
    IapOfferView CreateOfferLisaMiniBikini(Transform contentParent); 
    IapOfferView CreateOfferJoy(Transform contentParent);
  }
}