using System;
using _Project.Scripts.Animation;
using _Project.Scripts.Features.IapWindow.Serialized;
using _Project.Scripts.Features.ScrollView.Serialized;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Localization.Logic;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace _Project.Scripts.Features.ScrollView
{
  public class ScrollViewIapItemButton : ScrollViewItemButton
  {
    private const string MainLocalePath = "Main";

    private IUnlockService _unlockService;

    public TextMeshProUGUI AdditionalInfo;
    public TextMeshProUGUI DescriptionMain;
    public TextMeshProUGUI DescriptionAdditional;
    public Image DescriptionIcon;

    public override void Construct(ILocaleSystem localeSystem, IUnlockService unlockService,
      ScrollViewItemData scrollViewItemData,
      Action onClickItem, IAudioService audioService, string description, IRemote remote)
    {
      LocaleSystem = localeSystem;
      AudioService = audioService;

      _unlockService = unlockService;
      ScrollViewItemData = scrollViewItemData;

      SetIcon();
      SetDescription(description);

      Button.onClick.AddListener(() => onClickItem());

      void SetIcon() =>
        Icon.sprite = ScrollViewItemData.Icon;

      new ButtonAnimation(transform);

      if (remote.ShouldEnableFeatureTest())
      {
        if (AdditionalInfo)
        {
          if (string.IsNullOrEmpty(scrollViewItemData.AdditionalNameKey) == false)
            AdditionalInfo.text = scrollViewItemData.AdditionalNameKey;
          else
            AdditionalInfo.transform.parent.gameObject.SetActive(false);
        }

        if (DescriptionMain)
          DescriptionMain.text = localeSystem.GetText("Main", scrollViewItemData.DescriptionKey);

        if (DescriptionAdditional)
        {
          if (string.IsNullOrEmpty(scrollViewItemData.DescriptionAdditional))
            DescriptionAdditional.text = string.Empty;
          else
            DescriptionAdditional.text = localeSystem.GetText("Main", scrollViewItemData.DescriptionAdditional);
        }

        if (DescriptionIcon)
        {
          DescriptionIcon.sprite = scrollViewItemData.DescriptionIcon;
          DescriptionIcon.SetNativeSize();
        }
      }
    }

    public override void OpenContent(bool fromInit = false)
    {
      base.OpenContent(fromInit);

      Icon.DOFade(1f, Duration);
      Description.DOFade(0f, Duration);
    }

    public void SetLock(UnlockIapType type)
    {
      if (type != UnlockIapType.Soon && type != UnlockIapType.Soon_2)
      {
        Icon.enabled = true;
        Description.text = String.Empty;
      }
      else if (type == UnlockIapType.Soon_2)
      {
        Icon.enabled = true;
        Icon.SetNativeSize();
        Description.text = String.Empty;
        Icon.GetComponent<RectTransform>().anchoredPosition += Vector2.down * 20;
        return;
      }
      else
        return;

      SetBuyZoneDefaults(Buy.BoughtGroup);
      SetBuyZoneDefaults(Buy.AdvGroup);
      SetBuyZoneDefaults(Buy.MoneyGroup);
      SetBuyZoneDefaults(Buy.ChooseGroup);
      SetBuyZoneDefaults(Buy.IAPGroup);

      ActiveBuyingGroup = type == UnlockIapType.Iap ? Buy.MoneyGroup : Buy.AdvGroup;
      ActiveBuyingGroup.DOFade(1f, Duration);
    }

    public void SetCost(string cost) =>
      Buy.Cost.text = cost;

    protected override void SetDescription(string description)
    {
      if (Name != null)
        Name.text = LocaleSystem.GetText(MainLocalePath, ScrollViewItemData.NameKey) == null
          ? ScrollViewItemData.NameKey
          : LocaleSystem.GetText(MainLocalePath, ScrollViewItemData.NameKey);

      if (Description != null)
        Description.text = LocaleSystem.GetText(MainLocalePath, ScrollViewItemData.DescriptionKey) == null
          ? ScrollViewItemData.DescriptionKey
          : LocaleSystem.GetText(MainLocalePath, ScrollViewItemData.DescriptionKey);
    }

    public void PurchaseOn(bool fromInit = false)
    {
      Debug.Log("Bought");

      PlayOpenContentSound(fromInit);

      Activate();
    }
  }
}