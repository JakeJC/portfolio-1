using System;
using _Project.Scripts.Animation;
using _Project.Scripts.Features.ScrollView.Serialized;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _Project.Scripts.Features.ScrollView
{
  public class ScrollViewMiniGamesItemButton : ScrollViewItemButton
  {
    private static readonly Color32 ColorLight = new Color32(255, 255, 255, 255);
    private static readonly Color32 ColorDark = new Color32(68, 68, 68, 255);
    
    public Image ProgressBar;
    public GameObject IconPhoto;
    public GameObject Text;
    
    public TextMeshProUGUI Progress;

    public override void Construct(ILocaleSystem localeSystem, IUnlockService unlockService, ScrollViewItemData scrollViewItemData,
      Action onClickItem, IAudioService audioService, string description, IRemote remote)
    {
      LocaleSystem = localeSystem;
      AudioService = audioService;
      ScrollViewItemData = scrollViewItemData;

      Button.onClick.AddListener(() =>
      {
        onClickItem();
      });

      SetIcon();
      SetDescription(description);
  
      //new ButtonAnimation(Button.transform);
      
      void SetIcon() =>
        Icon.sprite = ScrollViewItemData.Icon;
    }

    public void SwitchFontColor(bool useLightFontColor)
    {
      Color32 color = useLightFontColor 
        ? ColorLight 
        : ColorDark;

      if(Name)
        Name.color = color;
      
      if(Description)
        Description.color = color;

      if (Progress)
        Progress.color = color;
    }

    public void SetProgress(float percent)
    {
      ProgressBar.transform.parent.parent.gameObject.SetActive(true);
      ProgressBar.DOFillAmount(percent * .01f, Duration);
    }

    public void HideAllElements()
    {
      IconPhoto.SetActive(false);
      Text.SetActive(false);
    }
    
    protected override void SetDescription(string description)
    {
      Description.text = LocaleSystem.GetText("Main", ScrollViewItemData.NameKey);
    }
  }
}