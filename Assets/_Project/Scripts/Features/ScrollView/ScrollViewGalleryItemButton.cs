using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Features.ScrollView
{
  
  public class ScrollViewGalleryItemButton : ScrollViewItemButton
  {
    public CanvasGroup LiveGroup;
    public RectTransform IconTransform;

    public bool IsStories
    {
      set => LiveGroup.alpha = value ? 1f : 0f;
    }

    public override void OpenContent(bool fromInit = false)
    {
      base.OpenContent(fromInit);
      Icon.DOFade(1f, Duration);
    }

    public void SetPreviewRectangle(Vector2 position, Vector2 scale)
    {
      IconTransform.anchoredPosition = position;
      IconTransform.localScale = scale;
    }
  }
}