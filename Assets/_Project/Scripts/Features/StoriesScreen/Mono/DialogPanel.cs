using System;
using System.Collections.Generic;
using _Project.Scripts.Animation;
using _Project.Scripts.Features.StoriesScreen.Serialized;
using _Project.Scripts.Services.Unlocker;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.StoriesScreen.Mono
{
  public class DialogPanel : MonoBehaviour
  {
#if TEST_DIALOG
      private const float Duration = 0f;
#else
      private const float Duration = .3f;
#endif
    
    public TextMeshProUGUI Message;
    public CanvasGroup CanvasGroup;

    public RoleType RoleType;

    [Space(15)] public List<Button> Buttons;

    [Space(15)] public List<LockPart> LockParts;

    private readonly List<CanvasGroup> _buttonCanvasGroups = new List<CanvasGroup>();
    private readonly List<TextMeshProUGUI> _buttonTexts = new List<TextMeshProUGUI>();

    private readonly Dictionary<UnlockType, Action<int>> _clickActions = new Dictionary<UnlockType, Action<int>>();
    
    private IRemote _remote;

    public void Construct(Action<int> onClickVariant, Action<int> onClickVariantAd, Action<int> onClickVariantMoney, IRemote remote)
    {
      _remote = remote;
      _clickActions.Add(UnlockType.Empty, onClickVariant);
      // TODO: при закрепе этого AB оставить "else", а в Girl 00N Story 00N заменить UnlockType на "Money"
      if (remote.ShouldEnableFeatureTest())
        _clickActions.Add(UnlockType.Advertising, onClickVariantMoney);
      else
        _clickActions.Add(UnlockType.Advertising, onClickVariantAd);
      _clickActions.Add(UnlockType.Money, onClickVariantMoney);

      Buttons.ForEach(item =>
      {
        _buttonCanvasGroups.Add(item.GetComponent<CanvasGroup>());
        _buttonTexts.Add(item.GetComponentInChildren<TextMeshProUGUI>());

        new ButtonAnimation(item.transform);
      });
    }

    public void SetLockAnswers(DialogOutput outputValue)
    {
      for (var i = 0; i < LockParts.Count; i++)
      {
        if (outputValue.AnswerTypes != null && i < outputValue.AnswerTypes.Count)
          LockParts[i].SetLockPart(outputValue.AnswerTypes[i], outputValue.MoneyUnlockAmount, _remote);
        else
          LockParts[i].Hide();
      }
    }

    public void SetButtonListeners(List<UnlockType> type)
    {
      Buttons.ForEach(item =>
      {
        item.onClick.RemoveAllListeners();
        item.onClick.AddListener(() =>
        {
          _clickActions[type.Count > 0 ? type[Buttons.IndexOf(item)] : UnlockType.Empty]?.Invoke(Buttons.IndexOf(item));
        });
      });
    }

    public void Show(string textMessage)
    {
      CanvasGroup.DOFade(1f, Duration)
        .SetDelay(Duration)
        .OnStart(() => Message.text = textMessage);
    }

    public void Hide()
    {
      CanvasGroup.DOFade(0f, Duration);
      HideButtons();
    }
    
    public void PreStart() //Stick
    {
      CanvasGroup.alpha = 0;
      _buttonCanvasGroups.ForEach(item =>
      {
        CanvasGroup.blocksRaycasts = false;
        item.alpha = 0;
      });
    }

    public void SetAnswerOptions(List<string> answerOptions)
    {
      if (answerOptions == null)
        return;

      for (var i = 0; i < answerOptions.Count; i++)
      {
        CanvasGroup.blocksRaycasts = true;
        _buttonCanvasGroups[i].DOFade(1f, Duration)
          .SetDelay(Duration);;
        _buttonTexts[i].text = answerOptions[i];
      }
    }

    private void HideButtons()
    {
      _buttonCanvasGroups.ForEach(item =>
      {
        CanvasGroup.blocksRaycasts = false;
        item.DOFade(0, Duration);
      });
    }
  }
}