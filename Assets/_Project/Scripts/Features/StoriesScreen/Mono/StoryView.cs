using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Animation;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Mono;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.StoriesScreen.Mono
{
  public class StoryView : MonoBehaviour
  {
    public Button BackButton;
    
    public Transform SectionsParent;
    public Transform ItemViewsParent;
    
    public Image Girl;

    public StoryWorkInProcessView WorkInProcessView;

    public void Construct(IRemote remote, ILocaleSystem localeSystem, Action onCloseStories, Action onWorkInProcessTake, uint income)
    {
      BackButton.onClick.AddListener(() => onCloseStories());

      CreateAnimation();
      
      Localize(localeSystem);
      
      if(remote.ShouldEnableFeatureTest())
        WorkInProcessView.Construct(localeSystem, onWorkInProcessTake, income);
    }
    
    public void SetGirlView(Sprite girlSprite) => 
      Girl.sprite = girlSprite;

    private void CreateAnimation()
    {
      new ButtonAnimation(BackButton.transform);
    }
    
    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Localize();
      });
    }
  }
}