using System.Collections.Generic;
using _Project.Scripts.Services.Unlocker;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace _Project.Scripts.Features.StoriesScreen.Mono
{
  public class LockPart : MonoBehaviour
  {
    private const float Duration = .3f;

    public CanvasGroup LockAd;
    public CanvasGroup LockMoney;
    public TextMeshProUGUI LockText;

    public void SetLockPart(UnlockType type, uint moneyUnlockAmount, IRemote remote)
    {
      if (type == UnlockType.Empty)
        return;

      gameObject.SetActive(true);

      CanvasGroup lockPart;

      // TODO: при закрепе этого AB оставить "else", а в Girl 00N Story 00N заменить UnlockType на "Money"
      if (remote.ShouldEnableFeatureTest())
        lockPart = LockMoney;
      else
        lockPart = type == UnlockType.Advertising ? LockAd : LockMoney;

      lockPart.DOFade(1f, Duration);
      LockText.text = moneyUnlockAmount.ToString();

      LockText.enableAutoSizing = true;
    }

    public void Hide()
    {
      gameObject.SetActive(false);

      LockAd.alpha = 0f;
      LockMoney.alpha = 0f;
    }
  }
}