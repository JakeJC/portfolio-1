using _Project.Scripts.Services._Interfaces;
using TMPro;
using UnityEngine;

namespace _Project.Scripts.Features.StoriesScreen.Mono
{
  public class PicturesCounterView : MonoBehaviour
  {
    [SerializeField] private TextMeshProUGUI _counter;
    
    private ILocaleSystem _localeSystem;
    private bool _changeToTextIfMaximum;

    public void Construct(ILocaleSystem localeSystem, bool changeToTextIfMaximum)
    {
      _changeToTextIfMaximum = changeToTextIfMaximum;
      _localeSystem = localeSystem;
    }

    public void SetOpenCount(int count, int maxCount)
    {
      _counter.text = count == maxCount && _changeToTextIfMaximum? 
        _localeSystem.GetText("Main", "New.Photos") :
        $"{count}/{maxCount}";
    }
  }
}