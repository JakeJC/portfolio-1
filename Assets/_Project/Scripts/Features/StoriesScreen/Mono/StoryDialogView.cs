using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Animation;
using _Project.Scripts.Features.GalleryScreen.Serialized;
using _Project.Scripts.Features.StoriesScreen.Serialized;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Mono;
using _Project.Scripts.Services.Money;
using AppacheAds.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;
using Spine.Unity;
using TMPro;
using UI_Particle_System.Scripts;
using UnityEngine;
using UnityEngine.UI;
using Vector3 = UnityEngine.Vector3;

namespace _Project.Scripts.Features.StoriesScreen.Mono
{
  public class StoryDialogView : MonoBehaviour
  {
#if TEST_DIALOG
    private const float Duration = 0f;
#else
    private const float Duration = .3f;
#endif

    private const float DurationNextAdFilling = 1f;

    public Image Girl;
    public CanvasGroup GirlVisualGroup;
    public TextMeshProUGUI TalkingName;

    public Button CloseButton;
    public Button NextButton;

    public Image NextAd;
    public Image NextArrow;
    public Image NextTriangle;

    public CanvasGroup NextButtonGroup;

    public Image HerPhoto;
    public SkeletonGraphic SkeletonAnimation;

    public CanvasGroup PhotoGroup;
    public CanvasGroup StoriesGroup;

    public CanvasGroup DialogGroup;
    public List<DialogPanel> DialogPanels;

    [Space(20)] [Header("For test")] public Button PreviousTest;
    public Button NextTest;
    public GameObject Test;

    public Image PhotoBackground;
    public Image StoryBackground;

    public GameObject OpenEffect;
    public Image Vignette;

    private Action<string> _onSavePhoto;
    private Action<int> _onClickNext;
    private Action<int> _onClickNextAd;

    private int _adCounter = 0;

    private ILocaleSystem _localeSystem;
    private IMoneyCounterLogic _moneyCounterLogic;
    private IRemote _remote;
    
    private readonly Dictionary<RoleType, DialogPanel> _dialogPanels = new Dictionary<RoleType, DialogPanel>();
    private DialogOutput _outputValue;

    public void Construct(IAdsService adsService, IMoneyCounterLogic moneyCounterLogic, ILocaleSystem localeSystem, IRemote remote, Action<int> onClickNext, Action<int> onClickNextAd,
      Action<int> onClickVariantAd, Action<int> onClickVariantMoney,
      Action<string> onSavePhoto, Action onClickBack)
    {
      _adsService = adsService;
      _moneyCounterLogic = moneyCounterLogic;
      _remote = remote;
      
      _onClickNext = onClickNext;
      _onClickNextAd = onClickNextAd;

      _onSavePhoto = onSavePhoto;
      _localeSystem = localeSystem;

      CloseButton.onClick.AddListener(() => onClickBack?.Invoke());
      NextButton.onClick.AddListener(() => onClickNext?.Invoke(0));

      foreach (DialogPanel t in DialogPanels)
      {
        t.Construct(onClickNext, onClickVariantAd, onClickVariantMoney, _remote);
        _dialogPanels.Add(t.RoleType, t);
      }

      CreateAnimations();
      Localize(_localeSystem);
    }

    public void SetTalkingName(string actualName)
    {
      if (TalkingName != null)
        TalkingName.text = actualName;
    }

    public void Show()
    {
      DialogGroup.DOFade(1f, Duration);
      DialogGroup.blocksRaycasts = true;
    }

    public void Hide()
    {
      DialogGroup.DOFade(0f, Duration);
      DialogGroup.blocksRaycasts = false;
    }

    public void ShowNextStage(string localePath, DialogOutput outputValue, Sprite spriteState, string newName)
    {
      _outputValue = outputValue;

      foreach (KeyValuePair<RoleType, DialogPanel> item in _dialogPanels)
        item.Value.Hide();

      HideNextGroup();

      SetState(outputValue.RoleType, spriteState, newName);

      if (CheckContent(outputValue))
      {
        CheckAnswerOptions(localePath, outputValue);
        return;
      }

      //Stick
      StopAllCoroutines();
      StartCoroutine(Waiting(outputValue));

      _dialogPanels[outputValue.RoleType].Show(_localeSystem.GetText(localePath, outputValue.TextKey));
      
      CheckAnswerOptions(localePath, outputValue);
    }

    private IEnumerator Waiting(DialogOutput outputValue)
    {
      yield return new WaitForSeconds(Duration);
      foreach (KeyValuePair<RoleType, DialogPanel> item in _dialogPanels.Where(item => item.Value.RoleType != outputValue.RoleType))
        item.Value.PreStart();
    }

    private RoleType? _previousRole;
    private IAdsService _adsService;

    private void SetState(RoleType role, Sprite sprite, string newName)
    {
      if (_previousRole != role || _previousRole == null)
      {
        Sequence seq = DOTween.Sequence();
     
        seq.Append(GirlVisualGroup.DOFade(0, Duration));
        seq.AppendCallback(() =>
        {
          Girl.sprite = sprite;
          SetTalkingName(newName);
        });
       
        if(role != RoleType.Description)
          seq.Append(GirlVisualGroup.DOFade(1, Duration));

        seq.Play();
        
        _previousRole = role;
      }
    }

    private void ContentOpenEffect()
    {
      OpenEffect.SetActive(true);

      Color defaultColor = Vignette.color;
      defaultColor.a = 0;
      Vignette.color = defaultColor;

      Vignette.DOFade(1, 1f).OnComplete(delegate { Vignette.DOFade(0, 1f).OnComplete(() => OpenEffect.SetActive(false)); });
    }

    private bool CheckContent(DialogOutput outputValue)
    {
      if (outputValue.Photo != null)
      {
        _moneyCounterLogic.ShowMoneyCounter(false);
        ContentOpenEffect();

        var group = outputValue.Photo.Type == GalleryType.Photo ? PhotoGroup : StoriesGroup;
        var transformContent = outputValue.Photo.Type == GalleryType.Photo
          ? HerPhoto.transform
          : SkeletonAnimation.transform;

        transformContent.DOScale(1f, Duration)
          .OnStart(() =>
          {
            transformContent.localScale = Vector3.zero;

            if (outputValue.Photo.Type == GalleryType.Photo)
            {
              HerPhoto.sprite = outputValue.Photo.GalleryLink.Image;

              PhotoBackground.sprite = outputValue.Photo.GalleryLink.InBlur;
              PhotoBackground.color = Color.white;
            }
            else
            {
              SkeletonAnimation.skeletonDataAsset = outputValue.Photo.GalleryLink.SkeletonData;
              SkeletonAnimation.Initialize(true);

              StoryBackground.sprite = outputValue.Photo.GalleryLink.InBlur;
              StoryBackground.color = Color.white;
            }
          })
          .SetEase(Ease.Linear);
        group.DOFade(1f, Duration);

        _onSavePhoto?.Invoke(outputValue.Photo.Id);
        return true;
      }

      if (PhotoGroup.alpha > 0)
        PhotoGroup.DOFade(0f, Duration);
      if (StoriesGroup.alpha > 0)
        StoriesGroup.DOFade(0f, Duration);

      return false;
    }

    private void CheckAnswerOptions(string localePath, DialogOutput outputValue)
    {
      var localeAnswers = outputValue.AnswerOptions?.Select(item => _localeSystem.GetText(localePath, item)).ToList();
      _dialogPanels[outputValue.RoleType].SetAnswerOptions(localeAnswers);

      _dialogPanels[outputValue.RoleType].SetLockAnswers(outputValue);
      _dialogPanels[outputValue.RoleType].SetButtonListeners(outputValue.AnswerTypes);

      if (_remote.ShouldEnableFeatureTest())
      {
        ShowNextButtonGroup(outputValue.AnswerOptions == null);
        return;
      }

      ShowNextButtonGroup(outputValue.AnswerOptions == null, (++_adCounter - 5) % 10 == 0);
    }

    private void ShowNextButtonGroup(bool isShow, bool isAd = false)
    {
      NextButtonGroup.DOFade(isShow ? 1f : 0f, Duration)
        .SetDelay(Duration)
        .OnComplete(() => NextButtonGroup.blocksRaycasts = isShow);

      if (isAd && _adsService.AdsEnabled)
        ShowNextAd();
    }

    private void HideNextGroup()
    {
      NextButtonGroup.blocksRaycasts = false;
      NextButtonGroup.DOFade(0f, Duration);

      HideNextAdGroup();
    }

    private void HideNextAdGroup()
    {
      NextButton.onClick.RemoveAllListeners();
      NextButton.onClick.AddListener(() => _onClickNext?.Invoke(0));

      NextAd.DOKill();
      NextAd.fillAmount = 1f;
      NextArrow.DOFade(1, 0);
      NextTriangle.DOFade(0, 0);
    }

    private void ShowNextAd()
    {
      NextAd.fillAmount = 0;
      NextAd.DOFillAmount(1f, DurationNextAdFilling)
        .SetDelay(Duration)
        .SetEase(Ease.Linear)
        .OnStart(() =>
        {
          NextButton.onClick.RemoveAllListeners();
          NextButton.onClick.AddListener(() => _onClickNextAd?.Invoke(0));

          NextArrow.DOFade(0, 0);
          NextTriangle.DOFade(1, 0);
        })
        .OnComplete(HideNextAdGroup);
    }

    private void CreateAnimations()
    {
      new ButtonAnimation(CloseButton.transform);
      new ButtonAnimation(NextButton.transform);
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Localize();
      });
    }

    public void ShowTest(Action<int> next, Action<int> previous)
    {
      Test.SetActive(true);
      NextTest.onClick.AddListener(() => next?.Invoke(0));
      PreviousTest.onClick.AddListener(() => previous?.Invoke(0));
    }

    public void ShowNextStageTest(string localePath, DialogOutput outputValue, Sprite spriteState, string newName)
    {
      _outputValue = outputValue;
      _adCounter = 0;

      foreach (KeyValuePair<RoleType, DialogPanel> item in _dialogPanels)
        item.Value.Hide();

      if (CheckContent(outputValue))
      {
        CheckAnswerOptions(localePath, outputValue);
        return;
      }

      _dialogPanels[outputValue.RoleType].Show(_localeSystem.GetText(localePath, outputValue.TextKey));
      CheckAnswerOptions(localePath, outputValue);
      SetState(outputValue.RoleType, spriteState, newName);
    }

    public void ShowPreviousStageTest(string localePath, DialogOutput outputValue, Sprite spriteState, string newName)
    {
      _adCounter = 0;

      foreach (KeyValuePair<RoleType, DialogPanel> item in _dialogPanels)
        item.Value.Hide();
      if (CheckContent(outputValue))
      {
        CheckAnswerOptions(localePath, outputValue);
        return;
      }

      _dialogPanels[outputValue.RoleType].Show(_localeSystem.GetText(localePath, outputValue.TextKey));
      CheckAnswerOptions(localePath, outputValue);
      SetState(outputValue.RoleType, spriteState, newName);
    }

    private void OnGUI()
    {
#if TEST_DIALOG
      GUIStyle guiStyle = new GUIStyle();
      guiStyle.fontSize = 32;
      guiStyle.normal.textColor = Color.white;
      GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, 300, 100), _outputValue.TextKey, guiStyle);
#endif
    }
  }
}