using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Animation;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Mono;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.StoriesScreen.Mono
{
  public class StoryWorkInProcessView : MonoBehaviour
  {
    public TextMeshProUGUI Income;
    
    public Button Take;
    public Button Back;

    public CanvasGroup MainGrp;
    public CanvasGroup TopGrp; 
    public CanvasGroup MiddleGrp; 
    public CanvasGroup BottomGrp; 
    
    public void Construct(ILocaleSystem localeSystem, Action onTake, uint income)
    {
      Take.onClick.AddListener(() => onTake());
      Back.onClick.AddListener(Close);
      
      new ButtonAnimation(Take.transform);
      new ButtonAnimation(Back.transform);

      Income.text = $"+{income}";
      
      Localize(localeSystem);
    }

    public void Open()
    {
      gameObject.SetActive(true);
      
      MainGrp.alpha = 0;
      MainGrp.interactable = true;
      MainGrp.DOFade(1, 0.1f);
        
      FadeGrp(TopGrp, 0.1f);
      FadeGrp(MiddleGrp, 0.2f);
      FadeGrp(BottomGrp, 0.3f);

      void FadeGrp(CanvasGroup canvasGroup, float delay)
      {
        canvasGroup.alpha = 0;
        canvasGroup.DOFade(1, 0.3f).SetDelay(delay);
      }
    }

    public void Close()
    {
      gameObject.SetActive(true);
      
      MainGrp.alpha = 1;
      MainGrp.interactable = false;
      MainGrp.DOFade(0, 0.2f).OnComplete(delegate { gameObject.SetActive(false); });
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Localize();
      });
    }
  }
}