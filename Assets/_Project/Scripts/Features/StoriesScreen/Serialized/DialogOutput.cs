using System;
using System.Collections.Generic;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Services.Unlocker;
using UnityEngine;

namespace _Project.Scripts.Features.StoriesScreen.Serialized
{
  [Serializable]
  public struct DialogOutput
  {
    public string TextKey;
    public RoleType RoleType;
    public List<string> AnswerOptions;
    public List<UnlockType> AnswerTypes;
    public GalleryData Photo;
    public uint MoneyUnlockAmount;
  }
}