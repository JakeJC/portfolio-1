using System;
using UnityEngine;

namespace _Project.Scripts.Features.StoriesScreen.Serialized
{
  [Serializable]
  public class StoryProgress
  {
    [SerializeField] private string _id;
    [SerializeField] private int _index;
    
    public int Index
    {
      get => _index;
      set => _index = value;
    }
    
    public string Id => _id;
    
    public StoryProgress(string id, int index)
    {
      _id = id;
      _index = index;
    }
  }
}