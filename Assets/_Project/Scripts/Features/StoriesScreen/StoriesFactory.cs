using _Project.Scripts.Features.ScrollView;
using _Project.Scripts.Features.StoriesScreen.Mono;
using _Project.Scripts.Services._Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.StoriesScreen
{
  public class StoriesFactory : IStoriesFactory
  {
    private const string StoryViewPath = "Features/StoryScreen/Story Window";
    private const string StoryDialogViewPath = "Features/StoryScreen/Story Dialog View";

    private const string ItemsViewPath = "Features/Other/ScrollViewPrefabs/Scroll View Items View";
    private const string ItemButtonPath = "Features/Other/ScrollViewPrefabs/Scroll View Item Button";
    private const string SectionButtonPath = "Features/Other/ScrollViewPrefabs/Scroll View Shop Section Button";

    private const string ComingSoonPath = "Features/Other/ScrollViewPrefabs/Coming Soon Story";

    private const string BaseCanvas = "Canvas";

    private readonly IAssetProvider _assetProvider;
    private readonly IRemote _remote;

    public StoriesFactory(IAssetProvider assetProvider, IRemote remote)
    {
      _remote = remote;
      _assetProvider = assetProvider;
    }

    public StoryView CreateStoriesView()
    {
      Transform parent = GameObject.Find(BaseCanvas).transform;

      var homeScreen = _assetProvider.GetResource<StoryView>(StoryViewPath + (_remote.ShouldEnableFeatureTest() ? "-AB" : string.Empty));
      return Object.Instantiate(homeScreen, parent);
    }

    public ScrollViewSectionButton CreateSectionButton(Transform parent)
    {
      var homeScreen = _assetProvider.GetResource<ScrollViewSectionButton>(SectionButtonPath);
      return Object.Instantiate(homeScreen, parent);
    }

    public ScrollViewItemsView CreateItemsView(Transform parent)
    {
      var homeScreen = _assetProvider.GetResource<ScrollViewItemsView>(ItemsViewPath);
      return Object.Instantiate(homeScreen, parent);
    }

    public ScrollViewItemButton CreateItemButton(Transform parent)
    {
      var homeScreen = _assetProvider.GetResource<ScrollViewItemButton>(ItemButtonPath + (_remote.ShouldEnableFeatureTest() ? "-AB" : string.Empty));
      return Object.Instantiate(homeScreen, parent);
    }

    public StoryDialogView CreateStoriesDialogView()
    {
      Transform parent = GameObject.Find(BaseCanvas).transform;

      var homeScreen = _assetProvider.GetResource<StoryDialogView>(StoryDialogViewPath);
      return Object.Instantiate(homeScreen, parent);
    }

    public GameObject CreateComingSoon(Transform parent)
    {
      var comingSoon = _assetProvider.GetResource<GameObject>(ComingSoonPath + (_remote.ShouldEnableFeatureTest() ? "-AB" : string.Empty));
      return Object.Instantiate(comingSoon, parent);
    }
  }
}