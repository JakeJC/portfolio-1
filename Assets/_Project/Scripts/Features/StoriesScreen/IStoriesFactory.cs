using _Project.Scripts.Features.ScrollView;
using _Project.Scripts.Features.StoriesScreen.Mono;
using UnityEngine;

namespace _Project.Scripts.Features.StoriesScreen
{
  public interface IStoriesFactory
  {
    StoryView CreateStoriesView();
    ScrollViewSectionButton CreateSectionButton(Transform parent);
    ScrollViewItemsView CreateItemsView(Transform parent);
    ScrollViewItemButton CreateItemButton(Transform parent);
    StoryDialogView CreateStoriesDialogView();
    GameObject CreateComingSoon(Transform itemsViewContentParent);
  }
}
