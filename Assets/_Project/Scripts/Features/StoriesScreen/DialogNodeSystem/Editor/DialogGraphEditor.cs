﻿using UnityEngine;

#if UNITY_EDITOR
using XNodeEditor;
#endif

namespace _Project.Scripts.Features.StoriesScreen.DialogNodeSystem.Editor
{
#if UNITY_EDITOR
  [CustomNodeGraphEditor(typeof(DialogGraph))]
  public class DialogGraphEditor : NodeGraphEditor
  {
    public override string GetNodeMenuName(System.Type type)
    {
      if (type.Namespace == "_Project.Scripts.Features.StoriesScreen.DialogNodeSystem.Nodes")
      {
        Debug.Log(type.Namespace);
        return base.GetNodeMenuName(type).Replace("Project/Scripts/Features/Stories Screen/Dialog Node System/Nodes/", "");
      }
      else return null;
    }
  }
#endif
}