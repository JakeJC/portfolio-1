using UnityEngine;
using XNode;

namespace _Project.Scripts.Features.StoriesScreen.DialogNodeSystem
{
    [CreateAssetMenu(fileName = "New Dialog Graph", menuName = "xNode/Dialog Graph")]
    public class DialogGraph : NodeGraph { }
}
