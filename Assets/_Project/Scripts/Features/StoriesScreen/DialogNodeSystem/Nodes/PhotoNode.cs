using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.StoriesScreen.Serialized;
using UnityEngine;
using XNode;

namespace _Project.Scripts.Features.StoriesScreen.DialogNodeSystem.Nodes
{
  [System.Serializable]
  [NodeWidth(250), NodeTint(70,100,70)]
  public class PhotoNode : ChoiceLogicNode
  {
    public GalleryData GalleryData;
    
    public override object GetValue(NodePort port)
    {
      output = new DialogOutput()
      {
        TextKey = _textKey,
        RoleType = roleType,
        Photo = GalleryData
      };

      return output;
    }
  }
}