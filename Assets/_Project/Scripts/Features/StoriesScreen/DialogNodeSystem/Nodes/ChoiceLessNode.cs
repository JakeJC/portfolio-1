using XNode;

namespace _Project.Scripts.Features.StoriesScreen.DialogNodeSystem.Nodes
{
  [System.Serializable]
  [NodeWidth(250), NodeTint(100, 70, 70)]
  public class ChoiceLessNode : ChoiceLogicNode
  {
  }
}