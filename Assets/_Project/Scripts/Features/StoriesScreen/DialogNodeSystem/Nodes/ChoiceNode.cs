using System.Collections.Generic;
using _Project.Scripts.Features.StoriesScreen.Serialized;
using _Project.Scripts.Services.Unlocker;
using XNode;

namespace _Project.Scripts.Features.StoriesScreen.DialogNodeSystem.Nodes
{
  [System.Serializable]
  [NodeWidth(250), NodeTint(70,100,70)]
  public class ChoiceNode : ChoiceLogicNode
  {
    public List<string> answerOptions;
    public List<UnlockType> answerTypes;
    public uint moneyUnlockAmount = 50;
    
    public override object GetValue(NodePort port)
    {
      output = new DialogOutput()
      {
        TextKey = _textKey,
        RoleType = roleType,
        AnswerOptions = answerOptions,
        AnswerTypes = answerTypes,
        MoneyUnlockAmount = moneyUnlockAmount
      };

      return output;
    }
  }
}