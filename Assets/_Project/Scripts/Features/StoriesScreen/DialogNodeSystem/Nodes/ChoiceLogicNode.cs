using _Project.Scripts.Features.StoriesScreen.Serialized;
using UnityEngine;
using XNode;

namespace _Project.Scripts.Features.StoriesScreen.DialogNodeSystem.Nodes
{
  public abstract class ChoiceLogicNode : Node
  {
    public string _textKey;
    public RoleType roleType = RoleType.Player;

    [Input, HideInInspector] public bool input;
    [Output] public DialogOutput output;

    public override object GetValue(NodePort port)
    {
      output = new DialogOutput()
      {
        TextKey = _textKey,
        RoleType = roleType
      };

      return output;
    }
  }
}