using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Core_Logic.StatesMachine.States;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.NotLoadedWindow;
using _Project.Scripts.Features.ShopScreen.Content.Extra;
using _Project.Scripts.Features.StoriesScreen.DialogNodeSystem.Nodes;
using _Project.Scripts.Features.StoriesScreen.Mono;
using _Project.Scripts.Features.StoriesScreen.Serialized;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using _Project.Scripts.Services.Money;
using AppacheAds.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using JetBrains.Annotations;
using ModestTree;
using UnityEngine;
using XNode;

namespace _Project.Scripts.Features.StoriesScreen
{
  public class StoryDialog
  {
    private const string MAIN = "Main";

    private readonly StoryDialogView _storyDialogView;

    private readonly IAdsService _adsService;
    private readonly IMoneyService _moneyService;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IGameStateMachine _gameStateMachine;
    private readonly IExtraContentViewLogic _extraContentViewLogic;
    private readonly IMoneyCounterLogic _moneyCounterLogic;
    private readonly IAssetProvider _assetProvider;
    private readonly IRemote _remote;
    
    private Node _node;
    private StoryData _storyData;

    private readonly Action<StoryData> _onEndStory;

    private Dictionary<RoleType, Sprite> _dialogVisualState;

    public StoryDialog(IAssetProvider assetProvider, MoneyCounterLogic moneyCounterLogic, IGameStateMachine gameStateMachine, IExtraContentViewLogic extraContentViewLogic, ILocaleSystem localeSystem,
      IAudioService audioService, IGameSaveSystem gameSaveSystem, IAdsService adsService, IMoneyService moneyService, IRemote remote,
      StoryDialogView storyDialogView, Action<StoryData> onEndStory)
    {
      _assetProvider = assetProvider;
      _extraContentViewLogic = extraContentViewLogic;
      _gameStateMachine = gameStateMachine;
      _onEndStory = onEndStory;
      _adsService = adsService;
      _localeSystem = localeSystem;
      _audioService = audioService;
      _moneyService = moneyService;
      _gameSaveSystem = gameSaveSystem;
      _remote = remote;

      _storyDialogView = storyDialogView;
      _moneyCounterLogic = moneyCounterLogic;
      _storyDialogView.Construct(_adsService, _moneyCounterLogic, localeSystem, _remote, OnClickNext, OnClickNextAd, 
        OnClickVariantAd, OnClickVariantMoney, OnSavePhoto, CloseDialog);

      CreateSounds(_storyDialogView);

      CreateDialogVisualStates();
    }

    private void CreateDialogVisualStates()
    {
      ExtraLookData extraLookData = _extraContentViewLogic.GetCurrentLook();

      _dialogVisualState = new Dictionary<RoleType, Sprite>()
      {
        {RoleType.Player, extraLookData.DefaultStatic},
        {RoleType.She, extraLookData.SpeakStatic},
        {RoleType.Description, null},
      };
    }

    public void LaunchDialog(StoryData storyData)
    {
      _storyData = storyData;

      if (_storyData.DialogGraph == null)
        return;

      _node = OnLoadStoryProgress();
      _storyDialogView.ShowNextStage(_storyData.LocalePath, GetOutputValue(), _dialogVisualState[((ChoiceLogicNode) _node).roleType],
        _localeSystem.GetText(MAIN, ((ChoiceLogicNode) _node).roleType == RoleType.She ? _storyData.Name : "YouName"));

      _storyDialogView.Show();

#if TEST_DIALOG
      _storyDialogView.ShowTest(OnClickNextText, OnClickPreviousTest);
#endif
    }

    public void CloseDialog()
    {
      OnSaveStoryProgress();
      _storyDialogView.Hide();

      _audioService.Play(AudioId.Karaoke_OST_Girls);
      _gameStateMachine.Enter<LoadStoryState>();
    }

    private void OnClickNext(int connectedIndex = 0)
    {      
      _moneyCounterLogic.ShowMoneyCounter(true);

      OnSaveStoryProgress();

      if (!_node.GetOutputPort("output").IsConnected)
      {
        Debug.Log("Dialog is over");
        _node = _storyData.DialogGraph.nodes[0];
        _onEndStory?.Invoke(_storyData);
        return;
      }

      List<NodePort> allConnections = _node.GetOutputPort("output").GetConnections();
      Node newNode = connectedIndex < allConnections.Count
        ? allConnections[connectedIndex].node
        : allConnections[0].node;

      _node = newNode;

      _storyDialogView.ShowNextStage(_storyData.LocalePath, GetOutputValue(), _dialogVisualState[((ChoiceLogicNode) _node).roleType],
        _localeSystem.GetText(MAIN, ((ChoiceLogicNode) _node).roleType == RoleType.She ? _storyData.Name : "YouName"));

      _audioService.Play(AudioId.Karaoke_Message);
    }

    private void OnClickNextAd(int connectedIndex)
    {
      if (_remote.ShouldEnableFeatureTest())
      {
        OnClickNext(connectedIndex);
        return;
      }
      
      _adsService.CurrentInterstitial.ShowInterstitial("dialog", 
        () => OnClickNext(connectedIndex),() => OnClickNext(connectedIndex));
    }

    private void OnClickVariantAd(int connectedIndex)
    {
      // if (!_adsService.Rewarded.RewardIsLoaded()) //ToDo Check is need or not
      //   return;
      
      _adsService.Rewarded.ShowReward("Next Message Reward", () => OnClickNext(connectedIndex), () => OnClickNext(connectedIndex));
    }

    // TODO: в случает НЕ закрепа AB прайс(50) вынести в контанту, а из DialogOutput удалить MoneyUnlockAmount
    private void OnClickVariantMoney(int connectedIndex) => 
      _moneyService.Spend(_remote.ShouldEnableFeatureTest() ? GetOutputValue().MoneyUnlockAmount : 50, () => OnClickNext(connectedIndex));

    private void OnSavePhoto(string id)
    {
      _gameSaveSystem.Get().UnlockedPhotoContent.Add(id);
      _gameSaveSystem.Save();
    }

    private void OnSaveStoryProgress()
    {
      _gameSaveSystem.Get().OnSaveStoryProgress(_storyData, _node);
      _gameSaveSystem.Save();
    }

    private Node OnLoadStoryProgress() =>
      _gameSaveSystem.Get().OnLoadStoryProgress(_storyData);

    private void CreateSounds(StoryDialogView dialogView)
    {
      new ButtonSound(_audioService, dialogView.NextButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, dialogView.CloseButton.gameObject, AudioId.Karaoke_Button);
    }

    private DialogOutput GetOutputValue() =>
      (DialogOutput) _node.GetOutputPort("output").GetOutputValue();

    private void OnClickNextText(int index = 0)
    {
      OnSaveStoryProgress();
      if (!_node.GetOutputPort("output").IsConnected)
      {
        Debug.Log("Dialog is over");
        _node = _storyData.DialogGraph.nodes[0];
        _onEndStory?.Invoke(_storyData);
        return;
      }

      List<NodePort> allConnections = _node.GetOutputPort("output").GetConnections();
      Node newNode = index < allConnections.Count
        ? allConnections[index].node
        : allConnections[0].node;
      _node = newNode;
      _storyDialogView.ShowNextStageTest(_storyData.LocalePath, GetOutputValue(), _dialogVisualState[((ChoiceLogicNode) _node).roleType],
        _localeSystem.GetText(MAIN, ((ChoiceLogicNode) _node).roleType == RoleType.She ? _storyData.Name : "YouName"));
      _audioService.Play(AudioId.Karaoke_Message);
    }

    private void OnClickPreviousTest(int index = 0)
    {
      OnSaveStoryProgress();
      if (!_node.GetInputPort("input").IsConnected)
      {
        Debug.Log("Dialog is over");
        _node = _storyData.DialogGraph.nodes[0];
        // _onEndStory?.Invoke(_storyData);
        return;
      }

      List<NodePort> allConnections = _node.GetInputPort("input").GetConnections();
      _node = allConnections[0].node;
      _storyDialogView.ShowPreviousStageTest(_storyData.LocalePath, GetOutputValue(), _dialogVisualState[((ChoiceLogicNode) _node).roleType],
        _localeSystem.GetText(MAIN, ((ChoiceLogicNode) _node).roleType == RoleType.She ? _storyData.Name : "YouName"));
      _audioService.Play(AudioId.Karaoke_Message);
    }
  }
}