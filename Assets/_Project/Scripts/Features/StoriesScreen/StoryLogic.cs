using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Core_Logic.Screens;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Core_Logic.StatesMachine.States;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.ScrollView;
using _Project.Scripts.Features.ScrollView.Serialized;
using _Project.Scripts.Features.SecretaryScreen.Data;
using _Project.Scripts.Features.SecretaryScreen.Serialized;
using _Project.Scripts.Features.ShopScreen.Content.Extra;
using _Project.Scripts.Features.StoriesScreen.DialogNodeSystem.Nodes;
using _Project.Scripts.Features.StoriesScreen.Mono;
using _Project.Scripts.Features.StoriesScreen.Serialized;
using _Project.Scripts.Services._data;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.Localization.Mono;
using _Project.Scripts.Services.Money;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.UI;
using XNode;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.StoriesScreen
{
  public class StoryLogic
  {
    private const uint WorkInProcessReward = 500;

    private const string GirlsDataPath = "Features/Data/GameGirlsData/Game Girls Data";

    private const string LocaleKey = "Main";

    private readonly IUiFactory _uiFactory;
    private readonly IStoriesFactory _storiesFactory;

    private readonly IRemote _remote;
    private readonly IAnalytics _analytics;
    private readonly IAdsService _adsService;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;
    private readonly IMoneyService _moneyService;
    private readonly ICurrentScreen _currentScreen;
    private readonly IUnlockService _unlockService;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IGameStateMachine _gameStateMachine;

    private readonly Dictionary<string, ScrollViewItemsView> _storyItemsView = new Dictionary<string, ScrollViewItemsView>();
    private readonly Dictionary<string, ScrollViewItemButton[]> _storyItemButtons = new Dictionary<string, ScrollViewItemButton[]>();

    private readonly GameGirlsData _gameGirlsData;

    private StoryView _storyView;

    private StoryDialog _storyDialog;
    private StoryDialogView _storyDialogView;

    private MoneyCounterLogic _moneyCounterLogic;
    private IAssetProvider _assetProvider;

    private List<ScrollViewSectionButton> _sections = new List<ScrollViewSectionButton>();

    private readonly IExtraContentViewLogic _extraContentViewLogic;

    public StoryLogic(IGameStateMachine gameStateMachine, IAssetProvider assetProvider, ICurrentScreen currentScreen,
      IGameSaveSystem gameSaveSystem, IAdsService adsService, IAnalytics analytics, ILocaleSystem localeSystem,
      IAudioService audioService, IRemote remote, IUnlockService unlockService, IMoneyService moneyService)
    {
      _remote = remote;
      _analytics = analytics;
      _adsService = adsService;
      _moneyService = moneyService;
      _localeSystem = localeSystem;
      _audioService = audioService;
      _currentScreen = currentScreen;
      _unlockService = unlockService;
      _gameSaveSystem = gameSaveSystem;
      _gameStateMachine = gameStateMachine;

      _assetProvider = assetProvider;
      _uiFactory = new UiFactory(_assetProvider, remote, localeSystem);
      _storiesFactory = new StoriesFactory(_assetProvider, _remote);

      _gameGirlsData = _assetProvider.GetResource<GameGirlsData>(GirlsDataPath);

      _currentScreen.SetCurrentScreen(ScreenNames.Stories);

      _extraContentViewLogic = new ExtraContentViewLogic();
      _extraContentViewLogic.Construct(_gameSaveSystem, assetProvider);
    }

    public void CreateStoriesView()
    {
      _storyView = _storiesFactory.CreateStoriesView();
      _storyView.Construct(_remote, _localeSystem, CloseStoryView, WorkInProcessTakeReward, WorkInProcessReward);

      _storyDialogView = _storiesFactory.CreateStoriesDialogView();

      _moneyCounterLogic = new MoneyCounterLogic(_assetProvider, _uiFactory, _localeSystem, _moneyService, _audioService, _adsService, _remote, _unlockService, _gameSaveSystem, _analytics);

      _storyDialog = new StoryDialog(_assetProvider, _moneyCounterLogic, _gameStateMachine, _extraContentViewLogic, _localeSystem, _audioService, _gameSaveSystem,
        _adsService, _moneyService, _remote, _storyDialogView, OnEndStory);

      UnlockStory(_gameGirlsData.GirlDatas[0].StoryDatas[0]);

      CreateStoriesContent();

      CreateSounds(_storyView);

      SetGirlVisual();
    }

    public void ClearStoriesView()
    {
      _moneyCounterLogic?.Clear();
      _sections.Clear();

      if (_storyDialogView)
        Object.Destroy(_storyDialogView.gameObject);
      if (_storyView)
        Object.Destroy(_storyView.gameObject);
    }

    private void SetGirlVisual()
    {
      ExtraLookData extraLookData = _extraContentViewLogic.GetCurrentLook();
      if (extraLookData)
        _storyView.SetGirlView(extraLookData.DefaultStatic);
    }

    private void Select(int index)
    {
      for (int i = 0; i < _sections.Count; i++)
        _sections[i].Select(i == index);
    }

    private void CreateStoriesContent()
    {
      foreach (GirlData t in _gameGirlsData.GirlDatas)
      {
        ScrollViewItemsView itemsView = _storiesFactory.CreateItemsView(_storyView.ItemViewsParent);
        _storyItemsView.Add(t.Name, itemsView);

        ScrollViewSectionButton shopButton = _storiesFactory.CreateSectionButton(_storyView.SectionsParent);
        _sections.Add(shopButton);

        shopButton.Construct(_localeSystem, t.Name, () =>
        {
          int index = _sections.IndexOf(shopButton);
          Select(index);
          OnClickGirl(t);
        }, t.Icon);

        var jewelryItemButton = new List<ScrollViewItemButton>();
        foreach (StoryData item in t.StoryDatas)
        {
          if (item.IsInDevelopment)
          {
            GameObject comingSoon = _storiesFactory.CreateComingSoon(itemsView.ContentParent);
            comingSoon.GetComponent<Button>().onClick.AddListener(delegate
            {
              if (_remote.ShouldEnableFeatureTest())
                OnClickWorkInProcessStory(t.StoryDatas);
            });

            foreach (var text in comingSoon.GetComponentsInChildren<LocalizedMainText>(true)) 
              text.Construct(_localeSystem);
          }
          else
          {
            ScrollViewItemButton scrollViewItemButton = _storiesFactory.CreateItemButton(itemsView.ContentParent); 
            scrollViewItemButton.Construct(_localeSystem, _unlockService,
              new ScrollViewItemData() { Id = item.Id, NameKey = item.Name, DescriptionKey = item.Description, Icon = _remote.ShouldEnableFeatureTest() ? item.Icon_AB : item.Icon },
              () => { OnClickStory(item); }, _audioService, _localeSystem.GetText(LocaleKey, item.Description), _remote);
            jewelryItemButton.Add(scrollViewItemButton);

            bool isItemUnlocked = _gameSaveSystem.Get().UnlockedStoryContent.Contains(item.Id);
            scrollViewItemButton.SetLockButton(!isItemUnlocked);

            var counterView = scrollViewItemButton.GetComponent<PicturesCounterView>();
            counterView.Construct(_localeSystem, false);
            if (counterView)
              counterView.SetOpenCount(GetUnlockedPicturesCount(item), GetMaxPicturesCount(item));
          }
        }

        _storyItemButtons.Add(t.Name, jewelryItemButton.ToArray());
      }

      Select(0);
      OnClickGirl(_gameGirlsData.GirlDatas[0]);

      int GetUnlockedPicturesCount(StoryData data)
      {
        int unlockedCount = 0;

        foreach (Node dialogGraphNode in data.DialogGraph.nodes)
        {
          if (dialogGraphNode is PhotoNode photoNode)
          {
            if (_gameSaveSystem.Get().UnlockedPhotoContent.Contains(photoNode.GalleryData.Id) ||
                _gameSaveSystem.Get().UnlockedStoriesContent.Contains(photoNode.GalleryData.Id))
              unlockedCount++;
          }
        }

        return unlockedCount;
      }

      int GetMaxPicturesCount(StoryData data) => data.DialogGraph.nodes.OfType<PhotoNode>().Count();
    }

    private void OnClickGirl(BaseContentData item)
    {
      foreach (KeyValuePair<string, ScrollViewItemsView> itemsView in GetDeactivatedItemsViews(item.Id))
        itemsView.Value.Deactivate();

      _storyItemsView[item.Name].Activate();
    }

    private void WorkInProcessTakeReward()
    {
      _moneyService.Earn(WorkInProcessReward);
      _storyView.WorkInProcessView.Close();

      _analytics.Send("btn_3rdstory");
      
      GameSaveData gameSaveData = _gameSaveSystem.Get();
      gameSaveData.RewardedLockedStory = true;
      _gameSaveSystem.Save();
    }

    private void OnClickWorkInProcessStory(StoryData[] storyDatas)
    {
      foreach (var story in storyDatas)
      {
        if(story.IsInDevelopment)
          continue;
        
        if(_gameSaveSystem.Get().UnlockedStoryContent.Contains(story.Id) == false)
          return;
      }
      
      if (_gameSaveSystem.Get().RewardedLockedStory == false)
        _storyView.WorkInProcessView.Open();
    }

    private void OnClickStory(StoryData item)
    {
      if (!_gameSaveSystem.Get().UnlockedStoryContent.Contains(item.Id))
        return;

      _analytics.Send("ev_story_start", new Dictionary<string, object>
      {
        { "story_id", item.Id },
        { "character_id", GetGirlDataByStory(item).Id },
      });

      _storyDialog.LaunchDialog(item);

      _audioService.Play(AudioId.Karaoke_OST_Novel);

      GirlData GetGirlDataByStory(StoryData storyData) =>
        _gameGirlsData.GirlDatas.FirstOrDefault(p => p.StoryDatas.Contains(storyData));
    }

    private void OnEndStory(StoryData item)
    {
      _analytics.Send("ev_story_finish", new Dictionary<string, object>
      {
        { "story_id", item.Id },
        { "character_id", GetGirlDataByStory(item).Id },
      });

      foreach (GirlData girl in _gameGirlsData.GirlDatas)
      {
        StoryData currentStory = girl.StoryDatas.FirstOrDefault(story => story.Id == item.Id);

        int nextIndex = girl.StoryDatas.ToList().IndexOf(currentStory) + 1;

        StoryData nextStory;
        bool shouldContinue;

        do
        {
          shouldContinue = GetNextStoryForOpen(girl, nextIndex, out StoryData nextStoryData);
          nextStory = nextStoryData;

          if (nextStory != null)
            UnlockStory(nextStory);

          nextIndex++;
        } while (shouldContinue);

        //if we need to start next dialog at once
        /*_storyDialog.LaunchDialog(nextStory);
        return;*/
        break;
      }

      Debug.Log("Story is ended");
      _storyDialog.CloseDialog();

      bool GetNextStoryForOpen(GirlData girl, int nextIndex, out StoryData nextStoryData)
      {
        if (nextIndex >= girl.StoryDatas.Length)
        {
          nextStoryData = null;
          return false;
        }

        nextStoryData = girl.StoryDatas[nextIndex];

        if (nextStoryData.IsInDevelopment)
          return true;

        return false;
      }

      GirlData GetGirlDataByStory(StoryData storyData) =>
        _gameGirlsData.GirlDatas.FirstOrDefault(p => p.StoryDatas.Contains(storyData));
    }

    private void CloseStoryView()
    {
      if (_remote.ShouldEnableFeatureTest())
      {
        _gameStateMachine.Enter<LoadHomeState>();
        return;
      }

      _adsService.CurrentInterstitial.ShowInterstitial(ScreenNames.Home,
        _gameStateMachine.Enter<LoadHomeState>,
        _gameStateMachine.Enter<LoadHomeState>);
    }

    private void CreateSounds(StoryView storyView) =>
      new ButtonSound(_audioService, storyView.BackButton.gameObject, AudioId.Karaoke_Button);

    private void UnlockStory(StoryData item)
    {
      if (_gameSaveSystem.Get().UnlockedStoryContent.Contains(item.Id)) return;

      _gameSaveSystem.Get().UnlockedStoryContent.Add(item.Id);
      _gameSaveSystem.Save();
    }

    private IEnumerable<KeyValuePair<string, ScrollViewItemsView>> GetDeactivatedItemsViews(string id) =>
      _storyItemsView.Where(item => item.Key != id);
  }
}