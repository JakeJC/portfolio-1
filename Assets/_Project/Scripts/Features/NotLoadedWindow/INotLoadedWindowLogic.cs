namespace _Project.Scripts.Features.NotLoadedWindow
{
  public interface INotLoadedWindowLogic
  {
    void Open();
  }
}