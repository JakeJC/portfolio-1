using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Animation;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Core_Logic.StatesMachine.States;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.Localization.Mono;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Policy
{
  public class PrivacyPolicy : MonoBehaviour
  {
    [Header("Main")] [SerializeField] private GameObject personalizedPanel;
    [SerializeField] private Toggle personalizedToggle;

    [SerializeField] private TextMeshProUGUI policyText;

    [Header("AcceptScreen")] [SerializeField]
    private GameObject acceptScreen;

    [SerializeField] private Button linkButton;
    [SerializeField] private Button acceptButton1;

    [Header("ScrollScreen")] [SerializeField]
    private ScrollRect scrollRect;

    [SerializeField] private GameObject scrollScreen;
    [SerializeField] private Button backButton;

    [SerializeField] private Button denyButton;
    [SerializeField] private CanvasGroup denyButtonCanvasGroup;

    [Header("Animation")] [SerializeField] private CanvasGroup blackscreen;
    [SerializeField] private CanvasGroup glowIcon;

    [SerializeField] private CanvasGroup topContainer;
    [SerializeField] private CanvasGroup middleContainer;
    [SerializeField] private CanvasGroup openContainer;
    [SerializeField] private CanvasGroup bottomContainer;

    [SerializeField] private Transform iconTransform;
    [SerializeField] private Transform buttonTransform;
    
    [SerializeField] private CanvasGroup toggleGroup;

    public event Action ClosingStarted;
    public event Action ClosingFinished;

    private IAudioService _audioService;
    private IGameSaveSystem _gameSaveSystem;
    private IAnalytics _analytics;
    private IRemote _remote;
    private IGameStateMachine _gameStateMachine;
    private IAdsService _adsService;

    public void Construct(IGameStateMachine gameStateMachine, IGameSaveSystem gameSaveSystem, IAudioService audioService,
      ILocaleSystem localeSystem, IAnalytics analytics, IRemote remote, IAdsService adsService)
    {
      _adsService = adsService;
      _gameStateMachine = gameStateMachine;
      _remote = remote;
      _analytics = analytics;
      _audioService = audioService;
      _gameSaveSystem = gameSaveSystem;

      gameObject.SetActive(false);

      denyButton.gameObject.SetActive(_gameSaveSystem.Get().IsPolicyAccepted);
      personalizedPanel.SetActive(_gameSaveSystem.Get().IsPolicyAccepted);

      denyButtonCanvasGroup.interactable = false;
      denyButtonCanvasGroup.alpha = 0.5f;

      Localize(localeSystem);

      AssignAnimations();

      scrollRect.onValueChanged.AddListener(OnDrag);

      FetchPolicyText();
      AssignPersonalizedToggle();
    }

    public void OpenWindow()
    {
      gameObject.SetActive(true);

      if (_gameSaveSystem.Get().IsPolicyAccepted)
        ShowScrollScreen();
      else
        ShowAcceptScreen();

      acceptButton1.gameObject.SetActive(!_gameSaveSystem.Get().IsPolicyAccepted);
      backButton.gameObject.SetActive(_gameSaveSystem.Get().IsPolicyAccepted);

      AssignListeners();

      AppearAnimation(!_gameSaveSystem.Get().IsPolicyAccepted);
    }

    private void AssignPersonalizedToggle()
    {
      personalizedToggle.isOn = !_adsService.IsNonPersonalized();
      AnimateToggle(personalizedToggle.isOn);

      personalizedToggle.onValueChanged.AddListener(isOn =>
      {
        _adsService.SetNonPersonalized(!isOn);
        AnimateToggle(isOn);
      });
    }

    private void AnimateToggle(bool isOn) => 
      toggleGroup.DOFade(isOn ? 1 : 0, 0.15f);

    private void FetchPolicyText()
    {
      string text = _remote.GetPrivacyText();
      if (text != "base" && !String.IsNullOrEmpty(text))
        policyText.text = text;
    }

    private void OnDrag(Vector2 arg0)
    {
      float normalizedPosition = scrollRect.verticalNormalizedPosition;

      if (normalizedPosition <= 0.01f)
      {
        denyButtonCanvasGroup.interactable = true;
        denyButtonCanvasGroup.alpha = 1f;

        new ButtonAnimation(denyButton.transform);
        new ButtonSound(_audioService, denyButton.gameObject, AudioId.Karaoke_Button);
      }
    }

    private void AssignAnimations()
    {
      new ButtonAnimation(acceptButton1.transform);
      new ButtonAnimation(backButton.transform);

      new ButtonSound(_audioService, acceptButton1.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, backButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, linkButton.gameObject, AudioId.Karaoke_Button);
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Localize();
      });
    }

    private void Accept()
    {
      _analytics.Send("btn_policy");

      _gameSaveSystem.Get().IsPolicyAccepted = true;
      _gameSaveSystem.Save();

      CloseWindow();
    }

    private void Deny()
    {
      _gameSaveSystem.Get().IsPolicyAccepted = false;
      _gameSaveSystem.Save();

      _audioService.Clean();

      _gameStateMachine.Enter<LoadPolicyState>();
    }

    private void Back() =>
      CloseWindow();

    private void CloseWindow()
    {
      ClosingStarted?.Invoke();
      RemoveListeners();
      gameObject.SetActive(false);
      ClosingFinished?.Invoke();
    }

    private void ShowAcceptScreen()
    {
      acceptScreen.gameObject.SetActive(true);
      scrollScreen.gameObject.SetActive(false);
    }

    private void ShowScrollScreen()
    {
      acceptScreen.gameObject.SetActive(false);
      scrollScreen.gameObject.SetActive(true);
    }

    private void AssignListeners()
    {
      if (!_gameSaveSystem.Get().IsPolicyAccepted)
      {
        linkButton.onClick.AddListener(ShowScrollScreen);
        acceptButton1.onClick.AddListener(Accept);
      }
      else
      {
        denyButton.onClick.AddListener(Deny);
        backButton.onClick.AddListener(Back);
      }
    }

    private void RemoveListeners()
    {
      linkButton.onClick.RemoveListener(ShowScrollScreen);
      acceptButton1.onClick.RemoveListener(Accept);

      denyButton.onClick.RemoveListener(Deny);
      backButton.onClick.RemoveListener(Back);
    }

    private void AppearAnimation(bool isFirstTime)
    {
      PrepareStartState();

      Sequence seq = DOTween.Sequence();

      ContainersAppear(seq);

      if(isFirstTime)
        seq.AppendCallback(GlowIconAnimation);

      void ContainersAppear(Sequence sequence)
      {
        var duration = 0.3f;
        var interval = 0.1f;

        if (isFirstTime)
          sequence.Append(blackscreen.DOFade(0, 1f));
        else
        {
          blackscreen.alpha = 0;
          sequence.AppendInterval(interval);
        }

        sequence.Append(topContainer.DOFade(1, duration));
        sequence.AppendInterval(interval);

        sequence.Append(isFirstTime ? middleContainer.DOFade(1, duration) : openContainer.DOFade(1, duration));
        sequence.AppendInterval(interval);
        
        if (isFirstTime)
        {
          sequence.Append(iconTransform.DOScale(1, duration));
          sequence.AppendInterval(interval);
        }
        
        sequence.Append(bottomContainer.DOFade(1, duration));
        sequence.Join(buttonTransform.DOScale(1, duration));

        sequence.AppendInterval(interval);
      }

      void PrepareStartState()
      {
        glowIcon.alpha = 0;

        topContainer.alpha = 0;

        if (isFirstTime)
          middleContainer.alpha = 0;
        else
          openContainer.alpha = 0;

        bottomContainer.alpha = 0;

        blackscreen.alpha = 1;
        
        iconTransform.localScale = 0.5f * Vector3.one;
        buttonTransform.localScale = 0.5f * Vector3.one;
      }
    }

    private void GlowIconAnimation()
    {
      glowIcon.DOFade(1, 0.1f).SetLoops(5, LoopType.Yoyo).SetEase(Ease.Linear).OnComplete((() =>
      {
        var sequence = DOTween.Sequence();
        sequence.SetDelay(1.5f);
        sequence.Append(glowIcon.DOFade(0.1f, 0.05f).SetLoops(8, LoopType.Yoyo).SetEase(Ease.Linear));
        sequence.Append(glowIcon.DOFade(0, 0.07f).SetLoops(3, LoopType.Yoyo).SetEase(Ease.Linear).SetDelay(0.15f));
        sequence.Append(glowIcon.DOFade(1, 0.4f).SetEase(Ease.OutBounce));
        sequence.Append(glowIcon.DOFade(1, 0).SetDelay(2f));
        sequence.SetLoops(-1);
      }));
      
      
      
      
    }
      

    private void OnGUI()
    {
      if (Application.isEditor)
      {
        if (GUI.Button(new Rect(10, 70, 50, 30), "Click"))
          AppearAnimation(true);
      }
    }
  }
}