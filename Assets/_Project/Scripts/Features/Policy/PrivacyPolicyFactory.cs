using _Project.Scripts.Services._Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.Policy
{
  public class PrivacyPolicyFactory : IPrivacyPolicyFactory
  {
    private const string CanvasBlurred = "Canvas";

    private const string UIPrivacyPolicyNewPath = "Features/Privacy/Privacy Policy Window New";
    
    private PrivacyPolicy _privacyPolicy;

    private readonly IAssetProvider _assetProvider;
    private readonly IRemote _remote;
    
    public PrivacyPolicyFactory(IAssetProvider assetProvider, IRemote remote)
    {
      _assetProvider = assetProvider;
      _remote = remote;
    }

    public PrivacyPolicy SpawnPrivacyPolicy()
    {
      if (_privacyPolicy != null)
        return _privacyPolicy;

      Clear();

      Transform parent = GameObject.Find(CanvasBlurred).transform;

      string currentUIPath = UIPrivacyPolicyNewPath;
      
      var privacyPolicyObject = _assetProvider.GetResource<GameObject>(currentUIPath);
      _privacyPolicy = Object.Instantiate(privacyPolicyObject, parent).GetComponent<PrivacyPolicy>();
      _privacyPolicy.gameObject.SetActive(false);
      return _privacyPolicy;
    }

    public void Clear()
    {
      if (!_privacyPolicy)
        return;

      Object.Destroy(_privacyPolicy.gameObject);
    }
  }
}