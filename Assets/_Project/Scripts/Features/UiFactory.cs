using System;
using System.Collections.Generic;
using _Project.Scripts.Features.DisableAds.Mono;
using _Project.Scripts.Features.IapWindow.Data;
using _Project.Scripts.Features.IapWindow.Mono;
using _Project.Scripts.Features.ScrollView;
using _Project.Scripts.Features.SettingsWindow;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.Money.Mono;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features
{
  public class UiFactory : IUiFactory
  {
    private const string MoneyCounterPath = "Features/Other/MoneyCounter/MoneyCounter";
    private const string AddMoneyWindowPath = "Features/Other/MoneyCounter/AddMoneyWindow";
    private const string IapPath = "Features/Windows/IAP Window";
    private const string SettingsPath = "Features/Windows/SettingsWindow";
    private const string DisableAdsPath = "Features/Windows/DisableAds";
    private const string LoadWindowPath = "Features/Windows/LoadWindow";
    private const string NoInternetWindowPath = "Features/Windows/NoInternet";
    private const string DailyRewardButtonPath = "Features/Other/IAP Daily Reward Button";
    private const string IapDailyRewardDataPath = "Features/Data/IapDailyRewardData";
    
    private const string IapOfferLisaMiniBikiniPath = "Features/Windows/IAP Offer Secretary Lisa Mini Bikini";
    private const string IapOfferJoy= "Features/Windows/IAP Offer Joy";

    private const string IapItemSmallButtonPath = "Features/Other/ScrollViewPrefabs/Iap/Scroll View Iap Item Small Button";
    private const string IapItemLargeButtonPath = "Features/Other/ScrollViewPrefabs/Iap/Scroll View Iap Item Large Button";

    private const string BaseCanvas = "Canvas";

    private readonly IAssetProvider _assetProvider;
    private readonly IRemote _remote;
    private readonly ILocaleSystem _localeSystem;

    private MoneyCounter _moneyCounter;

    // public LoadWindow Loading { get; private set; }
    public IapView IapWindow { get; private set; }
    public DisableAdsWindow DisableAdsWindow { get; private set; }

    public Settings SettingsWindow { get; private set; }

    public UiFactory(IAssetProvider assetProvider, IRemote remote, ILocaleSystem localeSystem)
    {
      _assetProvider = assetProvider;
      _remote = remote;
      _localeSystem = localeSystem;
    }

    public MoneyCounter SpawnMoneyCounter()
    {
      Transform parent = GameObject.Find(BaseCanvas).transform;

      var window = _assetProvider.GetResource<MoneyCounter>(MoneyCounterPath + (_remote.ShouldEnableFeatureTest() ? "-AB" : string.Empty));
      _moneyCounter = Object.Instantiate(window, parent);
      return _moneyCounter;
    }

    public AddMoneyView SpawnAddMoneyWindow()
    {
      Transform parent = GameObject.Find(BaseCanvas).transform;

      var window = _assetProvider.GetResource<AddMoneyView>(AddMoneyWindowPath + (_remote.ShouldEnableFeatureTest() ? "-AB" : string.Empty));
      return Object.Instantiate(window, parent);
    }

    public void SpawnDisableAdsWindow()
    {
      Transform parent = GameObject.Find(BaseCanvas).transform;

      var window = _assetProvider.GetResource<DisableAdsWindow>(DisableAdsPath);
      DisableAdsWindow = Object.Instantiate(window, parent);
    }

    public void SpawnIapWindow(Action onOpenWindow, Action<IapDailyRewardData> onDailyRewardTake, ILocaleSystem localeSystem)
    {
      Transform parent = GameObject.Find(BaseCanvas).transform;

      var window = _assetProvider.GetResource<IapView>(IapPath + (_remote.ShouldEnableFeatureTest() ? "-AB" : string.Empty));
      IapWindow = Object.Instantiate(window, parent);
      IapWindow.Construct(_localeSystem, onOpenWindow);

      if (_remote.ShouldEnableFeatureTest())
      {
        var dailyRewardButtonPrefab = _assetProvider.GetResource<IapDailyRewardButtonView>(DailyRewardButtonPath);

        List<IapDailyRewardButtonView> dailyRewardButtons = new List<IapDailyRewardButtonView>();
        int day = 1;
        foreach (var data in _assetProvider.GetAllResources<IapDailyRewardData>(IapDailyRewardDataPath))
        {
          var button = Object.Instantiate(dailyRewardButtonPrefab, IapWindow.DailyRewardView.Content);
          button.Construct(localeSystem, data, onDailyRewardTake, day);
          dailyRewardButtons.Add(button);
          day++;
        }

        IapWindow.DailyRewardView.DailyRewardButtons = dailyRewardButtons.ToArray();
      }
    }

    public void SpawnSettingsWindow()
    {
      Transform parent = GameObject.Find(BaseCanvas).transform;

      var window = _assetProvider.GetResource<Settings>(SettingsPath);
      SettingsWindow = Object.Instantiate(window, parent);
    }

    public void SpawnNoInternetConnection()
    {
      /*Transform parent = GameObject.Find(BaseCanvas).transform;

      var window = _assetProvider.GetResource<NoInternet>(NoInternetWindowPath);
      NoInternetWindow = Object.Instantiate(window, parent);*/
    }

    public void ClearMoneyCounter()
    {
      if (_moneyCounter.gameObject)
        Object.Destroy(_moneyCounter.gameObject);
    }

    public void ClearSettingsWindow()
    {
      SettingsWindow.Clear();

      if (SettingsWindow)
        Object.Destroy(SettingsWindow.gameObject);
    }

    public void ClearDisableAdsWindow()
    {
      if (DisableAdsWindow)
        Object.Destroy(DisableAdsWindow.gameObject);
    }

    public ScrollViewIapItemButton CreateIapItemSmallButton(Transform contentParent)
    {
      var iapItem = _assetProvider.GetResource<ScrollViewIapItemButton>(IapItemSmallButtonPath + (_remote.ShouldEnableFeatureTest() ? "-AB" : string.Empty));
      return Object.Instantiate(iapItem, contentParent);
    }

    public ScrollViewIapItemButton CreateIapItemLargeButton(Transform contentParent)
    {
      var iapItem = _assetProvider.GetResource<ScrollViewIapItemButton>(IapItemLargeButtonPath + (_remote.ShouldEnableFeatureTest() ? "-AB" : string.Empty));
      return Object.Instantiate(iapItem, contentParent);
    }

    public IapOfferView CreateOfferLisaMiniBikini(Transform contentParent)
    {
      var offer = _assetProvider.GetResource<IapOfferView>(IapOfferLisaMiniBikiniPath);
      offer.gameObject.SetActive(false);
      return Object.Instantiate(offer, contentParent);
    }

    public IapOfferView CreateOfferJoy(Transform contentParent)
    {
      var offer = _assetProvider.GetResource<IapOfferView>(IapOfferJoy);
      return Object.Instantiate(offer, contentParent);
    }
  }
}