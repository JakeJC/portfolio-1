using System.Collections.Generic;
using _Project.Scripts.Core_Logic.Screens;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Core_Logic.StatesMachine.States;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.DisableAds;
using _Project.Scripts.Features.IapWindow;
using _Project.Scripts.Features.IapWindow.Mono;
using _Project.Scripts.Features.Policy;
using _Project.Scripts.Features.ShopScreen;
using _Project.Scripts.Features.ShopScreen.Content;
using _Project.Scripts.Features.ShopScreen.Mono;
using _Project.Scripts.Features.TutorialPopUp;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.Money;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.HomeScreen
{
  public class HomeScreenLogic
  {
    private const string GirlsDataPath = "Features/Data/GameGirlsData/Game Girls Data";

    private readonly IHomeFactory _homeFactory;
    private readonly IUiFactory _uiFactory;

    private readonly IRemote _remote;
    private readonly IAnalytics _analytics;
    private readonly IAdsService _adsService;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;
    private readonly IMoneyService _moneyService;
    private readonly IAssetProvider _assetProvider;
    private readonly ICurrentScreen _currentScreen;
    private readonly IUnlockService _unlockService;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IGameMetaProvider _gameMetaProvider;
    private readonly IGameStateMachine _gameStateMachine;
#if !DISABLE_INAPPS
    private IapLogic _iapLogic;
#endif
    private MoneyCounterLogic _moneyCounterLogic;

    private Mono.HomeScreen _homeScreenView;
    private ShopLogic _shopLogic;

    private float _moneyBarFillValue;

    private DisableAdsLogic _disableAdsLogic;

    private ContentViewLogic _contentViewLogic;

    private TutorialPopUpLogic _tutorialPopUpLogic;

    public HomeScreenLogic(IGameStateMachine gameStateMachine, IAssetProvider assetProvider,
      ICurrentScreen currentScreen,
      IGameSaveSystem gameSaveSystem, IAdsService adsService, IAnalytics analytics, ILocaleSystem localeSystem,
      IAudioService audioService, IRemote remote, IMoneyService moneyService, IGameMetaProvider gameMetaProvider,
      IUnlockService unlockService)
    {
      _remote = remote;
      _analytics = analytics;
      _adsService = adsService;
      _localeSystem = localeSystem;
      _audioService = audioService;
      _moneyService = moneyService;
      _unlockService = unlockService;
      _assetProvider = assetProvider;
      _currentScreen = currentScreen;
      _gameSaveSystem = gameSaveSystem;
      _gameMetaProvider = gameMetaProvider;
      _gameStateMachine = gameStateMachine;

      _homeFactory = new HomeFactory(assetProvider, remote);
      _uiFactory = new UiFactory(assetProvider, remote, localeSystem);

      if (remote.ShouldEnableFeatureTest())
      {
        if (_gameSaveSystem.Get().MoneyCountFirstLaunch)
        {
          moneyService.Earn(400);
          _gameSaveSystem.Get().MoneyCountFirstLaunch = false;
          _gameSaveSystem.Save();
        }
        
        IapDailyRewardLogic.CreateUpdater();
      }
    }

    private void IAPLazyInitialization()
    {
      if (_uiFactory.IapWindow == null)
      {
#if !DISABLE_INAPPS
        _iapLogic = new IapLogic(_assetProvider, _adsService, _uiFactory, _localeSystem, _unlockService, _moneyService,
          _gameSaveSystem, _audioService, _remote, _analytics);
#endif
      }
    }

    private void CreateShopLogic()
    {
      _shopLogic = new ShopLogic(_gameStateMachine, _assetProvider, _currentScreen, _gameSaveSystem, _adsService,
        _analytics, _localeSystem, _audioService, _remote, _unlockService, _moneyService, _gameMetaProvider);
      _shopLogic.CreateShopView(delegate
      {
        _homeScreenView.ShowUI();
        _shopLogic.ClearShopView();

        if (_remote.ShouldEnableFeatureTest())
          return;

        _adsService.CurrentInterstitial.ShowInterstitial(ScreenNames.Home, () => { }, () => { });
      });
    }

    public void CreateHomeScreen()
    {
      _currentScreen.SetCurrentScreen(ScreenNames.Home);
      _adsService.Banner.HideBanner();

      _homeScreenView = _homeFactory.CreateHomeScreen();
      _homeScreenView.Construct(_localeSystem, _remote, ToGallery, ToSecretary, OpenSettingsWindow, ToShop, ToGames, ToStories,
        ToIapShop, ToNoAds, ClickBar, _audioService);

      IPrivacyPolicyFactory privacyPolicyFactory = new PrivacyPolicyFactory(_assetProvider, _remote);

      _uiFactory.SpawnSettingsWindow();
      _uiFactory.SettingsWindow.Construct(_gameStateMachine, _gameSaveSystem, _audioService, _localeSystem,
        _analytics, privacyPolicyFactory, _currentScreen, _adsService, _remote);

      CheckNetwork();

      _moneyCounterLogic = new MoneyCounterLogic(_assetProvider, _uiFactory, _localeSystem, _moneyService,
        _audioService, _adsService, _remote, _unlockService, _gameSaveSystem, _analytics);

      _disableAdsLogic = new DisableAdsLogic(_assetProvider, _gameSaveSystem, _analytics, _adsService, _localeSystem, _uiFactory,
        delegate(bool status) { _homeScreenView.NoAdsButton.gameObject.SetActive(!status); }, _remote);

      CreateSounds(_homeScreenView);

      //ToDo Заменить на выбор сохранённой герлы
      var gameGirlsData = _assetProvider.GetResource<GameGirlsData>(GirlsDataPath);
      _gameMetaProvider.SetCurrentGirl(gameGirlsData.GirlDatas[0].Id);

      if (!_gameSaveSystem.Get().UnlockedStoriesContent.Contains(gameGirlsData.GirlDatas[0].GalleryDatas[0].Id))
      {
        _gameSaveSystem.Get().UnlockedStoriesContent.Add(gameGirlsData.GirlDatas[0].GalleryDatas[0].Id);
        _gameSaveSystem.Save();
      }

      _contentViewLogic = new ContentViewLogic();
      _contentViewLogic.Construct(_gameSaveSystem, _assetProvider, _homeScreenView.GirlSkeletonAnimation);

      _contentViewLogic.Display();

      if (PlayerPrefs.GetInt(TutorialPopUpLogic.KeyTutorIsComplete, 0) != 0)
        return;

      List<TutorialData> tutorialDatas = new List<TutorialData>()
      {
        new TutorialData("Tutor.Welcome.Header", "Tutor.Welcome", _homeScreenView.StoriesButton.gameObject),
        new TutorialData("Games", "Tutor.Welcome2", _homeScreenView.GamesButton.gameObject),
        new TutorialData("Gallery", "Tutor.Welcome3", _homeScreenView.GalleryButton.gameObject),
        new TutorialData("Secretary", "Tutor.Welcome4", _homeScreenView.SecretaryButton.gameObject)
      };

      _tutorialPopUpLogic = new TutorialPopUpLogic(_audioService, _localeSystem, _assetProvider, _analytics, tutorialDatas);

      IAPLazyInitialization();
    }

    private void CreateSounds(Mono.HomeScreen homeScreen)
    {
      new ButtonSound(_audioService, homeScreen.GalleryButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, homeScreen.SecretaryButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, homeScreen.IapShopButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, homeScreen.SettingsButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, homeScreen.GamesButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, homeScreen.ShopButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, homeScreen.NoAdsButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, homeScreen.StoriesButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(_audioService, homeScreen.GirlButton.gameObject, AudioId.Karaoke_Button);

      _audioService.Play(AudioId.Karaoke_OST_Girls);
    }

    private void CheckNetwork()
    {
      if (Application.internetReachability != NetworkReachability.NotReachable)
        return;
    }

    public void CloseHomeScreen()
    {
      if(_remote.ShouldEnableFeatureTest() == false)
       _adsService.Banner.ShowBanner();

#if !DISABLE_INAPPS
      _iapLogic?.Clear();
#endif
      _disableAdsLogic?.Cleanup();
      _moneyCounterLogic?.Clear();

      _homeFactory?.ClearHomeScreen();
      _uiFactory?.ClearSettingsWindow();
    }

    private void ToGallery()
    {
      _analytics.Send("btn_gallery");

      if (_remote.ShouldEnableFeatureTest())
      {
        GoToGallery();
        return;
      }

      _adsService.CurrentInterstitial.ShowInterstitial(ScreenNames.Gallery, GoToGallery, GoToGallery);

      void GoToGallery()
      {
        _gameStateMachine.Enter<LoadGalleryState>();
      }
    }

    private void ToSecretary()
    {
      _analytics.Send("btn_assist");

      if (_remote.ShouldEnableFeatureTest())
      {
        GoToSecretary();
        return;
      }

      _adsService.CurrentInterstitial.ShowInterstitial(ScreenNames.Secretary, GoToSecretary, GoToSecretary);

      void GoToSecretary()
      {
        _gameStateMachine.Enter<LoadSecretaryState>();
      }
    }

    private void ToShop()
    {
      _analytics.Send("btn_shop");

      if (_remote.ShouldEnableFeatureTest())
      {
        GoToShop();
        _analytics.Send("btn_style");
        return;
      }

      _adsService.CurrentInterstitial.ShowInterstitial(ScreenNames.Shop, GoToShop, GoToShop);

      void GoToShop()
      {
        _homeScreenView.HideUI();
        CreateShopLogic();
      }
    }

    private void ToGames()
    {
      _analytics.Send("btn_games");

      if (_remote.ShouldEnableFeatureTest())
      {
        GoToGames();
        return;
      }

      _adsService.CurrentInterstitial.ShowInterstitial(ScreenNames.MiniGames, GoToGames, GoToGames);

      void GoToGames()
      {
        _gameStateMachine.Enter<LoadMiniGamesState>();
      }
    }

    private void ToStories()
    {
      _analytics.Send("btn_story");

      if (_remote.ShouldEnableFeatureTest())
      {
        GoToStories();
        return;
      }

      _adsService.CurrentInterstitial.ShowInterstitial(ScreenNames.Stories, GoToStories, GoToStories);

      void GoToStories()
      {
        _gameStateMachine.Enter<LoadStoryState>();
      }
    }

    private void ToIapShop()
    {
      IAPLazyInitialization();

      _uiFactory.IapWindow.OpenWindow();
    }

    private void ToNoAds() =>
      _disableAdsLogic.Open();

    private void OpenSettingsWindow()
    {
      _uiFactory.SettingsWindow.OpenWindow(_homeScreenView.ShowSettingsButton);
      _homeScreenView.HideSettingsButton();
      _analytics.Send("btn_settings");
    }

    private void ClickBar()
    {
      if (_moneyBarFillValue >= 1f)
      {
        _moneyBarFillValue = 0;
        _moneyService.Earn(1);
      }
      else
        _moneyBarFillValue += .1f;

      _homeScreenView.SetMoneyFill(_moneyBarFillValue);
    }
  }
}