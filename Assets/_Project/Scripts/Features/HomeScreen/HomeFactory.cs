using System;
using System.Collections.Generic;
using _Project.Scripts.Services._Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.HomeScreen
{
  public class HomeFactory : IHomeFactory
  {
    private const string HomePath = "Features/HomeScreen/Home";
    
    private const string BaseCanvas = "Canvas";

    private readonly IAssetProvider _assetProvider;
    private readonly IRemote _remote;
    
    public Mono.HomeScreen HomeScreen { get; private set; }

    public HomeFactory(IAssetProvider assetProvider, IRemote remote)
    {
      _assetProvider = assetProvider;
      _remote = remote;
    }

    public Mono.HomeScreen CreateHomeScreen()
    {
      ClearHomeScreen();

      var parent = GameObject.Find(BaseCanvas).transform;
      string currentHomePath = HomePath + (_remote.ShouldEnableFeatureTest() ? "-AB" : string.Empty);
      
      var homeScreen = _assetProvider.GetResource<Mono.HomeScreen>(currentHomePath);
      HomeScreen = Object.Instantiate(homeScreen, parent);

      return HomeScreen;
    }

    public void ClearHomeScreen()
    {
      if (!HomeScreen) return;
      
      HomeScreen.Clear();
      Object.Destroy(HomeScreen.gameObject);
    }
  }
}