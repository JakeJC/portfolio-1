using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Animation;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Localization.Mono;
using _Project.Smooth_Cap_Progress_Bar.Scripts;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;
using Spine.Unity;
using UnityEngine;
using UnityEngine.UI;
using ButtonAnimation = _Project.Scripts.Animation.ButtonAnimation;

namespace _Project.Scripts.Features.HomeScreen.Mono
{
  public class HomeScreen : MonoBehaviour
  {
    public const float AnimDuration = 0.3f;

    [Header("Main")] public Button ShopButton;
    public Button GalleryButton;
    public Button SettingsButton;
    public Button SecretaryButton;
    public Button GamesButton;
    public Button StoriesButton;
    public Button IapShopButton;
    public Button NoAdsButton;
    public Button GirlButton;

    public CustomRadialProgressBar ClickBar;

    [Header("Animations")] public CanvasGroup DarkBackground;
    public RectTransform BottomPanel;

    public CanvasGroup SecretaryCanvasGroup;
    public CanvasGroup GalleryCanvasGroup;
    public CanvasGroup NoAdsCanvasGroup;
    public CanvasGroup ShopCanvasGroup;
    public CanvasGroup InAppCanvasGroup;
    public CanvasGroup MoneyFillerCanvasGroup;

    public CanvasGroup TapToGirl;
    public SkeletonGraphic GirlSkeletonAnimation;

    private bool _girlInAnimation;
    private Coroutine _girlAnimation;

    public CanvasGroup Panels;
    public CanvasGroup GirlCanvasGroup;
    
    private IAudioService _audioService;
    private IRemote _remote;
    
    public void Construct(ILocaleSystem localeSystem, IRemote remote, Action toGallery, Action toSecretary, Action openSettingsWindow,
      Action toShop, Action toGames, Action toStories, Action toIapShop, Action toNoAds, Action onClickBar, IAudioService audioService)
    {
      _audioService = audioService;
      _remote = remote;
      
      ShopButton.onClick.AddListener(toShop.Invoke);
      GamesButton.onClick.AddListener(toGames.Invoke);
      GalleryButton.onClick.AddListener(toGallery.Invoke);
      StoriesButton.onClick.AddListener(toStories.Invoke);
      IapShopButton.onClick.AddListener(toIapShop.Invoke);
      SettingsButton.onClick.AddListener(openSettingsWindow.Invoke);
      SecretaryButton.onClick.AddListener(toSecretary.Invoke);
      NoAdsButton.onClick.AddListener(toNoAds.Invoke);
      GirlButton.onClick.AddListener(() =>
      {
        onClickBar.Invoke();
        _girlAnimation = StartCoroutine(GirlAnimationCooldown());
      });

      CreateAnimations();

      Localize(localeSystem);

      AppearAnimate();

      DisableInAppsButtons();
    }

    public void ShowUI()
    {
      Panels.alpha = 0;
      Panels.DOFade(1, AnimDuration);
      Panels.interactable = true;
      GirlCanvasGroup.interactable = true;
    }

    public void HideUI()
    {
      Panels.alpha = 1;
      Panels.DOFade(0, AnimDuration);
      Panels.interactable = false; 
      GirlCanvasGroup.interactable = false;
    }

    public void ShowSettingsButton() => 
      SettingsButton.GetComponent<Image>().DOFade(1, 0.15f).SetDelay(0.2f);

    public void HideSettingsButton() => 
      SettingsButton.GetComponent<Image>().DOFade(0, 0.15f);

    public void Clear()
    {
      if (_girlAnimation != null)
      {
        StopCoroutine(_girlAnimation);
        _girlAnimation = null;
      }
    }

    IEnumerator GirlAnimationCooldown()
    {
      if (_girlInAnimation)
        yield break;

      GirlSkeletonAnimation.AnimationState.SetAnimation(0, "click", false);
      _girlInAnimation = true;
      
      yield return new WaitForSeconds(GirlSkeletonAnimation.SkeletonData.FindAnimation("click").Duration);
      GirlSkeletonAnimation.AnimationState.SetAnimation(0, "idle1", true);

      yield return new WaitForSeconds(3);
      _girlInAnimation = false;
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Localize();
      });
    }

    public void SetMoneyFill(float fillValue)
    {
      DOTween.To(() =>  ClickBar.FillValue, x =>  ClickBar.FillValue = x, fillValue * 100, .2f)
        .OnStart(delegate { _audioService.Play(AudioId.Karaoke_ProgressBar); })
        .OnComplete(delegate { _audioService.Stop(AudioId.Karaoke_ProgressBar); });
    }

    private void DisableInAppsButtons()
    {
#if DISABLE_INAPPS
      IapShopButton.gameObject.SetActive(false); 
#endif      
    }

    private void CreateAnimations()
    {
      new ButtonAnimation(GamesButton.transform);
      new ButtonAnimation(StoriesButton.transform);
      
      new ButtonAnimation(ShopButton.transform);
      new ButtonAnimation(IapShopButton.transform);
      new ButtonAnimation(SettingsButton.transform);
      new ButtonAnimation(NoAdsButton.transform);
    }

    private void AppearAnimate()
    {
      DarkBackground.alpha = 1;
      BottomPanel.transform.position = Vector3.down;
      BottomPanel.anchoredPosition = BottomPanel.sizeDelta.y * Vector2.down;

      Sequence sequence = DOTween.Sequence();

      sequence.Append(DarkBackground.DOFade(0, 1f));
      sequence.Append(BottomPanel.DOAnchorPosY(0, 0.25f));

      SecretaryCanvasGroup.alpha = 0;
      SecretaryCanvasGroup.transform.localScale = Vector3.zero;

      GalleryCanvasGroup.alpha = 0;
      GalleryCanvasGroup.transform.localScale = Vector3.zero;

      AppearButton(ref sequence, SecretaryCanvasGroup);
      AppearButton(ref sequence, GalleryCanvasGroup);
#if !DISABLE_INAPPS              
      AppearButton(ref sequence, NoAdsCanvasGroup);
#endif
      AppearButton(ref sequence, ShopCanvasGroup);
#if !DISABLE_INAPPS
      AppearButton(ref sequence, InAppCanvasGroup);
#endif
      AppearButton(ref sequence, MoneyFillerCanvasGroup);

      TapToGirl.alpha = 0;
      sequence.Append(TapToGirl.DOFade(1, 1f));

      void AppearButton(ref Sequence seq, CanvasGroup group)
      {
        group.alpha = 0;
        group.transform.localScale = Vector3.zero;

        seq.Append(group.DOFade(1, 0.25f));
        seq.Join(group.transform.DOScale(Vector3.one, 0.25f));
      }
    }
  }
}