using System;
using _Project.Scripts.Features.Lose.Mono;
using _Project.Scripts.Features.NotLoadedWindow;
using _Project.Scripts.Services._Interfaces;
using AppacheAds.Scripts.Interfaces;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.Lose
{
  public class StripLoseLogic
  {
    private const int TargetSeconds = 5;
    private const float ButtonAppearDelay = 1.5f;

    private readonly StripLoseWindowFactory _loseWindowFactory;

    private readonly IAdsService _adsService;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAssetProvider _assetProvider;

    private Mono.Timer _timer;

    private StripLoseScreen _loseScreen;

    private Action _onContinue;

    public StripLoseLogic(IAssetProvider assetProvider, IAdsService adsService, ILocaleSystem localeSystem)
    {
      _assetProvider = assetProvider;
      _adsService = adsService;
      _loseWindowFactory = new StripLoseWindowFactory(assetProvider);
      _localeSystem = localeSystem;
    }

    public void CreateLoseScreen(Action onContinue, Action onLose)
    {
      _onContinue = onContinue;

      _loseScreen = _loseWindowFactory.SpawnLoseWindow();
      _loseScreen.Construct(_localeSystem, Continue, onLose + ToMiniGames);

      //CreateTimer();

      _loseScreen.AppearExitButton(ButtonAppearDelay);
      _loseScreen.AppearElements(0);
      _loseScreen.BlinkHeart();
    }

    public void Clear()
    {
      if (_loseScreen)
        Object.Destroy(_loseScreen.gameObject);

      ClearTimer();
    }

    private void Continue()
    {
      _adsService.Rewarded.ShowReward("restore_game", () =>
      {
        Clear();
        _onContinue?.Invoke();
      }, () =>
      {
        INotLoadedWindowLogic notLoadedWindowLogic = new NotLoadedWindowLogic(_localeSystem, _assetProvider);
        notLoadedWindowLogic.Open();
      });
    }

    private void ToMiniGames()
    {
      _loseScreen.DisappearElements(InterstitialExit);

      void InterstitialExit()
      {
        Clear();
        // _adsService.CurrentInterstitial.ShowInterstitial("start",
        //   Clear,
        //   Clear);
      }
    }

    private void CreateTimer()
    {
      _timer = new GameObject() {name = "Lose Timer"}.AddComponent<Mono.Timer>();
      _timer.Construct(TargetSeconds, Tick, OnTimerEnd);
      _timer.Run();
    }

    private void Tick(float seconds) =>
      _loseScreen.Fill(seconds, TargetSeconds);

    private void OnTimerEnd() =>
      ToMiniGames();

    private void ClearTimer()
    {
      if (_timer)
        Object.Destroy(_timer.gameObject);
    }
  }
}