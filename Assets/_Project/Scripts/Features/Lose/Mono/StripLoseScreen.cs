using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Animation;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Mono;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Lose.Mono
{
  public class StripLoseScreen : MonoBehaviour
  {
    public Button ContinueButton;
    
    public Button MiniGamesButton;
    public CanvasGroup CanvasGroupToHome;
    public CanvasGroup ParentGroup;
    public CanvasGroup Heart;
    
    public TextMeshProUGUI Counter;
    
    public void Construct(ILocaleSystem localeSystem, Action onContinue, Action toMiniGames)
    {
      ContinueButton.onClick.AddListener(onContinue.Invoke);
      MiniGamesButton.onClick.AddListener(toMiniGames.Invoke);

      new ButtonAnimation(ContinueButton.transform);
      new ButtonAnimation(MiniGamesButton.transform);
      
      CanvasGroupToHome.alpha = 0;
      ParentGroup.alpha = 0;

      Localize(localeSystem);
    }

    public void Fill(float seconds, int startValue)
    {
      //Counter.text = (Mathf.FloorToInt(seconds) + 1).ToString();
    }

    public void AppearExitButton(float delay) => 
      CanvasGroupToHome.DOFade(1, 0.25f).SetDelay(delay);

    public void AppearElements(float delay) =>
      ParentGroup.DOFade(1, 0.5f).SetDelay(delay);

    public void DisappearElements(Action onComplete) =>
      ParentGroup.DOFade(0, 0.5f).OnComplete(onComplete.Invoke);

    public void BlinkHeart()
    {
      var sequence = DOTween.Sequence();
      sequence.SetDelay(1.5f);
      sequence.Append(Heart.DOFade(0.1f, 0.05f).SetLoops(8, LoopType.Yoyo).SetEase(Ease.Linear));
      sequence.Append(Heart.DOFade(0, 0.07f).SetLoops(3, LoopType.Yoyo).SetEase(Ease.Linear).SetDelay(0.15f));
      sequence.Append(Heart.DOFade(1, 0.4f).SetEase(Ease.OutBounce));
      sequence.Append(Heart.DOFade(1, 0).SetDelay(1.5f));
      sequence.SetLoops(-1);
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Localize();
      });
    }
  }
}