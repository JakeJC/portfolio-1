using System;
using System.Collections;
using UnityEngine;

namespace _Project.Scripts.Features.Lose.Mono
{
  public class Timer : MonoBehaviour
  {
    private int _seconds;
    
    private Action<float> _onTick;
    private Action _onEnd;

    public void Construct(int seconds, Action<float> onTick, Action onEnd)
    {
      _onEnd = onEnd;
      _onTick = onTick;
      _seconds = seconds;
    }

    public void Run() => 
      StartCoroutine(TimerRun());

    private IEnumerator TimerRun()
    {
      float time = _seconds;
      var delta = new WaitForSeconds(0.1f);
      
      while (time > 0)
      {
        _onTick?.Invoke(time);
        time -= 0.1f;
        
        yield return delta;
      }
      
      _onTick?.Invoke(time);
      _onEnd?.Invoke();
    }
  }
}