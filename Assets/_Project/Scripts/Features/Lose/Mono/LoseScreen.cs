using System;
using _Project.Scripts.Animation;
using _Project.Scripts.Core_Logic;
using _Project.Scripts.Services._Interfaces;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Lose.Mono
{
  public class LoseScreen : MonoBehaviour
  {
    public Image Background;
    public TMP_Text HeaderText;

    public Image Light1Image;
    public Image Light2Image;

    public RectTransform StarTransform;
    public CanvasGroup StarCanvasGroup;

    public Button ContinueButton;
    public CanvasGroup ContinueButtonCanvasGroup;
    public TMP_Text ContinueButtonText;

    public Button MiniGamesButton;
    public CanvasGroup CanvasGroupToHome;
    public TMP_Text NoButtonText;

    public StarBar Bar;
    public TextMeshProUGUI Counter;

    private ILocaleSystem _localeSystem;

    public void Construct(ILocaleSystem localeSystem, Action onContinue, Action toMiniGames)
    {
      _localeSystem = localeSystem;

      ContinueButton.onClick.AddListener(onContinue.Invoke);
      MiniGamesButton.onClick.AddListener(toMiniGames.Invoke);

      CanvasGroupToHome.alpha = 0;

      AssignAnimations();
      Localize();
    }

    public void OpenWindow()
    {
      Background.color = Background.color.SetA(0);

      Counter.color = Counter.color.SetA(0);
      HeaderText.color = HeaderText.color.SetA(0);
      Light1Image.color = Light1Image.color.SetA(0);
      Light2Image.color = Light2Image.color.SetA(0);
      StarTransform.localScale = Vector3.one * 0.4f;
      StarCanvasGroup.alpha = 0f;
      ContinueButtonCanvasGroup.alpha = 0;
      CanvasGroupToHome.alpha = 0;

      gameObject.SetActive(true);

      DOTween.Sequence()
        .Insert(0.0f, Background.DOFade(1f, 0.5f))
        .Insert(0.2f, Counter.DOFade(1f, 0.3f))
        .Insert(0.2f, HeaderText.DOFade(1f, 0.3f))
        .Insert(0.2f, Light1Image.DOFade(1f, 0.3f))
        .Insert(0.2f, Light2Image.DOFade(1f, 0.3f))
        .Insert(0.1f, StarTransform.DOScale(1f, 0.4f))
        .Insert(0.1f, StarCanvasGroup.DOFade(1f, 0.4f))
        .Insert(0.3f, ContinueButtonCanvasGroup.DOFade(1f, 0.3f))
        .AppendInterval(3)
        .Append(CanvasGroupToHome.DOFade(1f, 0.3f));
    }

    public void Fill(float seconds, int startValue)
    {
      Bar.Fill(seconds / startValue);
      Counter.text = (Mathf.FloorToInt(seconds) + 1).ToString();
    }

    public void AppearExitButton(float delay) => 
      CanvasGroupToHome.DOFade(1, 0.25f).SetDelay(delay);

    private void Localize()
    {
      HeaderText.text = _localeSystem.GetText("Main", "TryAgain");
      ContinueButtonText.text = _localeSystem.GetText("Main", "Continue");
      NoButtonText.text = _localeSystem.GetText("Main", "NoThanks");
    }

    private void AssignAnimations()
    {
      new ButtonAnimation(ContinueButton.transform);
      new ButtonAnimation(MiniGamesButton.transform);
    }
  }
}