using _Project.Scripts.Core_Logic;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Lose.Mono
{
  public class StarBar : MonoBehaviour
  {
    public Image[] LightBulbs;
    public Image[] Lights;

    public void Fill(float value)
    {
      int index = Mathf.FloorToInt(value * (Lights.Length - 1));

      for (var i = 0; i < Lights.Length; i++)
      {
        var c = LightBulbs[i].color;
        LightBulbs[i].color = i < index ? c.SetA(1.0f) : c.SetA(0.5f);
        Lights[i].enabled = i < index ? true : false;
      }
    }
  }
}