using _Project.Scripts.Features.Lose.Mono;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.Lose
{
  public class LoseWindowFactory
  {
    private const string LoseScreenPath = "Features/Other/Lose";
    private const string BaseCanvas = "Canvas";

    private readonly IAssetProvider _assetProvider;

    public LoseWindowFactory(IAssetProvider assetProvider) => 
      _assetProvider = assetProvider;

    public LoseScreen SpawnLoseWindow()
    {
      Transform parent = GameObject.Find(BaseCanvas).transform;

      var loseScreen = _assetProvider.GetResource<LoseScreen>(LoseScreenPath);
      return Object.Instantiate(loseScreen, parent);
    }
  }
}