using _Project.Scripts.Features.Lose.Mono;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.Lose
{
  public class StripLoseWindowFactory
  {
    private const string StripLoseScreenPath = "Features/Other/Strip Lose Window";
    private const string BaseCanvas = "Canvas";

    private readonly IAssetProvider _assetProvider;

    public StripLoseWindowFactory(IAssetProvider assetProvider) => 
      _assetProvider = assetProvider;

    public StripLoseScreen SpawnLoseWindow()
    {
      Transform parent = GameObject.Find(BaseCanvas).transform;

      var loseScreen = _assetProvider.GetResource<StripLoseScreen>(StripLoseScreenPath);
      return Object.Instantiate(loseScreen, parent);
    }
  }
}