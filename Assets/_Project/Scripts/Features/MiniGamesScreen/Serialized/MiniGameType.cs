using UnityEngine;

namespace _Project.Scripts.Features.MiniGamesScreen.Serialized
{
  public enum MiniGameType
  {
    Quiz,
    StripTaper,
    SoonMock,
    StripTaperLevels,
    QuizProgress,
  }
}