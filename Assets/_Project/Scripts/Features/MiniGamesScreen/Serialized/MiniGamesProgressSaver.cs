using System;
using UnityEngine;

namespace _Project.Scripts.Features.MiniGamesScreen.Serialized
{
  [Serializable]
  public class MiniGamesProgressSaver
  {
    [SerializeField] private MiniGameType _type;
    [SerializeField] private int _index;

    public int Index
    {
      get => _index;
      set => _index = value;
    }

    public MiniGameType Type => _type;

    public MiniGamesProgressSaver(MiniGameType type, int index)
    {
      _type = type;
      Index = index;
    }
  }
}