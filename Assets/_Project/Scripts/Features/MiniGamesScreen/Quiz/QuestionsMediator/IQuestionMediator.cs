using _Project.Scripts.Features.Data;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsMediator
{
  public interface IQuestionMediator
  {
    void LaunchGame<TData>(TData data, BaseContentData mainData) where TData : BaseContentData;
    void EndGame(bool isWin);
  }
}