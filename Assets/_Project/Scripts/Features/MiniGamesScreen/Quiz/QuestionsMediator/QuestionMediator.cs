using System;
using System.Collections.Generic;
using _Project.Scripts.Features.BateWindow;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.Lose;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionData;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Interfaces;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono;
using _Project.Scripts.Features.MiniGamesScreen.Serialized;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio.Enum;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsMediator
{
  public class QuestionMediator : IQuestionMediator
  {
    private const int SuperWinRewardMultiplier = 2;
    private readonly Dictionary<Type, IQuestionLogic> _gameTypes;

    private IQuestionLogic _currentLevelLogic;
    private readonly ILocaleSystem _localeSystem;
    private readonly IGameSaveSystem _gameSaveSystem;

    private readonly LoseLogic _loseLogic;
    private readonly BateWindowLogic _bateWindowLogic;

    private readonly MiniGameData _miniGameData;
    private readonly IAudioService _audioService;
    private readonly IAdsService _adsService;
    private readonly IMoneyService _moneyService;
    private BaseContentData _baseContentData;

    private Action _onExit;
    private IAnalytics _analytics;
    private BaseContentData _currentGirlData;
    private readonly IRemote _remote;
    private readonly Action _onRenter;
    private readonly IUnlockService _unlockService;

    private int QuizLaunchCount
    {
      get => PlayerPrefs.GetInt("QuizLaunchCount", 0);
      set => PlayerPrefs.SetInt("QuizLaunchCount", value);
    }

    public QuestionMediator(IAnalytics analytics, IAssetProvider assetProvider, IGameSaveSystem gameSaveSystem,
      ILocaleSystem localeSystem, LoseLogic loseLogic, BateWindowLogic bateWindowLogic, MiniGameData miniGameData,
      IAudioService audioService, IAdsService adsService, IMoneyService moneyService, Action onExit, IRemote remote, Action onRenter, IUnlockService unlockService)
    {
      _analytics = analytics;
      _localeSystem = localeSystem;
      _gameSaveSystem = gameSaveSystem;

      _loseLogic = loseLogic;
      _miniGameData = miniGameData;
      _audioService = audioService;
      _adsService = adsService;
      _moneyService = moneyService;
      _bateWindowLogic = bateWindowLogic;
      _onExit = onExit;
      _remote = remote;
      _onRenter = onRenter;
      _unlockService = unlockService;

      _gameTypes = new Dictionary<Type, IQuestionLogic>
      {
        [typeof(TextQuestionData)] = new TextQuestionLogic(_localeSystem, this, assetProvider, _audioService, _adsService, _moneyService, _gameSaveSystem, 
          Clear, _remote, _unlockService, _analytics),
      };
    }

    public void LaunchGame<TData>(TData data, BaseContentData currentGirlData) where TData : BaseContentData
    {
      _currentGirlData = currentGirlData;
      _baseContentData = data;

      Type type = data.GetType();
      _currentLevelLogic = _gameTypes[type];
      _currentLevelLogic.Enter(data, HintEvent, LoadLevelProgress, LoadBarProgress);

      QuizLaunchCount++;

      _analytics.Send("ev_game_start", new Dictionary<string, object>
      {
        { "game_id", _miniGameData.Id },
        { "character_id", _currentGirlData.Id },
        { "try_number", QuizLaunchCount },
        { "game_lvl ", LoadLevelProgress() + 1 }
      });
    }

    private void HintEvent()
    {
      _analytics.Send("btn_help", new Dictionary<string, object>
      {
        { "game_id", _miniGameData.Id },
        { "character_id", _currentGirlData.Id },
        { "try_number", QuizLaunchCount }
      });
    }

    public void EndGame(bool isWin)
    {
      if (isWin)
      {
        SaveBarProgress(LoadBarProgress() + 1);
        int progressStep = LoadBarProgress();

        if (progressStep % QuizProgressBar.ProgressbarSteps == 0)
        {
          Debug.Log("SuperWin");
          SuperWin();
          return;
        }

        Clear();
        Win();
      }
      else
        Lose();
    }

    private void Win()
    {
      Debug.Log("Win");
      _audioService.Play(AudioId.Karaoke_Win);

      Action onCloseBate = delegate { _onRenter.Invoke(); };

      _bateWindowLogic.Open(_currentGirlData as GirlData, _miniGameData, onCloseBate, true);

      _analytics.Send("ev_game_finish", new Dictionary<string, object>
      {
        { "game_id", _miniGameData.Id },
        { "character_id", _currentGirlData.Id },
        { "result", "win" },
        { "try_number", QuizLaunchCount }
      });
    }

    private void SuperWin()
    {
      Debug.Log("Super Win");
      _audioService.Play(AudioId.Karaoke_Win);
      SaveLevelProgress(LoadLevelProgress() + 1);

      _analytics.Send("ev_game_finish", new Dictionary<string, object>
      {
        { "game_id", _miniGameData.Id },
        { "character_id", _currentGirlData.Id },
        { "result", "win" },
        { "try_number", QuizLaunchCount }
      });

      var data = _gameSaveSystem.Get();
      data.FiftyFiftyHelpIsUsed = false;
      data.QuizAssistantHelpIsUsed = false;
      _gameSaveSystem.Save();

      LevelDataWithVariantsAndOneAnswer rewardData = (LevelDataWithVariantsAndOneAnswer)_baseContentData;

      if (_currentLevelLogic.GetQuestionUI().CheckContent(rewardData.Reward, delegate
      {
        Clear();

        if (LoadLevelProgress() < MiniGamesLogic.LevelsMax)
          _onRenter.Invoke();
      }))
        return;

      _currentLevelLogic.GetQuestionUI().CheckContent(rewardData.Reward, Clear);
      _bateWindowLogic.Open(_currentGirlData as GirlData, _miniGameData, null, true, SuperWinRewardMultiplier);
    }

    private void WinWithoutProgressBar()
    {
      Debug.Log("WinWithoutProgressBar");
      _audioService.Play(AudioId.Karaoke_Win);
      SaveLevelProgress(LoadLevelProgress() + 1);

      LevelDataWithVariantsAndOneAnswer rewardData = (LevelDataWithVariantsAndOneAnswer)_baseContentData;

      _analytics.Send("ev_game_finish", new Dictionary<string, object>
      {
        { "game_id", _miniGameData.Id },
        { "character_id", _currentGirlData.Id },
        { "result", "win" },
        { "try_number", QuizLaunchCount }
      });

      if (rewardData.Reward == null || IsUnlocked(rewardData.Reward))
      {
        _bateWindowLogic.Open(_currentGirlData as GirlData, _miniGameData);
        _currentLevelLogic.Exit();
        return;
      }

      _currentLevelLogic.ShowReward(Clear);

      bool IsUnlocked(GalleryData galleryData)
      {
        return _gameSaveSystem.Get().UnlockedStoriesContent.Contains(galleryData.Id) ||
               _gameSaveSystem.Get().UnlockedPhotoContent.Contains(galleryData.Id);
      }
    }

    private void Clear()
    {
      _currentLevelLogic.Exit();
      _onExit?.Invoke();
    }

    private void Lose()
    {
      Action onLose = null;

      _currentLevelLogic.Exit();
      _audioService.Play(AudioId.Karaoke_Lose);
      _loseLogic.CreateLoseScreen(() => _currentLevelLogic.Enter(_baseContentData, HintEvent, LoadLevelProgress, LoadBarProgress), onLose);

      _analytics.Send("ev_game_finish", new Dictionary<string, object>
      {
        { "game_id", _miniGameData.Id },
        { "character_id", _currentGirlData.Id },
        { "result", "loss" },
        { "try_number", QuizLaunchCount }
      });
    }

    private void SaveLevelProgress(int newIndex)
    {
      _gameSaveSystem.Get().OnSaveMiniGameProgress(MiniGameType.Quiz, newIndex);
      _gameSaveSystem.Save();
    }

    private void SaveBarProgress(int newIndex)
    {
      _gameSaveSystem.Get().OnSaveMiniGameProgress(MiniGameType.QuizProgress, newIndex);
      _gameSaveSystem.Save();
    }

    private int LoadLevelProgress() =>
      _gameSaveSystem.Get().OnLoadMiniGameProgress(MiniGameType.Quiz);

    private int LoadBarProgress() =>
      _gameSaveSystem.Get().OnLoadMiniGameProgress(MiniGameType.QuizProgress);
  }
}