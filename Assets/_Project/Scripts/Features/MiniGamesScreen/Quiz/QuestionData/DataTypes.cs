using System;
using UnityEngine;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionData
{
  public static class DataTypes
  {
    [Serializable]
    public struct Picture
    {
      public Sprite Sprite;
      public string Id;
    }
  }
}