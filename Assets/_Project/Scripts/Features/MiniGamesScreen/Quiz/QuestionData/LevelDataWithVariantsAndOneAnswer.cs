using _Project.Scripts.Features.Data;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionData
{
  public abstract class LevelDataWithVariantsAndOneAnswer : BaseContentData
  {
    public string[] Variants;
    public string CorrectAnswer;
    public GalleryData Reward;
  }
}