using UnityEngine;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionData
{
  [CreateAssetMenu(fileName = "Picture Question Data", menuName = "Level Data/Picture Question Data", order = 0)]
  public class PictureQuestionData : LevelDataWithVariantsAndOneAnswer
  {
    public Sprite Picture;
  }
}