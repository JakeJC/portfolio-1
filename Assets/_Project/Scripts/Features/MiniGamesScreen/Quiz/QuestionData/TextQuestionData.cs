using UnityEngine;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionData
{  
  [CreateAssetMenu(fileName = "Text Question Data", menuName = "Level Data/Text Question Data", order = 0)]
  public class TextQuestionData : LevelDataWithVariantsAndOneAnswer
  {
    public string QuestionText;
  }
}