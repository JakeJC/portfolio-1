using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionData
{ 
  [CreateAssetMenu(fileName = "Emoji Question Data", menuName = "Level Data/Emoji Question Data", order = 0)]
  public class EmojiQuestionData : LevelDataWithVariantsAndOneAnswer
  {
    public List<string> EmojiSequence;
  }
}