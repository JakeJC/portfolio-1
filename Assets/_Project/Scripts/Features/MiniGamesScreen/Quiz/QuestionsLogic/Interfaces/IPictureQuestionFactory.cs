using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Interfaces
{
  public interface IPictureQuestionFactory
  {
    PictureQuestionUI PictureQuestionUI { get; }
    PictureQuestionUI CreatePictureQuestionUI();
    void Clear();
  }
}