using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Interfaces
{
  public interface IEmojiQuestionFactory
  {
    EmojiQuestionUI EmojiQuestionUI { get; }
    EmojiQuestionUI CreateEmojiQuestionUI();
    void Clear();
  }
}