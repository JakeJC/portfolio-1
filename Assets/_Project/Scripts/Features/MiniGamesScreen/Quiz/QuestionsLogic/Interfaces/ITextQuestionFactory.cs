using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Interfaces
{
  public interface ITextQuestionFactory
  {
    TextQuestionUI TextQuestionUI { get; }
    TextQuestionUI CreateTextQuestionUI();
    void Clear();
  }
}