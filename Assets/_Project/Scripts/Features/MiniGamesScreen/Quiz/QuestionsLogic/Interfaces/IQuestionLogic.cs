using System;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Interfaces
{
  public interface IQuestionLogic
  {
    QuestionUI GetQuestionUI();

    void Enter<TData>(TData data, Action hintEvent, Func<int> getLevel, Func<int> getProgress) where TData : BaseContentData;
    void Exit();
    void Hint();
    void ShowReward(Action onEnd);
  }
}