using System;
using System.Linq;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionData;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Abstract;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Factories;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Interfaces;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsMediator;
using _Project.Scripts.Features.NotLoadedWindow;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Money;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using TMPro;
using UnityEngine.UI;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic
{
  public class EmojiQuestionLogic : BaseQuestionLogic, IQuestionLogic
  {
    private readonly IMoneyService _moneyService;
    private readonly IAudioService _audioService;
    private readonly IUiFactory _uiFactory;

    private MoneyCounterLogic _moneyCounterLogic;

    private IEmojiQuestionFactory _emojiQuestionFactory;

    private EmojiQuestionData _emojiQuestionData;

    private readonly Action _onBack;
    private readonly IUnlockService _unlockService;
    private Action _hintEvent;
    private EmojiQuestionUI _questionUI;

    public EmojiQuestionLogic(
      ILocaleSystem localeSystem, IQuestionMediator questionMediator, IAssetProvider assetProvider,
      IAudioService audioService, IAdsService adsService, IMoneyService moneyService,
      Action onBack, IRemote remote, IGameSaveSystem gameSaveSystem, IUnlockService unlockService, IAnalytics analytics)
      : base(questionMediator, remote, localeSystem, assetProvider, gameSaveSystem, adsService, analytics)
    {
      _onBack = onBack;
      _unlockService = unlockService;
      _audioService = audioService;
      _moneyService = moneyService;

      _uiFactory = new UiFactory(assetProvider, remote, localeSystem);
    }

    public override QuestionUI GetQuestionUI() =>
      _questionUI;

    protected override Button GetCorrectButton() =>
      _emojiQuestionFactory.EmojiQuestionUI.AnswersButtons.FirstOrDefault(
        p => p.GetComponentInChildren<TextMeshProUGUI>().text.Equals(_emojiQuestionData.CorrectAnswer));

    public LevelDataWithVariantsAndOneAnswer GetLevelDataWithVariantsAndOneAnswer() =>
      _emojiQuestionData;

    public void Enter<TData>(TData data, Action hintEvent, Func<int> getLevel, Func<int> getProgress) where TData : BaseContentData
    {
      _hintEvent = hintEvent;

      _emojiQuestionData = data as EmojiQuestionData;
      _emojiQuestionFactory = new EmojiQuestionFactory(_gameSaveSystem, _audioService, _localeSystem, _assetProvider, _emojiQuestionData, Choose, Next, Hint, _onBack,
        _remote, getLevel, getProgress, AssistantHelp, FiftyFifty);

      CreateGameUI();
      OpenGameUI();
    }

    public void Exit()
    {
      _emojiQuestionFactory.Clear();
      _moneyCounterLogic.Clear();
    }

    public void Hint()
    {
      _hintEvent?.Invoke();

      Button correctButton = _emojiQuestionFactory.EmojiQuestionUI.AnswersButtons.FirstOrDefault(
        p => p.GetComponentInChildren<TextMeshProUGUI>().text.Equals(_emojiQuestionData.CorrectAnswer));

      correctButton.onClick.Invoke();
    }

    public void ShowReward(Action onEnd)
    {
      _emojiQuestionFactory.EmojiQuestionUI.ShowReward(onEnd);
    }

    protected override bool IsAnswerCorrect(string userAnswerId)
    {
      var isCorrect = _emojiQuestionData.CorrectAnswer.Equals(userAnswerId);
      return isCorrect;
    }

    protected override void AddProgressBarStep() =>
      _questionUI.AddProgressBarStep();

    private void CreateGameUI()
    {
      _questionUI = _emojiQuestionFactory.CreateEmojiQuestionUI();
      _moneyCounterLogic = new MoneyCounterLogic(_assetProvider, _uiFactory, _localeSystem, _moneyService, _audioService, _adsService, _remote, _unlockService, _gameSaveSystem, _analytics);
    }

    private void OpenGameUI()
    {
      _emojiQuestionFactory.EmojiQuestionUI.SetMoneyCounter(_moneyCounterLogic);
      _emojiQuestionFactory.EmojiQuestionUI.Open();
    }

    private void Choose(string userAnswerId)
    {
      bool isCorrect = IsAnswerCorrect(userAnswerId);
      _emojiQuestionFactory.EmojiQuestionUI.UpdateNeonSign(isCorrect);

      if (isCorrect)
        AddProgressBarStep();
    }
  }
}