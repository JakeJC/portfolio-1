using System;
using System.Linq;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionData;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Abstract;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Factories;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Interfaces;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsMediator;
using _Project.Scripts.Features.NotLoadedWindow;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Money;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using TMPro;
using UnityEngine.UI;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic
{
  public class PictureQuestionLogic : BaseQuestionLogic, IQuestionLogic
  {
    private readonly IMoneyService _moneyService;
    private readonly IAudioService _audioService;
    private readonly IUiFactory _uiFactory;
    
    private MoneyCounterLogic _moneyCounterLogic;

    private IPictureQuestionFactory _pictureQuestionFactory;

    private PictureQuestionData _pictureQuestionData;

    private readonly Action _onBack;
    private readonly IUnlockService _unlockService;
    private Action _hintEvent;
    private PictureQuestionUI _questionUI;

    public PictureQuestionLogic(
      ILocaleSystem localeSystem, IQuestionMediator questionMediator, IAssetProvider assetProvider,
      IAudioService audioService, IAdsService adsService, IMoneyService moneyService, IGameSaveSystem gameSaveSystem,
      Action onBack, IRemote remote, IUnlockService unlockService, IAnalytics analytics) : base(questionMediator, remote, localeSystem, assetProvider, gameSaveSystem, adsService, analytics)
    {
      _onBack = onBack;
      _unlockService = unlockService;
      _audioService = audioService;
      _moneyService = moneyService;

      _uiFactory = new UiFactory(assetProvider, remote, localeSystem);
    }
    public override QuestionUI GetQuestionUI() => 
      _questionUI;
    
    protected override Button GetCorrectButton() =>
      _pictureQuestionFactory.PictureQuestionUI.AnswersButtons.FirstOrDefault(
        p => p.GetComponentInChildren<TextMeshProUGUI>().text.Equals(_pictureQuestionData.CorrectAnswer));

    public LevelDataWithVariantsAndOneAnswer GetLevelDataWithVariantsAndOneAnswer() => 
      _pictureQuestionData;

    public void Enter<TData>(TData data, Action hintEvent, Func<int> getLevel,  Func<int> getProgress) where TData : BaseContentData
    {
      _hintEvent = hintEvent;
      
      _pictureQuestionData = data as PictureQuestionData;
      _pictureQuestionFactory = new PictureQuestionFactory(_gameSaveSystem, _audioService,_localeSystem, _assetProvider,
        _pictureQuestionData, Choose, Next, Hint, _onBack,
        _remote, getLevel,getProgress, AssistantHelp, FiftyFifty);

      CreateGameUI();
      OpenGameUI();
    }

    public void Exit()
    {
      _pictureQuestionFactory.Clear();
      _moneyCounterLogic.Clear();
    }

    public void Hint()
    {
      _hintEvent?.Invoke();
      
      Button correctButton = _pictureQuestionFactory.PictureQuestionUI.AnswersButtons.FirstOrDefault(
        p => p.GetComponentInChildren<TextMeshProUGUI>().text.Equals(_pictureQuestionData.CorrectAnswer));

      correctButton.onClick.Invoke();
    }

    public void ShowReward(Action onEnd)
    {
      _pictureQuestionFactory.PictureQuestionUI.ShowReward(onEnd);
    }

    protected override bool IsAnswerCorrect(string userAnswerId)
    {
      var isCorrect = _pictureQuestionData.CorrectAnswer.Equals(userAnswerId);
      return isCorrect;
    }
    
    protected override void AddProgressBarStep() => 
      _questionUI.AddProgressBarStep();

    private void CreateGameUI()
    {
      _questionUI = _pictureQuestionFactory.CreatePictureQuestionUI();
      _moneyCounterLogic = new MoneyCounterLogic(_assetProvider, _uiFactory, _localeSystem, _moneyService, _audioService, _adsService, _remote, _unlockService,
        _gameSaveSystem, _analytics);
    }

    private void OpenGameUI()
    {
      _pictureQuestionFactory.PictureQuestionUI.SetMoneyCounter(_moneyCounterLogic);
      _pictureQuestionFactory.PictureQuestionUI.Open();
    }

    private void Choose(string userAnswerId)
    {
      bool isCorrect = IsAnswerCorrect(userAnswerId);
      _pictureQuestionFactory.PictureQuestionUI.UpdateNeonSign(isCorrect);
      
      if (isCorrect)
        AddProgressBarStep();
    }
  }
}