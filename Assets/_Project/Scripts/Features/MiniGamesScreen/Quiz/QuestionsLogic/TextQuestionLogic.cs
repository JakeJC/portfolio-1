using System;
using System.Linq;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionData;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Abstract;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Factories;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Interfaces;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsMediator;
using _Project.Scripts.Features.NotLoadedWindow;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Money;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic
{
  public class TextQuestionLogic : BaseQuestionLogic, IQuestionLogic
  {
    private readonly IMoneyService _moneyService;
    private readonly IAudioService _audioService;
    private readonly IUiFactory _uiFactory;

    private MoneyCounterLogic _moneyCounterLogic;

    private ITextQuestionFactory _textQuestionFactory;

    private TextQuestionData _textQuestionData;

    private readonly Action _onBack;
    private readonly IUnlockService _unlockService;
    private Action _onHint;
    private TextQuestionUI _questionUI;

    public TextQuestionLogic(ILocaleSystem localeSystem, IQuestionMediator questionMediator, IAssetProvider assetProvider,
      IAudioService audioService, IAdsService adsService, IMoneyService moneyService, IGameSaveSystem gameSaveSystem,
      Action onBack, IRemote remote, IUnlockService unlockService, IAnalytics analytics)
      : base(questionMediator, remote, localeSystem, assetProvider, gameSaveSystem, adsService, analytics)
    {
      _onBack = onBack;
      _unlockService = unlockService;
      _audioService = audioService;
      _moneyService = moneyService;

      _uiFactory = new UiFactory(assetProvider, remote, localeSystem);
    }

    public override QuestionUI GetQuestionUI() =>
      _questionUI;

    protected override Button GetCorrectButton() =>
      _textQuestionFactory.TextQuestionUI.AnswersButtons.FirstOrDefault(
        p => p.GetComponentInChildren<TextMeshProUGUI>().text.Equals(Locale(_textQuestionData.CorrectAnswer)));

    public LevelDataWithVariantsAndOneAnswer GetLevelDataWithVariantsAndOneAnswer() =>
      _textQuestionData;

    public void Enter<TData>(TData data, Action hintEvent, Func<int> getLevel, Func<int> getProgress) where TData : BaseContentData
    {
      _onHint = hintEvent;

      _textQuestionData = data as TextQuestionData;
      _textQuestionFactory = new TextQuestionFactory(_localeSystem, _audioService, _assetProvider, _gameSaveSystem, _textQuestionData, Choose, Next, Hint, _onBack,
        _remote, getLevel, getProgress, AssistantHelp, FiftyFifty);

      CreateGameUI();
      OpenGameUI();
    }

    public void Exit()
    {
      _textQuestionFactory.Clear();
      _moneyCounterLogic.Clear();
    }

    public void ShowReward(Action onEnd)
    {
      _textQuestionFactory.TextQuestionUI.ShowReward(onEnd);
    }

    public void Hint()
    {
      _onHint?.Invoke();

      _adsService.Rewarded.ShowReward("hint", () =>
      {
        Button correctButton = _textQuestionFactory.TextQuestionUI.AnswersButtons.FirstOrDefault(
          p => p.GetComponentInChildren<TextMeshProUGUI>().text.Equals(Locale(_textQuestionData.CorrectAnswer)));

        correctButton.onClick.Invoke();

        _textQuestionFactory.TextQuestionUI.SetCorrect(correctButton);
      }, () =>
      {
        INotLoadedWindowLogic notLoadedWindowLogic = new NotLoadedWindowLogic(_localeSystem, _assetProvider);
        notLoadedWindowLogic.Open();
      });
    }

    protected override bool IsAnswerCorrect(string userAnswerId)
    {
      // var isCorrect = Locale(_textQuestionData.CorrectAnswer).Equals(Locale(userAnswerId));
      var isCorrect = _textQuestionData.CorrectAnswer.Equals(userAnswerId);
      return isCorrect;
    }

    protected override void AddProgressBarStep() =>
      _questionUI.AddProgressBarStep();

    private void CreateGameUI()
    {
      _questionUI = _textQuestionFactory.CreateTextQuestionUI();
      
      _moneyCounterLogic = new MoneyCounterLogic(_assetProvider, _uiFactory, _localeSystem, _moneyService, _audioService, _adsService, _remote, _unlockService, _gameSaveSystem, _analytics);
    }

    private void OpenGameUI()
    {
      _textQuestionFactory.TextQuestionUI.SetMoneyCounter(_moneyCounterLogic);
      _textQuestionFactory.TextQuestionUI.Open();
    }

    private string Locale(string key) =>
      _localeSystem.GetText("Quests", key);

    private void Choose(string userAnswerId)
    {
      bool isCorrect = IsAnswerCorrect(userAnswerId);
      _textQuestionFactory.TextQuestionUI.UpdateNeonSign(isCorrect);

      if (isCorrect)
        AddProgressBarStep();
    }
  }
}