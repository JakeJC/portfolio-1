using System;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionData;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Interfaces;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.Money;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Factories
{
  public class TextQuestionFactory : QuestionFactory, ITextQuestionFactory
  {
    private const string UIPictureQuestionPath = "Features/MiniGamesScreen/Quiz/Text";
    private const string ParentPath = "Canvas";

    public TextQuestionUI TextQuestionUI { get; private set; }

    private readonly TextQuestionData _textQuestionData;

    private readonly Action _onBack;
    private readonly Action _onHint;
    private readonly Action<string> _onChoose;
    private readonly Action<bool> _onEnd;

    public TextQuestionFactory(ILocaleSystem localeSystem, IAudioService audioService, IAssetProvider assetProvider, IGameSaveSystem gameSaveSystem,
      TextQuestionData textQuestionData, Action<string> onChoose, Action<bool> onEnd, Action onHint, Action onBack,
      IRemote remote, Func<int> getLevel, Func<int> getProgress, UnityAction onAssistantHelp, UnityAction onFiftyFifty)
      : base(gameSaveSystem, audioService, localeSystem, assetProvider, remote, getLevel, getProgress, onAssistantHelp, onFiftyFifty)
    {
      _onBack = onBack;
      _onHint = onHint;
      _onChoose = onChoose;
      _onEnd = onEnd;
      _textQuestionData = textQuestionData;
    }

    public TextQuestionUI CreateTextQuestionUI()
    {
      Transform parent = GameObject.Find(ParentPath).transform;

      var prefab = _assetProvider.GetResource<TextQuestionUI>(UIPictureQuestionPath);
      TextQuestionUI = Object.Instantiate(prefab, parent);

      TextQuestionUI.Construct(_gameSaveSystem, _audioService, _localeSystem, _remote, _textQuestionData, _onChoose, _onEnd, _onHint, _onBack, OnSavePhoto,
        CreateTextQuestionUI(TextQuestionUI.ProgressBarPanel), _getLevel, _getProgress, _onAssistantHelp, _onFiftyFifty);

      return TextQuestionUI;
    }

    public void Clear()
    {
      if (TextQuestionUI)
        TextQuestionUI.Close();
    }

    private void OnSavePhoto(string id)
    {
      _gameSaveSystem.Get().UnlockedPhotoContent.Add(id);
      _gameSaveSystem.Save();
    }
  }
}