using System;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionData;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Interfaces;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono;
using _Project.Scripts.Services._Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Factories
{
  public class EmojiQuestionFactory : QuestionFactory, IEmojiQuestionFactory
  {
    private const string UIEmojiQuestionPath = "Features/MiniGamesScreen/Quiz/Emoji";
    private const string ParentPath = "Canvas";

    private readonly IRemote _remote;

    public EmojiQuestionUI EmojiQuestionUI { get; private set; }

    private readonly EmojiQuestionData _emojiQuestionData;

    private readonly Action _onBack;
    private readonly Action _onHint;
    private readonly Action<string> _onChoose;
    private readonly Action<bool> _onEnd;

    public EmojiQuestionFactory(IGameSaveSystem gameSaveSystem, IAudioService audioService, ILocaleSystem localeSystem, IAssetProvider assetProvider, EmojiQuestionData emojiQuestionData,
      Action<string> onChoose, Action<bool> onEnd, Action onHint, Action onBack,
      IRemote remote, Func<int> getLevel, Func<int> getProgress, UnityAction onAssistantHelp, UnityAction onFiftyFifty)
      : base(gameSaveSystem, audioService,localeSystem,assetProvider, remote, getLevel, getProgress, onAssistantHelp, onFiftyFifty)
    {
      _onBack = onBack;
      _onHint = onHint;
      _onChoose = onChoose;
      _onEnd = onEnd;
      _emojiQuestionData = emojiQuestionData;
    }

    public EmojiQuestionUI CreateEmojiQuestionUI()
    {
      var prefab = _assetProvider.GetResource<EmojiQuestionUI>(UIEmojiQuestionPath);
      EmojiQuestionUI = Object.Instantiate(prefab, GameObject.Find(ParentPath).transform);

      EmojiQuestionUI.Construct(_gameSaveSystem, _audioService, _localeSystem, _remote, _emojiQuestionData, _onChoose, _onEnd, _onHint, _onBack,OnSavePhoto,
        CreateTextQuestionUI(EmojiQuestionUI.ProgressBarPanel), _getLevel,_getProgress, _onAssistantHelp, _onFiftyFifty);

      return EmojiQuestionUI;
    }

    public void Clear()
    {
      if (EmojiQuestionUI)
        Object.Destroy(EmojiQuestionUI.gameObject);
    }
  }
}