using System;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionData;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Interfaces;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono;
using _Project.Scripts.Services._Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Factories
{
  public class PictureQuestionFactory : QuestionFactory, IPictureQuestionFactory
  {
    private const string UIPictureQuestionPath = "Features/MiniGamesScreen/Quiz/Picture";
    private const string ParentPath = "Canvas";

    public PictureQuestionUI PictureQuestionUI { get; private set; }

    private readonly PictureQuestionData _pictureQuestionData;

    private readonly Action _onBack;
    private readonly Action _onHint;
    private readonly Action<string> _onChoose;
    private readonly Action<bool> _onEnd;

    public PictureQuestionFactory(IGameSaveSystem gameSaveSystem, IAudioService audioService, ILocaleSystem localeSystem, IAssetProvider assetProvider, PictureQuestionData pictureQuestionData,
      Action<string> onChoose, Action<bool> onEnd, Action onHint, Action onBack,
      IRemote remote, Func<int> getLevel, Func<int> getProgress, UnityAction onAssistantHelp, UnityAction onFiftyFifty)
      : base(gameSaveSystem, audioService, localeSystem, assetProvider, remote, getLevel, getProgress, onAssistantHelp, onFiftyFifty)
    {
      _onHint = onHint;
      _onBack = onBack;
      _onChoose = onChoose;
      _onEnd = onEnd;
      _pictureQuestionData = pictureQuestionData;
    }

    public PictureQuestionUI CreatePictureQuestionUI()
    {
      var prefab = _assetProvider.GetResource<PictureQuestionUI>(UIPictureQuestionPath);
      PictureQuestionUI = Object.Instantiate(prefab, GameObject.Find(ParentPath).transform);

      PictureQuestionUI.Construct(_gameSaveSystem,_audioService, _localeSystem, _remote, _pictureQuestionData, _onChoose, _onEnd, _onHint, _onBack, OnSavePhoto,
        CreateTextQuestionUI(PictureQuestionUI.ProgressBarPanel), _getLevel, _getProgress, _onAssistantHelp, _onFiftyFifty);

      return PictureQuestionUI;
    }

    public void Clear()
    {
      if (PictureQuestionUI)
        Object.Destroy(PictureQuestionUI.gameObject);
    }
  }
}