using System;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono;
using _Project.Scripts.Services._Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

public abstract class QuestionFactory
{
  private const string ProgressBarPath = "Features/MiniGamesScreen/Quiz/Elements/Quiz Progress Bar";

  protected readonly IAssetProvider _assetProvider;
  protected readonly IRemote _remote;
  protected readonly IGameSaveSystem _gameSaveSystem;
  protected readonly IAudioService _audioService;
  protected readonly ILocaleSystem _localeSystem;

  protected readonly Func<int> _getLevel;
  protected readonly Func<int> _getProgress;

  protected readonly UnityAction _onAssistantHelp;
  protected readonly UnityAction _onFiftyFifty;

  protected QuestionFactory(IGameSaveSystem gameSaveSystem, IAudioService audioService, ILocaleSystem localeSystem, IAssetProvider assetProvider, IRemote remote,
    Func<int> getLevel, Func<int> getProgress, UnityAction onAssistantHelp, UnityAction onFiftyFifty)
  {
    _gameSaveSystem = gameSaveSystem;
    _audioService = audioService;
    _localeSystem = localeSystem;
    _assetProvider = assetProvider;
    _remote = remote;
    _getLevel = getLevel;
    _getProgress = getProgress;

    _onAssistantHelp = onAssistantHelp;
    _onFiftyFifty = onFiftyFifty;
  }

  protected QuizProgressBar CreateTextQuestionUI(RectTransform parent) =>
    Object.Instantiate(_assetProvider.GetResource<QuizProgressBar>(ProgressBarPath), parent);

  protected void OnSavePhoto(string id)
  {
    _gameSaveSystem.Get().UnlockedPhotoContent.Add(id);
    _gameSaveSystem.Save();
  }
}