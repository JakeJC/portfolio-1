﻿using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono
{
  public class QuizNeonSign : MonoBehaviour
  {
    [SerializeField] private CanvasGroup container;
    [SerializeField] private Image iconVinyl;
    [SerializeField] private Image iconTruth;
    [SerializeField] private Image iconFalse;

    public CanvasGroup Container => container;
    public Image IconVinyl => iconVinyl;
    public Image IconTruth => iconTruth;
    public Image IconFalse => iconFalse;
  }
}