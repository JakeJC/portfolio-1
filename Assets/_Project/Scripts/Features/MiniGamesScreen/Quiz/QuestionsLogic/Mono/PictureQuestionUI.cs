using System;
using System.Collections;
using System.Collections.Generic;
using _Project.Scripts.Animation;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.GalleryScreen.Serialized;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionData;
using _Project.Scripts.Services._Interfaces;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono
{
  public class PictureQuestionUI : QuestionUI
  {
    public Image Image;
    public Button HintButton;
    public CanvasGroup HintButtonCanvasGroup;
    public Button BackButton;
    public RectTransform FieldText;
    public CanvasGroup FieldTextCanvasGroup;
    public QuizNeonSign NeonSign;

    private Button _rewardExitButton;
    private PictureQuestionData _imageQuestionData;

    private Action _onBack;
    private Action _onHint;
    private Action<bool> _onEnd;

    public void Construct(IGameSaveSystem gameSaveSystem, IAudioService _audioService, ILocaleSystem localeSystem, IRemote remote,
      PictureQuestionData imageQuestionData, Action<string> onChoose, Action<bool> onEnd, Action onHint, Action onBack, Action<string> onSavePhoto,
      QuizProgressBar quizProgressBar, Func<int> level, Func<int> progressStep, UnityAction onAssistantHelp, UnityAction onFiftyFifty)
    {
      BaseConstruct(gameSaveSystem, _audioService, localeSystem, remote, quizProgressBar, onSavePhoto, onChoose, level, progressStep, onAssistantHelp, onFiftyFifty);

      _onBack = onBack;
      _onHint = onHint;
      _onEnd = onEnd;
      _imageQuestionData = imageQuestionData;

      AssignAnimations();
      SetVisualInformation(imageQuestionData);
      AssignListenersToButtons();

      StartCoroutine(StartBlinkingAnimation());
    }

    public void Open()
    {
      FieldText.localScale = Vector3.one * 0.4f;
      FieldTextCanvasGroup.alpha = 0;
      HintButtonCanvasGroup.alpha = 0;

      DOTween.Sequence()
        .Insert(0, FieldText.DOScale(Vector3.one, 0.5f))
        .Insert(0, FieldTextCanvasGroup.DOFade(1f, 0.5f))
        .Insert(0.5f, HintButtonCanvasGroup.DOFade(1f, 0.3f));
    }

    private IEnumerator StartBlinkingAnimation()
    {
      while (true)
      {
        var cooldown = UnityEngine.Random.Range(0.5f, 1.5f);
        yield return new WaitForSeconds(cooldown);

        var animate = NeonAnimations.CanvasGroupBlinkingRandom();
        var tween = animate(NeonSign.Container);

        yield return new DOTweenCYInstruction.WaitForCompletion(tween);
      }
    }

    public void UpdateNeonSign(bool isWin)
    {
      var targetSign = isWin ? NeonSign.IconTruth : NeonSign.IconFalse;

      var animate = NeonAnimations.ImageFadingRandom();

      DOTween.Kill(NeonSign.IconVinyl);

      var tween = DOTween.Sequence()
          .Insert(0.0f, HintButtonCanvasGroup.DOFade(0f, 0.2f))
          .Insert(0.2f, FieldText.DOScale(Vector3.zero, 0.5f))
          .Insert(0.2f, FieldTextCanvasGroup.DOFade(0f, 0.5f))
          .Append(animate(NeonSign.IconVinyl))
          .AppendCallback(Callback)
          .AppendInterval(1f)
          .OnComplete(() => _onEnd?.Invoke(isWin))
        ;

      void Callback()
      {
        NeonSign.IconVinyl.gameObject.SetActive(false);
        targetSign.gameObject.SetActive(true);
      }
    }

    public void ShowReward(Action onEnd)
    {
      if (_imageQuestionData.Reward == null)
      {
        onEnd?.Invoke();
        return;
      }

      CheckContent(_imageQuestionData.Reward, onEnd);
    }

    private void SetVisualInformation(PictureQuestionData imageQuestionData)
    {
      foreach (Button answerButton in AnswersButtons)
      {
        var text = answerButton.GetComponentInChildren<TextMeshProUGUI>();
        int index = AnswersButtons.IndexOf(answerButton);
        text.text = imageQuestionData.Variants[index];
      }

      Image.sprite = imageQuestionData.Picture;
    }

    private void AssignListenersToButtons()
    {
      foreach (Button answerButton in AnswersButtons)
      {
        answerButton.onClick.AddListener(() =>
        {
          int index = AnswersButtons.IndexOf(answerButton);
          Choose(_imageQuestionData.Variants[index]);
        });
      }

      BackButton.onClick.AddListener(() => _onBack?.Invoke());
      HintButton.onClick.AddListener(() => _onHint?.Invoke());
    }

    private void AssignAnimations()
    {
      foreach (var answerButton in AnswersButtons)
      {
        new ButtonAnimation(answerButton.transform);
      }

      new ButtonAnimation(HintButton.transform);
      new ButtonAnimation(BackButton.transform);
    }
  }
}