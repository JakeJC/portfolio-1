using _Project.Scripts.Animation;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using AppacheRemote.Scripts.Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono
{
  public class QuizHelpButtonsView : MonoBehaviour
  {
    public Button AssistantHelpButton;
    public Button FiftyFiftyButton;
    
    public TextMeshProUGUI AssistantHelpText;
    public TextMeshProUGUI FiftyFiftyText;
    
    public Image AssistantHelpAdsIcon;
    public Image FiftyFiftyAdsIcon;

    public void Construct(IRemote remote, ILocaleSystem localeSystem, IAudioService audioService,
      UnityAction onAssistantHelp, UnityAction onFiftyFifty)
    {
      AssistantHelpText.text = localeSystem.GetText("Main", "MiniGames.AssistantHelp");
      
      AssistantHelpButton.onClick.AddListener(onAssistantHelp);
      FiftyFiftyButton.onClick.AddListener(onFiftyFifty);

      new ButtonAnimation(AssistantHelpButton.transform);
      new ButtonAnimation(FiftyFiftyButton.transform);
      
      new ButtonSound(audioService, AssistantHelpButton.gameObject, AudioId.Karaoke_Button);
      new ButtonSound(audioService, FiftyFiftyButton.gameObject, AudioId.Karaoke_Button);

      if (remote.ShouldEnableFeatureTest())
      {
        AssistantHelpAdsIcon.gameObject.SetActive(false);
        FiftyFiftyAdsIcon.gameObject.SetActive(false);

        Vector2 anchoredPos;
        RectTransform rectTransform; 

        rectTransform = AssistantHelpText.GetComponent<RectTransform>();
        anchoredPos = rectTransform.anchoredPosition;
        anchoredPos.x = 0;
        rectTransform.anchoredPosition = anchoredPos;
        
        rectTransform = FiftyFiftyText.GetComponent<RectTransform>();
        anchoredPos = rectTransform.anchoredPosition;
        anchoredPos.x = 0;
        rectTransform.anchoredPosition = anchoredPos;
      }
    }
  }
}