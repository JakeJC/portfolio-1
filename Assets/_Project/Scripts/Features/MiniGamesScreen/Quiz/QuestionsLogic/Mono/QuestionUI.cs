using System;
using System.Collections.Generic;
using _Project.Scripts.Animation;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.GalleryScreen.Serialized;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.Money;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = System.Random;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono
{
  public class QuestionUI : MonoBehaviour
  {
    [Serializable]
    public class SkinData
    {
      public Sprite Icon;
      public Sprite Background;
    }
    
    private const float Duration = .3f;
    
    public Image Background;
    public Image Vinyl;
    public RectTransform ProgressBarPanel;
    public QuizHelpButtonsView QuizHelpButtons;
    public QuizHelpWindow QuizHelpWindow;
    public GameObject OpenEffect;
    public Image Vignette;
    public Image PhotoBackground;
    public Image StoryBackground;
    public CanvasGroup PhotoGroup;
    public CanvasGroup StoriesGroup;
    public Image HerPhoto;
    public SkeletonGraphic SkeletonAnimation;
    public CanvasGroup CloseRewardButton;
    public List<Button> AnswersButtons;
    
    public SkinData[] SkinDatas;
    
    private Action<string> _onSavePhoto;
    private Action<string> _onChoose;
    private QuizProgressBar _quizProgressBar;

    private IMoneyCounterLogic _moneyCounterLogic;
    private ILocaleSystem _localeSystem;
    
    private Func<int> _progressStep;
    private Func<int> _level;

    protected void BaseConstruct(IGameSaveSystem gameSaveSystem, IAudioService _audioService, ILocaleSystem localeSystem, IRemote remote,
      QuizProgressBar quizProgressBar, Action<string> onSavePhoto, Action<string> onChoose, 
      Func<int> level,Func<int> progressStep, UnityAction onAssistantHelp, UnityAction onFiftyFifty)
    {
      _level = level;
      _progressStep = progressStep;
      _localeSystem = localeSystem;
      _onSavePhoto = onSavePhoto;
      _quizProgressBar = quizProgressBar;
      _onChoose = onChoose;
      
      if (QuizHelpWindow)
      {
        QuizHelpWindow.Construct(localeSystem);
        QuizHelpWindow.gameObject.SetActive(false);
      }

      if (QuizHelpButtons) 
        QuizHelpButtons.Construct(remote, localeSystem, _audioService, onAssistantHelp, onFiftyFifty);

      if (_quizProgressBar)
      {
        _quizProgressBar.Construct(localeSystem, level, progressStep);

        if (gameSaveSystem.Get().QuizAssistantHelpIsUsed)
        {
          var buttonGrp = QuizHelpButtons.AssistantHelpButton.GetComponent<CanvasGroup>();
          buttonGrp.interactable = false;
          buttonGrp.alpha = 0;
        }

        if (gameSaveSystem.Get().FiftyFiftyHelpIsUsed)
        {
          var buttonGrp = QuizHelpButtons.FiftyFiftyButton.GetComponent<CanvasGroup>();
          buttonGrp.interactable = false;
          buttonGrp.alpha = 0;
        }
      }
      
      if (remote.ShouldEnableFeatureTest()) 
        PrepareSkin();

      CloseRewardButton.GetComponentInChildren<TMP_Text>().text = localeSystem.GetText("Main", "Next");
      
      void PrepareSkin()
      {
        int skinIndex = Mathf.Clamp(level.Invoke(), 0, SkinDatas.Length - 1);

        if (level.Invoke() > SkinDatas.Length - 1)
          skinIndex = level.Invoke() % 2 == 0 ? 0 : 1; // TODO: поменять

        SkinData skinData = SkinDatas[skinIndex];
        Vinyl.sprite = skinData.Icon;
        Background.sprite = skinData.Background;
      }
    }

    public void SetMoneyCounter(IMoneyCounterLogic moneyCounterLogic) => 
      _moneyCounterLogic = moneyCounterLogic;
    
    public void AddProgressBarStep() => 
      _quizProgressBar.AddProgressBarStep();

    public void ShowAssistantHelp(Button correctButton)
    {
      var assistantButtonGrp = QuizHelpButtons.AssistantHelpButton.GetComponent<CanvasGroup>();
      assistantButtonGrp.interactable = false;
      assistantButtonGrp.DOFade(0, 1);

      string correctAnswer = correctButton.GetComponentInChildren<TextMeshProUGUI>(true).text;

      string message = _progressStep.Invoke() - _level.Invoke() * QuizProgressBar.ProgressbarSteps == QuizProgressBar.ProgressbarSteps - 1
        ? _localeSystem.GetText("Main", "MiniGames.Help2")
        : _localeSystem.GetText("Main", "MiniGames.Help1");
      
      QuizHelpWindow.Open(string.Format(message, correctAnswer));
    }

    public void ShowFiftyFiftyHelp(Button correctButton)
    {
      List<Button> notCorrectButtons = new List<Button>(AnswersButtons);
      notCorrectButtons.Remove(correctButton);

      Button notCorrectButton1 = notCorrectButtons[UnityEngine.Random.Range(0, notCorrectButtons.Count - 1)];
      notCorrectButtons.Remove(notCorrectButton1);
      
      Button notCorrectButton2 = notCorrectButtons[UnityEngine.Random.Range(0, notCorrectButtons.Count - 1)];
      notCorrectButtons.Remove(notCorrectButton2);
      
      notCorrectButton1.gameObject.SetActive(false);
      notCorrectButton2.gameObject.SetActive(false);
      
      var fiftyFiftyButtonGrp = QuizHelpButtons.FiftyFiftyButton.GetComponent<CanvasGroup>();
      fiftyFiftyButtonGrp.interactable = false;
      fiftyFiftyButtonGrp.DOFade(0, 1);
    }

    public bool CheckContent(GalleryData outputValue, Action onEnd)
    {
      Debug.Log("CheckContent: " + ( outputValue == null));
      
      if (outputValue != null)
      {
        var group = outputValue.Type == GalleryType.Photo ? PhotoGroup : StoriesGroup;
        
        var transformContent = outputValue.Type == GalleryType.Photo
          ? HerPhoto.transform
          : SkeletonAnimation.transform;

        transformContent.DOScale(1f, Duration)
          .OnStart(() =>
          {
            _moneyCounterLogic.ShowMoneyCounter(false);
            ContentOpenEffect();

            transformContent.localScale = Vector3.zero;

            if (outputValue.Type == GalleryType.Photo)
            {
              HerPhoto.sprite = outputValue.GalleryLink.Image;

              PhotoBackground.sprite = outputValue.GalleryLink.InBlur;
              PhotoBackground.color = Color.white;
            }
            else
            {
              SkeletonAnimation.skeletonDataAsset = outputValue.GalleryLink.SkeletonData;
              SkeletonAnimation.Initialize(true);

              StoryBackground.sprite = outputValue.GalleryLink.InBlur;
              StoryBackground.color = Color.white;
            }
          })
          .SetEase(Ease.Linear);

        CloseRewardButton.GetComponent<Button>().onClick.AddListener(() =>
        {
          _moneyCounterLogic.ShowMoneyCounter(true);
          onEnd.Invoke();
        });

        CloseRewardButton.blocksRaycasts = true;

        group.DOFade(1f, Duration);
        CloseRewardButton.DOFade(1f, Duration);

        _onSavePhoto?.Invoke(outputValue.Id);

        return true;
      }

      if (PhotoGroup.alpha > 0)
        PhotoGroup.DOFade(0f, Duration);
      if (StoriesGroup.alpha > 0)
        StoriesGroup.DOFade(0f, Duration);

      return false;
    }
    
    protected void Choose(string answer) => 
      _onChoose?.Invoke(answer);
    
    protected void ContentOpenEffect()
    {
      OpenEffect.SetActive(true);
      
      Color defaultColor = Vignette.color;
      defaultColor.a = 0;
      Vignette.color = defaultColor;
      
      Vignette.DOFade(1, 1f).OnComplete(delegate
      {
        Vignette.DOFade(0, 1f).OnComplete(() => OpenEffect.SetActive(false));
      });
    }
  }
}