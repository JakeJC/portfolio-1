using System;
using _Project.Scripts.Services._Interfaces;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono
{
  public class QuizProgressBar : MonoBehaviour
  {
    public const int ProgressbarSteps = 3;
    
    private const float DurationFilling = 1.3f;
    private const float FillMax = 770f;

    public RectTransform BarFill;
    public TextMeshProUGUI LevelText;
    
    private Func<int> _progressStep;
    private Func<int> _level;

    private float ProgressStep => _progressStep.Invoke() - ProgressbarSteps * _level.Invoke();

    public void Construct(ILocaleSystem localeSystem, Func<int> level, Func<int> progressStep)
    {
      _level = level;
      LevelText.text = string.Format(localeSystem.GetText("Main", "Level"), (level.Invoke() + 1).ToString())
        .Replace(")", string.Empty).Replace("(", string.Empty);

      _progressStep = progressStep;
      UpdateProgressBar(ProgressStep,true);

      if (_level.Invoke() >= MiniGamesLogic.LevelsMax) 
        LevelText.gameObject.SetActive(false); 
    }

    public void AddProgressBarStep() => 
      UpdateProgressBar(ProgressStep + 1);

    private void UpdateProgressBar(float ratio, bool immediately = false)
    {
      ratio /= ProgressbarSteps;
      
      if (immediately)
      {
        BarFill.sizeDelta = new Vector2(BarFill.sizeDelta.x, ratio * FillMax);
        return;
      }

      float value = ratio * FillMax;

      DOTween.To(() => BarFill.sizeDelta.y, y
            => BarFill.sizeDelta = new Vector2(BarFill.sizeDelta.x, y),
          value, DurationFilling)
        .OnComplete(() => { Debug.Log("OnComplete"); });
    }
  }
}