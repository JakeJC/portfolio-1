using _Project.Scripts.Animation;
using _Project.Scripts.Services._Interfaces;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono
{
  public class QuizHelpWindow : MonoBehaviour
  {
    private const float Duration = 0.5f;
    
    public Button ThanksButton;
    public TextMeshProUGUI ThanksText;
    public TextMeshProUGUI MessageText;
    public CanvasGroup WindowGrp;

    public void Construct(ILocaleSystem localeSystem)
    {
      ThanksText.text = localeSystem.GetText("Main", "MiniGames.Thanks");
      ThanksButton.onClick.AddListener(Close);
      new ButtonAnimation(ThanksButton.transform);
    }

    public void Open(string message)
    {
      MessageText.text = message;
      
      ThanksButton.interactable = true;
      WindowGrp.alpha = 0;
      gameObject.SetActive(true);
      WindowGrp.DOFade(1, Duration);
    }

    private void Close()
    {
      ThanksButton.interactable = false;
      WindowGrp.alpha = 1;
      WindowGrp.DOFade(0, Duration).
        OnComplete(() => gameObject.SetActive(false));
    }
  }
}