using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsMediator;
using _Project.Scripts.Features.NotLoadedWindow;
using _Project.Scripts.Services._Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Abstract
{
  public abstract class BaseQuestionLogic
  {
    protected readonly IRemote _remote;
    protected readonly ILocaleSystem _localeSystem;
    protected readonly IAssetProvider _assetProvider;
    protected readonly IGameSaveSystem _gameSaveSystem;
    protected readonly IAdsService _adsService;
    protected readonly IAnalytics _analytics;
    private readonly IQuestionMediator _questionMediator;

    protected BaseQuestionLogic(IQuestionMediator questionMediator, IRemote remote, ILocaleSystem localeSystem, 
      IAssetProvider assetProvider, IGameSaveSystem gameSaveSystem, IAdsService adsService, IAnalytics analytics)
    {
      _questionMediator = questionMediator;
      _remote = remote;
      _localeSystem = localeSystem;
      _assetProvider = assetProvider;
      _gameSaveSystem = gameSaveSystem;
      _adsService = adsService;
      _analytics = analytics;
    }

    public abstract QuestionUI GetQuestionUI();
    
    protected abstract Button GetCorrectButton();
    
    protected abstract bool IsAnswerCorrect(string userAnswerId);
    
    protected abstract void AddProgressBarStep();

    protected void Next(bool isCorrect) => 
      _questionMediator.EndGame(isCorrect);

    protected void AssistantHelp()
    {
      if (_remote.ShouldEnableFeatureTest())
      {
        ApplyAssistantHelp(GetCorrectButton(), GetQuestionUI());
        return;
      }

      _adsService.Rewarded.ShowReward("hint", delegate { ApplyAssistantHelp(GetCorrectButton(), GetQuestionUI()); }, AdsNotLoaded);
      
      void ApplyAssistantHelp(Button correctButton, QuestionUI questionUI)
      {
        questionUI.ShowAssistantHelp(correctButton);

        var data = _gameSaveSystem.Get();
        data.QuizAssistantHelpIsUsed = true;
        _gameSaveSystem.Save();
      }
    }
    
    protected void FiftyFifty()
    {
      if (_remote.ShouldEnableFeatureTest())
      {
        ApplyFiftyFifty(GetCorrectButton(), GetQuestionUI());
        return;
      }
      
      _adsService.Rewarded.ShowReward("hint", delegate { ApplyFiftyFifty(GetCorrectButton(), GetQuestionUI()); }, AdsNotLoaded);
      
      void ApplyFiftyFifty(Button correctButton, QuestionUI questionUI)
      {
        questionUI.ShowFiftyFiftyHelp(correctButton);

        var data = _gameSaveSystem.Get();
        data.FiftyFiftyHelpIsUsed = true;
        _gameSaveSystem.Save();
      }
    }

    private void AdsNotLoaded()
    {
      INotLoadedWindowLogic notLoadedWindowLogic = new NotLoadedWindowLogic(_localeSystem, _assetProvider);
      notLoadedWindowLogic.Open();
    }
  }
}