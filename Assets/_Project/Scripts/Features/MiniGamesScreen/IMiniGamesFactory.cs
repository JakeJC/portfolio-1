using _Project.Scripts.Features.MiniGamesScreen.Mono;
using _Project.Scripts.Features.ScrollView;
using UnityEngine;

namespace _Project.Scripts.Features.MiniGamesScreen
{
  public interface IMiniGamesFactory
  {
    MiniGamesView CreateMiniGamesView();
    ScrollViewSectionButton CreateSectionButton(Transform parent);
    ScrollViewItemsView CreateItemsView(Transform parent);
    ScrollViewMiniGamesItemButton CreateItemButton(Transform parent);
    GameObject CreateComingSoon(Transform itemsViewContentParent);
  }
}