using _Project.Scripts.Features.MiniGamesScreen.Mono;
using _Project.Scripts.Features.ScrollView;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.MiniGamesScreen
{
  public class MiniGamesFactory : IMiniGamesFactory
  {
    private const string MiniGamesViewPath = "Features/MiniGamesScreen/MiniGames Window";

    private const string ItemsViewPath = "Features/Other/ScrollViewPrefabs/Scroll View Items View";
    private const string ItemButtonPath = "Features/Other/ScrollViewPrefabs/MiniGames/Scroll View MiniGames Item Button";
    private const string SectionButtonPath = "Features/Other/ScrollViewPrefabs/Scroll View Shop Section Button";

    private const string ItemMockButtonPath =
      "Features/Other/ScrollViewPrefabs/MiniGames/ Scroll View MiniGames Item Mock Button";
   
    private const string ComingSoonPath = "Features/Other/ScrollViewPrefabs/Coming Soon Game";

    private const string BaseCanvas = "Canvas";

    private readonly IAssetProvider _assetProvider;

    public MiniGamesFactory(IAssetProvider assetProvider)
    {
      _assetProvider = assetProvider;
    }

    public MiniGamesView CreateMiniGamesView()
    {
      Transform parent = GameObject.Find(BaseCanvas).transform;

      var homeScreen = _assetProvider.GetResource<MiniGamesView>(MiniGamesViewPath);
      return Object.Instantiate(homeScreen, parent);
    }

    public ScrollViewSectionButton CreateSectionButton(Transform parent)
    {
      var homeScreen = _assetProvider.GetResource<ScrollViewSectionButton>(SectionButtonPath);
      return Object.Instantiate(homeScreen, parent);
    }

    public ScrollViewItemsView CreateItemsView(Transform parent)
    {
      var homeScreen = _assetProvider.GetResource<ScrollViewItemsView>(ItemsViewPath);
      return Object.Instantiate(homeScreen, parent);
    }

    public ScrollViewMiniGamesItemButton CreateItemButton(Transform parent)
    {
      var homeScreen = _assetProvider.GetResource<ScrollViewMiniGamesItemButton>(ItemButtonPath);
      return Object.Instantiate(homeScreen, parent);
    }

    public GameObject CreateComingSoon(Transform parent)
    {
      var comingSoon = _assetProvider.GetResource<GameObject>(ComingSoonPath);
      return Object.Instantiate(comingSoon, parent);
    }
  }
}