using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Core_Logic.Screens;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Core_Logic.StatesMachine.States;
using _Project.Scripts.Features.BateWindow;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.Lose;
using _Project.Scripts.Features.MiniGamesScreen.Mono;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionData;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsMediator;
using _Project.Scripts.Features.MiniGamesScreen.Serialized;
using _Project.Scripts.Features.MiniGamesScreen.StripTaper;
using _Project.Scripts.Features.ScrollView;
using _Project.Scripts.Features.ScrollView.Serialized;
using _Project.Scripts.Features.ShopScreen.Content.Extra;
using _Project.Scripts.Features.StoriesScreen.Mono;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using _Project.Scripts.Services.Localization.Mono;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace _Project.Scripts.Features.MiniGamesScreen
{
  public class MiniGamesLogic
  {
    private const string GirlsDataPath = "Features/Data/GameGirlsData/Game Girls Data";
    public const int LevelsMax = 2; // TODO: пробросить

    private readonly IUiFactory _uiFactory;
    private readonly IMiniGamesFactory _miniGamesFactory;

    private readonly IRemote _remote;
    private readonly IAnalytics _analytics;
    private readonly IAdsService _adsService;
    private readonly IMoneyService _moneyService;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;
    private readonly ICurrentScreen _currentScreen;
    private readonly IAssetProvider _assetProvider;
    private readonly IUnlockService _unlockService;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IGameStateMachine _gameStateMachine;

    private readonly GameGirlsData _gameGirlsData;

    private readonly Dictionary<string, ScrollViewItemsView> _miniGamesItemsView = new Dictionary<string, ScrollViewItemsView>();
    private readonly Dictionary<string, ScrollViewItemButton[]> _miniGamesItemButtons = new Dictionary<string, ScrollViewItemButton[]>();

    private QuestionMediator _questionMediator;
    private StripTaperLogic _stripTaperLogic;

    private MiniGamesView _miniGamesView;

    private LoseLogic _loseLogic;
    private StripLoseLogic _stripLoseLogic;
    private BateWindowLogic _bateWindowLogic;

    private List<ScrollViewSectionButton> _sections = new List<ScrollViewSectionButton>();

    private readonly IExtraContentViewLogic _extraContentViewLogic;
    private BaseContentData _currentGirlData;

    public MiniGamesLogic(IGameStateMachine gameStateMachine, IAssetProvider assetProvider, ICurrentScreen currentScreen,
      IGameSaveSystem gameSaveSystem, IAdsService adsService, IAnalytics analytics, ILocaleSystem localeSystem,
      IAudioService audioService, IRemote remote, IUnlockService unlockService, IMoneyService moneyService)
    {
      _remote = remote;
      _analytics = analytics;
      _adsService = adsService;
      _localeSystem = localeSystem;
      _audioService = audioService;
      _currentScreen = currentScreen;
      _assetProvider = assetProvider;
      _unlockService = unlockService;
      _moneyService = moneyService;
      _gameSaveSystem = gameSaveSystem;
      _gameStateMachine = gameStateMachine;

      _uiFactory = new UiFactory(assetProvider, remote, localeSystem);
      _miniGamesFactory = new MiniGamesFactory(assetProvider);

      _gameGirlsData = assetProvider.GetResource<GameGirlsData>(GirlsDataPath);

      _currentScreen.SetCurrentScreen(ScreenNames.MiniGames);

      _extraContentViewLogic = new ExtraContentViewLogic();
      _extraContentViewLogic.Construct(_gameSaveSystem, assetProvider);
    }

    public void CreateMiniGamesView()
    {
      _miniGamesView = _miniGamesFactory.CreateMiniGamesView();
      _miniGamesView.Construct(_localeSystem, CloseMiniGamesView);

      CreateMiniGamesContent();

      _loseLogic = new LoseLogic(_assetProvider, _adsService, _localeSystem);
      _stripLoseLogic = new StripLoseLogic(_assetProvider, _adsService, _localeSystem);
      _bateWindowLogic = new BateWindowLogic(_analytics, _localeSystem, _assetProvider, _adsService, _moneyService, 100, ToMiniGamesView);

      CreateSounds(_miniGamesView);

      ExtraLookData extraLookData = _extraContentViewLogic.GetCurrentLook();
      if (extraLookData)
        _miniGamesView.SetGirlView(extraLookData.DefaultStatic);
    }

    public void ClearMiniGamesView()
    {
      _loseLogic.Clear();
      _stripLoseLogic.Clear();
      _bateWindowLogic.Clear();

      _sections.Clear();

      if (_miniGamesView)
        Object.Destroy(_miniGamesView.gameObject);
    }

    private void Select(int index)
    {
      for (int i = 0; i < _sections.Count; i++)
        _sections[i].Select(i == index);
    }

    private void CreateMiniGamesContent()
    {
      foreach (GirlData t in _gameGirlsData.GirlDatas)
      {
        ScrollViewItemsView itemsView = _miniGamesFactory.CreateItemsView(_miniGamesView.ItemViewsParent);
        _miniGamesItemsView.Add(t.Name, itemsView);

        ScrollViewSectionButton shopButton = _miniGamesFactory.CreateSectionButton(_miniGamesView.SectionsParent);
        _sections.Add(shopButton);

        shopButton.Construct(_localeSystem, t.Name, () =>
        {
          int index = _sections.IndexOf(shopButton);
          Select(index);
          OnClickGirl(t);
        }, t.Icon);

        var miniGameItemButton = new List<ScrollViewItemButton>();
        foreach (MiniGameData item in t.MiniGameDatas)
        {
          if (item.Type == MiniGameType.SoonMock)
          {
            GameObject comingSoon = _miniGamesFactory.CreateComingSoon(itemsView.ContentParent);
            comingSoon.GetComponentInChildren<LocalizedMainText>(true).Construct(_localeSystem);
          }
          else
          {
            ScrollViewMiniGamesItemButton scrollViewItemButton = _miniGamesFactory.CreateItemButton(itemsView.ContentParent);

            scrollViewItemButton.Construct(_localeSystem, _unlockService, new ScrollViewItemData() { Id = item.Id, NameKey = item.Name, DescriptionKey = item.Description, Icon = item.Icon },
              () => OnClickGame(item), _audioService, string.Empty, _remote);

            scrollViewItemButton.SwitchFontColor(item.UseLightFontColor);

            if (item.isShowingBar)
              scrollViewItemButton.SetProgress(GetProgressPercent(item));

            miniGameItemButton.Add(scrollViewItemButton);

            var counterView = scrollViewItemButton.GetComponent<PicturesCounterView>();
            counterView.Construct(_localeSystem, true);
            if (counterView)
              counterView.SetOpenCount(GetUnlockedPicturesCount(item), GetMaxPicturesCount(item));
          }
        }

        _miniGamesItemButtons.Add(t.Name, miniGameItemButton.ToArray());
      }

      Select(0);
      OnClickGirl(_gameGirlsData.GirlDatas[0]);

      int GetUnlockedPicturesCount(MiniGameData data)
      {
        int unlockedCount = 0;

        foreach (var level in data.Levels) /**/
        {
          if (level.GetType() == typeof(TextQuestionData))
          {
            var rewardedLevel = level as TextQuestionData;

            if (rewardedLevel == null || rewardedLevel.Reward == null)
              continue;

            if (_gameSaveSystem.Get().UnlockedPhotoContent.Contains(rewardedLevel.Reward.Id) ||
                _gameSaveSystem.Get().UnlockedStoriesContent.Contains(rewardedLevel.Reward.Id))
              unlockedCount++;
          }
          else if (level.GetType() == typeof(GalleryData))
          {
            var rewardedLevel = level as GalleryData;

            if (rewardedLevel == null)
              continue;

            if (_gameSaveSystem.Get().UnlockedPhotoContent.Contains(rewardedLevel.Id) ||
                _gameSaveSystem.Get().UnlockedStoriesContent.Contains(rewardedLevel.Id))
              unlockedCount++;
          }
        }

        return unlockedCount;
      }

      int GetMaxPicturesCount(MiniGameData data)
      {
        int count = 0;

        foreach (var level in data.Levels)
        {
          if (level.GetType() == typeof(TextQuestionData))
          {
            var rewardedLevel = level as TextQuestionData;

            if (rewardedLevel != null && rewardedLevel.Reward != null)
              count++;
          }
          else if (level.GetType() == typeof(GalleryData))
          {
            return data.Levels.Count;
          }
        }

        return count;
      }
    }

    private void OnClickGirl(BaseContentData item)
    {
      _currentGirlData = item;

      foreach (KeyValuePair<string, ScrollViewItemsView> itemsView in GetDeactivatedItemsViews(item.Id))
        itemsView.Value.Deactivate();

      _miniGamesItemsView[item.Name].Activate();
    }

    private void OnReenterGame(MiniGameData item) =>
      LaunchMiniGame(item);

    private void OnClickGame(MiniGameData item) =>
      LaunchMiniGame(item);

    private void LaunchMiniGame(MiniGameData item)
    {
      int index = _gameSaveSystem.Get().OnLoadMiniGameProgress(item.Type);
      BaseContentData playableLevel;

      switch (item.Type)
      {
        case MiniGameType.Quiz:
          index = _gameSaveSystem.Get().OnLoadMiniGameProgress(MiniGameType.QuizProgress);

          _questionMediator = new QuestionMediator(_analytics, _assetProvider, _gameSaveSystem, _localeSystem, _loseLogic, _bateWindowLogic, item, _audioService, _adsService,
            _moneyService, ToMiniGamesView, _remote, delegate { OnReenterGame(item); }, _unlockService);
          playableLevel = index < item.Levels.Count ? item.Levels[index] : item.Levels[Random.Range(0, item.Levels.Count)];
          _questionMediator.LaunchGame(playableLevel, _currentGirlData);

          break;
        case MiniGameType.StripTaperLevels:
          _stripTaperLogic = new StripTaperLogic(_analytics, _assetProvider, _gameSaveSystem, _localeSystem, _stripLoseLogic, _bateWindowLogic, item, _audioService, _adsService,
            _moneyService, ToMiniGamesView, _remote, delegate { OnReenterGame(item); }, _unlockService);
          playableLevel = index < item.Levels.Count ? item.Levels[index] : null;
          _stripTaperLogic.LaunchGame(playableLevel, _currentGirlData);

          break;
        default:
          throw new System.ArgumentOutOfRangeException();
      }

      GirlData GetGirlDataByMiniGame(MiniGameData storyData) =>
        _gameGirlsData.GirlDatas.FirstOrDefault(p => p.MiniGameDatas.Contains(storyData));
    }

    private void CloseMiniGamesView()
    {
      if (_remote.ShouldEnableFeatureTest())
      {
        _gameStateMachine.Enter<LoadHomeState>();
        return;
      }
      
      _adsService.CurrentInterstitial.ShowInterstitial(ScreenNames.Home,
        _gameStateMachine.Enter<LoadHomeState>,
        _gameStateMachine.Enter<LoadHomeState>);
    }

    private void ToMiniGamesView() =>
      _gameStateMachine.Enter<LoadMiniGamesState>();

    private void CreateSounds(MiniGamesView miniGamesView)
    {
      new ButtonSound(_audioService, miniGamesView.BackButton.gameObject, AudioId.Karaoke_Button);
    }

    private float GetProgressPercent(MiniGameData miniGameData)
    {
      int index = _gameSaveSystem.Get().OnLoadMiniGameProgress(miniGameData.Type);
      int capacity = miniGameData.Levels.Count;

      return ((float)index / capacity) * 100;
    }

    private IEnumerable<KeyValuePair<string, ScrollViewItemsView>> GetDeactivatedItemsViews(string id) =>
      _miniGamesItemsView.Where(item => item.Key != id);
  }
}