using System;
using System.Collections.Generic;
using _Project.Scripts.Features.BateWindow;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.Lose;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionData;
using _Project.Scripts.Features.MiniGamesScreen.Quiz.QuestionsLogic.Mono;
using _Project.Scripts.Features.MiniGamesScreen.Serialized;
using _Project.Scripts.Features.MiniGamesScreen.StripTaper.Factories;
using _Project.Scripts.Features.MiniGamesScreen.StripTaper.Interfaces;
using _Project.Scripts.Features.MiniGamesScreen.StripTaper.Mono;
using _Project.Scripts.Features.ShopScreen.Content.Extra;
using _Project.Scripts.Services._data;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Money;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.MiniGamesScreen.StripTaper
{
  public class StripTaperLogic
  {
    private const int MaxLives = 3;
    private const int SuperWinRewardMultiplier = 2;

    private readonly IAssetProvider _assetProvider;
    private readonly ILocaleSystem _localeSystem;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IMoneyService _moneyService;
    private readonly IAdsService _adsService;
    private readonly IUiFactory _uiFactory;
    private readonly IAudioService _audioService;
    private readonly IAnalytics _analytics;
    private readonly IRemote _remote;
    private readonly IStripTaperFactory _stripTaperFactory;

    private readonly StripLoseLogic _loseLogic;
    private readonly BateWindowLogic _bateWindowLogic;
    private readonly MiniGameData _miniGameData;

    private BaseContentData _currentGirlData;
    private MoneyCounterLogic _moneyCounterLogic;
    private GalleryData _galleryData;
    private StripTaperView _stripTaperView;

    private readonly Action _onUpdateMiniGamesState;
    private readonly Action _onRenter;
    private readonly IUnlockService _unlockService;

    private int _lives = 3;

    private int StripLaunchCount
    {
      get => PlayerPrefs.GetInt("StripLaunchCount", 0);
      set => PlayerPrefs.SetInt("StripLaunchCount", value);
    }

    public StripTaperLogic(IAnalytics analytics, IAssetProvider assetProvider, IGameSaveSystem gameSaveSystem, ILocaleSystem localeSystem,
      StripLoseLogic loseLogic, BateWindowLogic bateWindowLogic, MiniGameData miniGameData, IAudioService audioService,
      IAdsService adsService, IMoneyService moneyService, Action onUpdateMiniGamesState, IRemote remote, Action onRenter, IUnlockService unlockService)
    {
      _miniGameData = miniGameData;
      _analytics = analytics;
      _onUpdateMiniGamesState = onUpdateMiniGamesState;
      _remote = remote;
      _onRenter = onRenter;
      _unlockService = unlockService;

      _assetProvider = assetProvider;
      _loseLogic = loseLogic;
      _audioService = audioService;
      _localeSystem = localeSystem;
      _gameSaveSystem = gameSaveSystem;
      _bateWindowLogic = bateWindowLogic;
      _adsService = adsService;
      _moneyService = moneyService;

      _uiFactory = new UiFactory(assetProvider, remote, localeSystem);

      _stripTaperFactory = new StripTaperFactory(assetProvider, remote);
    }

    public void LaunchGame<TData>(TData data, BaseContentData currentGirlData) where TData : BaseContentData
    {
      _currentGirlData = currentGirlData;
      _galleryData = data as GalleryData;

      _lives = 3;
      CreateGameUI();

      StripLaunchCount++;

      _analytics.Send("ev_game_start", new Dictionary<string, object>
      {
        { "game_id", _miniGameData.Id },
        { "character_id", _currentGirlData.Id },
        { "try_number", StripLaunchCount },
        { "game_lvl ", LoadLevelProgress() + 1 }
      });
    }

    private void CreateGameUI()
    {
      var extraContentViewLogic = new ExtraContentViewLogic();
      extraContentViewLogic.Construct(_gameSaveSystem, _assetProvider);

      _stripTaperView = _stripTaperFactory.CreateStripTaperView();
      _moneyCounterLogic = new MoneyCounterLogic(_assetProvider, _uiFactory, _localeSystem, _moneyService, _audioService, _adsService, _remote, _unlockService, _gameSaveSystem, _analytics);
      _stripTaperView.Construct(_moneyCounterLogic, extraContentViewLogic, _localeSystem, Clear, EndGame, OnSavePhoto, LoadBarProgress(), LoadLevelProgress(), _remote);

      if (LoadLevelProgress() == 0 && LoadBarProgress() == 0)
        _stripTaperView.ShowSpeakBubble(_localeSystem.GetText("Main", "MiniGames.5Times"),
          _stripTaperView.HideSpeakBubble);

      if (LoadBarProgress() == 15 && _gameSaveSystem.Get().GreatBubbleWasActivated == false)
      {
        _gameSaveSystem.Get().GreatBubbleWasActivated = true;
        _gameSaveSystem.Save();

        _stripTaperView.ShowSpeakBubble(_localeSystem.GetText("Main", "MiniGames.Great"),
          _stripTaperView.HideSpeakBubble);
      }
    }

    private void EndGame(bool isWin, int filling)
    {
      if (isWin)
      {
        SaveBarProgress(_gameSaveSystem.Get().OnLoadMiniGameProgress(MiniGameType.StripTaper) + 1);

        if (filling % 25 != 0)
          return;

        if (filling >= 100)
        {
          SuperWin();
          return;
        }

        Clear();
        Win();
      }
      else
      {
        if (_lives == MaxLives && _gameSaveSystem.Get().MissClickBubbleWasActivated == false)
        {
          _gameSaveSystem.Get().MissClickBubbleWasActivated = true;
          _gameSaveSystem.Save();

          _stripTaperView.ShowSpeakBubble(_localeSystem.GetText("Main", "MiniGames.Oh"),
            _stripTaperView.HideSpeakBubble);
        }

        --_lives;
        _stripTaperView.UpdateHearts(_lives);
        if (_lives > 0)
          return;

        Clear();
        Lose();
      }
    }

    private void OnSavePhoto(string id)
    {
      _gameSaveSystem.Get().UnlockedPhotoContent.Add(id);
      _gameSaveSystem.Save();
    }

    private void Clear()
    {
      if (_stripTaperView)
        Object.Destroy(_stripTaperView.gameObject);
      _moneyCounterLogic.Clear();

      _onUpdateMiniGamesState?.Invoke();
    }

    private void Win()
    {
      Debug.Log("Win");
      _audioService.Play(AudioId.Karaoke_Win);

      Action onCloseBate = delegate { _onRenter.Invoke(); };

      _bateWindowLogic.Open(_currentGirlData as GirlData, _miniGameData, onCloseBate, true);

      _analytics.Send("ev_game_finish", new Dictionary<string, object>
      {
        { "game_id", _miniGameData.Id },
        { "character_id", _currentGirlData.Id },
        { "result", "win" },
        { "try_number", StripLaunchCount }
      });
    }

    private void SuperWin()
    {
      Debug.Log("Super Win");

      SaveBarProgress(0);
      SaveLevelProgress();

      _audioService.Play(AudioId.Karaoke_Win);

      _analytics.Send("ev_game_finish", new Dictionary<string, object>
      {
        { "game_id", _miniGameData.Id },
        { "character_id", _currentGirlData.Id },
        { "result", "win" },
        { "try_number", StripLaunchCount }
      });

      if (_stripTaperView.CheckContent(_galleryData, delegate
      {
        Clear();

        if (LoadLevelProgress() < MiniGamesLogic.LevelsMax)
          _onRenter.Invoke();
      }))
        return;

      _stripTaperView.CheckContent(_galleryData, Clear);
      _bateWindowLogic.Open(_currentGirlData as GirlData, _miniGameData, null, true, SuperWinRewardMultiplier);
    }

    private void SaveLevelProgress()
    {
      int index = _gameSaveSystem.Get().OnLoadMiniGameProgress(MiniGameType.StripTaperLevels);
      index++;
      _gameSaveSystem.Get().OnSaveMiniGameProgress(MiniGameType.StripTaperLevels, index);
      _gameSaveSystem.Save();
    }

    private int LoadLevelProgress() =>
      _gameSaveSystem.Get().OnLoadMiniGameProgress(MiniGameType.StripTaperLevels);

    private void SaveBarProgress(int newIndex)
    {
      _gameSaveSystem.Get().OnSaveMiniGameProgress(MiniGameType.StripTaper, newIndex);
      _gameSaveSystem.Save();
    }

    private int LoadBarProgress() =>
      _gameSaveSystem.Get().OnLoadMiniGameProgress(MiniGameType.StripTaper);

    private void DowngradeBarProgressToLastGift()
    {
      int index = _gameSaveSystem.Get().OnLoadMiniGameProgress(MiniGameType.StripTaper);
      index -= index % StripTaperView.ProgressbarStepsAmount;
      _gameSaveSystem.Get().OnSaveMiniGameProgress(MiniGameType.StripTaper, index);
      _gameSaveSystem.Save();
    }

    private void Lose()
    {
      Action onLose = DowngradeBarProgressToLastGift;

      _audioService.Play(AudioId.Karaoke_Lose);
      _loseLogic.CreateLoseScreen(() => LaunchGame(_galleryData, _currentGirlData), onLose);

      _analytics.Send("ev_game_finish", new Dictionary<string, object>
      {
        { "game_id", _miniGameData.Id },
        { "character_id", _currentGirlData.Id },
        { "result", "loss" },
        { "try_number", StripLaunchCount }
      });
    }
  }
}