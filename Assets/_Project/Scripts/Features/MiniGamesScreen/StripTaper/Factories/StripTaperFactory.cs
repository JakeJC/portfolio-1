using _Project.Scripts.Features.MiniGamesScreen.StripTaper.Interfaces;
using _Project.Scripts.Features.MiniGamesScreen.StripTaper.Mono;
using _Project.Scripts.Services._Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.MiniGamesScreen.StripTaper.Factories
{
  public class StripTaperFactory : IStripTaperFactory
  {
    private const string UIPictureQuestionPath = "Features/MiniGamesScreen/StripTaper/StripTaper";
    private const string ParentPath = "Canvas";

    private readonly IAssetProvider _assetProvider;
    private readonly IRemote _remote;

    public StripTaperFactory(IAssetProvider assetProvider, IRemote remote)
    {
      _assetProvider = assetProvider;
      _remote = remote;
    }

    public StripTaperView CreateStripTaperView()
    {
      Transform parent = GameObject.Find(ParentPath).transform;

      var prefab = _assetProvider.GetResource<StripTaperView>(UIPictureQuestionPath);
      return Object.Instantiate(prefab, parent);
    }
  }
}