using _Project.Scripts.Features.MiniGamesScreen.StripTaper.Mono;
using UnityEngine;

namespace _Project.Scripts.Features.MiniGamesScreen.StripTaper.Interfaces
{
  public interface IStripTaperFactory
  {
    StripTaperView CreateStripTaperView();
  }
}