﻿using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.MiniGamesScreen.StripTaper.Mono
{
    public class StripNeonSigns : MonoBehaviour
    {
        [SerializeField] private Image heartOff1;
        [SerializeField] private Image heartOff2;
        [SerializeField] private Image heartOff3;
        [SerializeField] private Image heartOn1;
        [SerializeField] private Image heartOn2;
        [SerializeField] private Image heartOn3;

        public Image HeartOff1 => heartOff1;
        public Image HeartOff2 => heartOff2;
        public Image HeartOff3 => heartOff3;
        public Image HeartOn1 => heartOn1;
        public Image HeartOn2 => heartOn2;
        public Image HeartOn3 => heartOn3;
    }
}