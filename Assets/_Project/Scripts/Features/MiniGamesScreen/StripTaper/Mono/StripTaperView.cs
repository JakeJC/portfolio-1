using System;
using System.Collections;
using _Project.Scripts.Animation;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.GalleryScreen.Serialized;
using _Project.Scripts.Features.ShopScreen.Content.Extra;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Money;
using AppacheRemote;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.MiniGamesScreen.StripTaper.Mono
{
  public class StripTaperView : MonoBehaviour
  {
    [Serializable]
    public class SkinData
    {
      public Sprite HeartOn;
      public Sprite HeartOff;
      public Sprite Background;
    }

#if TEST_DIALOG
    private const float RewardAnimationDuration = .3f;
#else
    private const float RewardAnimationDuration = .3f;
#endif

    public const int ProgressbarStepsAmount = 5;
    private float CenterX => SlideBar.anchoredPosition.x;
    private float CenterY => SlideBar.anchoredPosition.y - 249.2888f;
    private const float Radius = 294.4525f;
    private const float AngleL = 136.1230f;
    private const float AngleR = 43.8770f;
    private const float Duration = 2f;
    private const float DurationFilling = .3f;
    private const float Offset = 37;

    public Button TapButton;
    public Button BackButton;
    public RectTransform BarFill;
    public RectTransform BarFillArea;
    public RectTransform Sector1;
    public RectTransform Sector2;
    public RectTransform SlideBar;
    public RectTransform Slider;
    public StripNeonSigns NeonSigns;
    public TMP_Text TapMeText;
    public Image GirlImage;

    public CanvasGroup PhotoGroup;
    public CanvasGroup StoriesGroup;

    public ParticleSystem CorrectEffect;
    public RectTransform CorrectEffectTransform;

    public Image HerPhoto;
    public SkeletonGraphic SkeletonAnimation;

    public CanvasGroup CloseRewardButton;

    public Image PhotoBackground;
    public Image StoryBackground;

    public GameObject OpenEffect;
    public Image Vignette;

    public GameObject SpeakBubble;
    public TextMeshProUGUI SpeakBubbleText;
    public CurvedSlidebar CurvedSlidebar;
    public TextMeshProUGUI LevelTitle;
    public Image[] HeartsOn;
    public Image[] HeartsOff;
    public Image Background;
    public SkinData[] SkinDatas;

    private Action _onBack;
    private Action<bool, int> _onEndGame;
    private Action<string> _onSavePhoto;

    private ILocaleSystem _localeSystem;
    private IExtraContentViewLogic _extraContentViewLogic;
    private IMoneyCounterLogic _moneyCounterLogic;
    private IRemote _remote;

    private int _barFillRatio;
    private Sequence _speakBubbleSequence;

    public void Construct(IMoneyCounterLogic moneyCounterLogic, IExtraContentViewLogic extraContentViewLogic, ILocaleSystem localeSystem,
      Action onBack, Action<bool, int> onEndGame, Action<string> onSavePhoto, int barLevel, int level, IRemote remote)
    {
      _remote = remote;
      _moneyCounterLogic = moneyCounterLogic;
      _extraContentViewLogic = extraContentViewLogic;

      _onBack = onBack;
      _onEndGame = onEndGame;
      _localeSystem = localeSystem;
      _onSavePhoto = onSavePhoto;

      Localize();

      if (level > SkinDatas.Length - 1)
        LevelTitle.gameObject.SetActive(false);
      LevelTitle.text = string.Format(localeSystem.GetText("Main", "Level"), (level + 1).ToString())
        .Replace(")", string.Empty).Replace("(", string.Empty);

      SpeakBubble.SetActive(false);

      Slider.GetComponent<Image>().enabled = false;
      Sector1.GetComponent<Image>().enabled = false;
      Sector2.GetComponent<Image>().enabled = false;
      SlideBar.GetComponent<Image>().enabled = false;

      PrepareSkin();

      SetTargetPosition();
      StartSliderMoving();

      TapButton.onClick.AddListener(CheckOnTap);
      BackButton.onClick.AddListener(() => _onBack?.Invoke());

      SetBarFill(ProgressbarStepsAmount * barLevel);

      StartCoroutine(StartBlinkingAnimation());

      SetGirlSprite(level);

      CorrectEffect.gameObject.SetActive(false);

      void PrepareSkin()
      {
        int skinIndex = Mathf.Clamp(level, 0, SkinDatas.Length - 1);

        if (level > SkinDatas.Length - 1)
          skinIndex = level % 2 == 0 ? 0 : 1; // TODO: поменять

        CurvedSlidebar.SetActiveCurve(skinIndex);
        SkinData skinData = SkinDatas[skinIndex];
        foreach (var image in HeartsOn) image.sprite = skinData.HeartOn;
        foreach (var image in HeartsOff) image.sprite = skinData.HeartOff;
        Background.sprite = skinData.Background;
      }
    }

    public void UpdateHearts(int livesLeft)
    {
      Debug.Assert(livesLeft <= 3);

      var targetHeart = livesLeft switch
      {
        2 => NeonSigns.HeartOn1,
        1 => NeonSigns.HeartOn2,
        0 => NeonSigns.HeartOn3,
        _ => null,
      };

      var animate = NeonAnimations.ImageFadingRandom();

      DOTween.Kill(targetHeart);
      animate(targetHeart)
        .OnComplete(() => targetHeart.enabled = false);
    }

    public void ShowSpeakBubble(string message, Action onComplete = null, float delay = 5)
    {
      SpeakBubbleText.text = message;

      SpeakBubble.SetActive(true);

      CanvasGroup speakBubbleCanvasGroup = SpeakBubble.GetComponent<CanvasGroup>();
      CanvasGroup speakBubbleTextCanvasGroup = SpeakBubbleText.GetComponent<CanvasGroup>();

      SpeakBubble.transform.localScale = Vector3.one * 0.2f;
      speakBubbleCanvasGroup.alpha = 0;
      speakBubbleTextCanvasGroup.alpha = 0;

      _speakBubbleSequence?.Kill();
      _speakBubbleSequence = DOTween.Sequence();
      _speakBubbleSequence
        .Insert(0, SpeakBubble.transform.DOScale(Vector3.one * 0.6f, 0.5f))
        .Insert(0, speakBubbleCanvasGroup.DOFade(1f, 0.5f))
        .Insert(0.5f, speakBubbleTextCanvasGroup.DOFade(1f, 0.3f))
        .AppendInterval(delay)
        .onComplete = delegate { onComplete?.Invoke(); };
    }

    public void HideSpeakBubble()
    {
      SpeakBubble.SetActive(true);

      CanvasGroup speakBubbleCanvasGroup = SpeakBubble.GetComponent<CanvasGroup>();
      CanvasGroup speakBubbleTextCanvasGroup = SpeakBubbleText.GetComponent<CanvasGroup>();

      SpeakBubble.transform.localScale = Vector3.one * 0.6f;
      speakBubbleCanvasGroup.alpha = 1;
      speakBubbleTextCanvasGroup.alpha = 1;

      _speakBubbleSequence?.Kill();
      _speakBubbleSequence = DOTween.Sequence();
      _speakBubbleSequence
        .Insert(0, SpeakBubble.transform.DOScale(Vector3.one * 0.2f, 0.5f))
        .Insert(0, speakBubbleCanvasGroup.DOFade(0, 0.5f))
        .Insert(0.5f, speakBubbleTextCanvasGroup.DOFade(0, 0.3f))
        .onComplete = delegate { SpeakBubble.SetActive(false); };
    }

    public bool CheckContent(GalleryData outputValue, Action onEnd)
    {
      if (outputValue != null)
      {
        var group = outputValue.Type == GalleryType.Photo ? PhotoGroup : StoriesGroup;
        var transformContent = outputValue.Type == GalleryType.Photo
          ? HerPhoto.transform
          : SkeletonAnimation.transform;

        transformContent.DOScale(1f, Duration)
          .OnStart(() =>
          {
            _moneyCounterLogic.ShowMoneyCounter(false);
            ContentOpenEffect();

            transformContent.localScale = Vector3.zero;

            if (outputValue.Type == GalleryType.Photo)
            {
              HerPhoto.sprite = outputValue.GalleryLink.Image;

              PhotoBackground.sprite = outputValue.GalleryLink.InBlur;
              PhotoBackground.color = Color.white;
            }
            else
            {
              SkeletonAnimation.skeletonDataAsset = outputValue.GalleryLink.SkeletonData;
              SkeletonAnimation.Initialize(true);

              StoryBackground.sprite = outputValue.GalleryLink.InBlur;
              StoryBackground.color = Color.white;
            }
          })
          .SetEase(Ease.Linear);

        CloseRewardButton.GetComponent<Button>().onClick.AddListener(() =>
        {
          _moneyCounterLogic.ShowMoneyCounter(true);
          onEnd.Invoke();
        });

        CloseRewardButton.blocksRaycasts = true;

        group.DOFade(1f, Duration);
        CloseRewardButton.DOFade(1f, Duration);

        _onSavePhoto?.Invoke(outputValue.Id);

        return true;
      }

      if (PhotoGroup.alpha > 0)
        PhotoGroup.DOFade(0f, Duration);
      if (StoriesGroup.alpha > 0)
        StoriesGroup.DOFade(0f, Duration);

      return false;
    }

    private void SetGirlSprite(int level)
    {
      bool useDefaultStatic = level % 2 == 0;

      GirlImage.sprite = useDefaultStatic
        ? _extraContentViewLogic.GetCurrentLook().DefaultStatic
        : _extraContentViewLogic.GetCurrentLook().SpeakStatic;
    }

    private IEnumerator StartBlinkingAnimation()
    {
      while (true)
      {
        var cooldown = UnityEngine.Random.Range(0.5f, 1.5f);
        yield return new WaitForSeconds(cooldown);

        var animate = NeonAnimations.ImageBlinkingRandom();
        var tween = DOTween.Sequence()
            .Insert(0, animate(NeonSigns.HeartOn1))
            .Insert(0, animate(NeonSigns.HeartOn2))
            .Insert(0, animate(NeonSigns.HeartOn3))
          ;

        yield return new DOTweenCYInstruction.WaitForCompletion(tween);
      }
    }

    private void ContentOpenEffect()
    {
      OpenEffect.SetActive(true);

      Color defaultColor = Vignette.color;
      defaultColor.a = 0;
      Vignette.color = defaultColor;

      Vignette.DOFade(1, 1f).OnComplete(delegate { Vignette.DOFade(0, 1f).OnComplete(() => OpenEffect.SetActive(false)); });
    }

    private int GetBarFill()
    {
      return _barFillRatio;
    }

    private void SetBarFill(int fillRatio)
    {
      _barFillRatio = fillRatio;
      BarFill.offsetMax = BarFillArea.offsetMax - Vector2.up * (BarFillArea.rect.height * (100 - fillRatio) / 100);
    }

    private void CheckOnTap()
    {
      Slider.DOPause();

      if (InRange())
      {
        PlayEffect();

        DOTween.To(GetBarFill, SetBarFill, _barFillRatio + ProgressbarStepsAmount, DurationFilling)
          .SetEase(Ease.OutExpo)
          .OnComplete(() => { ContinueGame(true); });
        return;
      }

      ContinueGame(false);
    }

    private void PlayEffect()
    {
      CorrectEffect = Instantiate(CorrectEffect, CorrectEffectTransform.parent);
      CorrectEffectTransform = CorrectEffect.GetComponent<RectTransform>();
      CorrectEffectTransform.anchoredPosition = Sector1.anchoredPosition;
      CorrectEffect.gameObject.SetActive(true);

      CurvedSlidebar.SetEffectPosition(CorrectEffectTransform);
    }

    private void ContinueGame(bool inRange)
    {
      _onEndGame?.Invoke(inRange, _barFillRatio);
      SetTargetPosition();
      Slider.DOPlay();
    }

    private void StartSliderMoving()
    {
      var angleDeg = AngleL;

      float Getter()
      {
        return angleDeg;
      }

      void Setter(float angle)
      {
        angleDeg = angle;
        Slider.anchoredPosition = GetAnchoredPosition(angleDeg);

        CurvedSlidebar.SetSliderPosition(angleDeg, AngleL, AngleR);
      }

      DOTween.To(Getter, Setter, AngleR, Duration)
        .SetEase(Ease.Linear)
        .SetLoops(-1, LoopType.Yoyo);
    }

    private Vector2 GetAnchoredPosition(float angleDeg)
    {
      var angleRad = angleDeg * Mathf.Deg2Rad;
      var x = CenterX + Mathf.Cos(angleRad) * Radius;
      var y = CenterY + Mathf.Sin(angleRad) * Radius;

      return new Vector2(x, y);
    }

    private void SetTargetPosition()
    {
      var angleDeg = UnityEngine.Random.Range(AngleL, AngleR);
      Vector2 target = GetAnchoredPosition(angleDeg);
      Sector1.anchoredPosition = target;
      Sector2.anchoredPosition = target;

      CurvedSlidebar.SetSectorsPosition(angleDeg, AngleL, AngleR);
    }

    private bool InRange()
    {
      var linePoint = Slider.anchoredPosition;
      var targetPoint = Sector1.anchoredPosition;
      var distance = Vector2.Distance(linePoint, targetPoint);
      return distance <= Offset;
    }

    private void Localize()
    {
      CloseRewardButton.GetComponentInChildren<TMP_Text>().text = _localeSystem.GetText("Main", "Next");
      TapMeText.text = _localeSystem.GetText("Main", "TapOnMe");
    }
  }
}