using UnityEngine;

namespace _Project.Scripts.Features.MiniGamesScreen.StripTaper.Mono
{
  public class CurvedSlidebar : MonoBehaviour
  {
    [System.Serializable]
    public class Data
    {
      public RectTransform Background;
      public BezierCurve BezierCurve;

      public void SetActive(bool value)
      {
        Background.gameObject.SetActive(value);
        BezierCurve.gameObject.SetActive(value);
      }
    }
    
    public RectTransform Slider;
    public RectTransform Sector1Curve;
    public RectTransform Sector2Curve;

    public Data[] CurveData;

    private int _curveDataCurrent;

    public void SetActiveCurve(int index)
    {
      _curveDataCurrent = index;
      _curveDataCurrent = Mathf.Clamp(_curveDataCurrent, 0, CurveData.Length - 1);

      foreach (var data in CurveData) 
        data.SetActive(false);
      
      CurveData[_curveDataCurrent].SetActive(true);
    }

    public void SetSectorsPosition(float angleDeg, float angleL, float angleR)
    {
      Vector2 targetOnCurve = GetPosition(angleDeg, angleL, angleR);

      Sector1Curve.localPosition = ToRectPos(targetOnCurve, Sector1Curve.transform.parent.gameObject.GetComponent<RectTransform>());
      Sector2Curve.localPosition = ToRectPos(targetOnCurve, Sector2Curve.transform.parent.gameObject.GetComponent<RectTransform>());
    }

    public void SetSliderPosition(float angleDeg, float angleL, float angleR)
    {
      Vector2 targetOnCurve = GetPosition(angleDeg, angleL, angleR);
      
      Slider.localPosition = ToRectPos(targetOnCurve, Slider.transform.parent.gameObject.GetComponent<RectTransform>());
    }
    
    public void SetEffectPosition(RectTransform effectTransform) => 
      effectTransform.position = Sector1Curve.position;

    private Vector2 GetPosition(float angleDeg, float angleL, float angleR)
    {
      float bezierValue = 1 - (angleDeg - angleR) / (angleL - angleR);
      bezierValue = Mathf.Clamp(bezierValue, 0.001f, 0.999f); 
      
      return CurveData[_curveDataCurrent].BezierCurve.GetPointAt(bezierValue);
    }

    private Vector2 ToRectPos(Vector2 worldPos, RectTransform parent)
    {
      Vector2 screenPos = RectTransformUtility.WorldToScreenPoint(Camera.main, worldPos);
      RectTransformUtility.ScreenPointToLocalPointInRectangle(parent, screenPos, Camera.main, out Vector2 rectPos);

      return rectPos;
    }
  }
}