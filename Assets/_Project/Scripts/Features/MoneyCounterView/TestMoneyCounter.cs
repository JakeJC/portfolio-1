using _Project.Scripts.Features.MoneyCounterView.Logic;
using _Project.Scripts.Services.AssetProvider;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.Features.MoneyCounterView
{
  public class TestMoneyCounter : MonoBehaviour
  {
    private MoneyCounter _moneyCounter;
    private uint _count = 0;
    
    private void Start()
    {
      _moneyCounter = new MoneyCounter();
      _moneyCounter.Construct(new AssetProvider());
      _moneyCounter.Initialize(GameObject.Find("Canvas").transform);
    }

    [Button]
    public void SetCount(uint count) => 
      _moneyCounter.SetCount(count);

    [Button]
    public void Next()
    {
      _count++;   
      _moneyCounter.SetCount(_count);
    }

    [Button]
    public void Previous()
    {
      _count--;   
      _moneyCounter.SetCount(_count);
    }
  }
}