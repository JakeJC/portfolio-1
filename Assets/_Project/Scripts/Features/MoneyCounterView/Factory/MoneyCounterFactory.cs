using System.Collections.Generic;
using _Project.Scripts.Features.MoneyCounterView.Mono;
using _Project.Scripts.Services._Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject.Asteroids;

namespace _Project.Scripts.Features.MoneyCounterView.Factory
{
  public class MoneyCounterFactory : IMoneyCounterFactory
  {
    private const string DigitPrefabPath = "Digit";
    private const string SectionPrefabPath = "Section";
    private const string PanelPrefabPath = "Money Counter Panel";
    
    private readonly IAssetProvider _assetProvider;
    
    private Mono.MoneyCounterView _moneyCounterView;
    public List<DigitSection> Sections { get; set; } = new List<DigitSection>();

    public MoneyCounterFactory(IAssetProvider assetProvider) => 
      _assetProvider = assetProvider;

    public void SpawnBaseMoneyCounterPanel(Transform counterParent)
    {
      var moneyCounterView = _assetProvider.GetResource<Mono.MoneyCounterView>(PanelPrefabPath);
      _moneyCounterView = Object.Instantiate(moneyCounterView, counterParent);

      SpawnSections(_moneyCounterView.transform);
    }

    public void Clear()
    {
      Object.Destroy(_moneyCounterView.gameObject);
      Sections.Clear();
    }

    public void ShowCounter(bool isActive) => 
      _moneyCounterView.gameObject.SetActive(isActive);

    private void SpawnSections(Transform parent)
    {
      for (int i = 0; i < int.MaxValue.ToString().Length; i++)
        Sections.Add(CreateSection(parent, i));
    }

    private DigitSection CreateSection(Transform parent, int i)
    {
      Create(parent, out DigitSection section);

      SpawnDigits(section);
      
      return section;

      void Create(Transform transform, out DigitSection digitSection)
      {
        var sec = Object.Instantiate(_assetProvider.GetResource<GameObject>(SectionPrefabPath), transform);
        sec.name = i.ToString();
        digitSection = sec.GetComponent<DigitSection>();
      }
    }

    private void SpawnDigits(DigitSection digitSection)
    {
      for (int j = 0; j < 10; j++)
      {
        var digit = Object.Instantiate(_assetProvider.GetResource<GameObject>(DigitPrefabPath), digitSection.transform);
        digit.name = $"Digit {j}";
        
        var digitText = digit.GetComponent<TextMeshProUGUI>();
        digitText.text = j.ToString();

        SetupDigitRotation(j, digit);
        SetupDigitComponent(digit, j);
      }

      void SetupDigitRotation(int j, GameObject digit)
      {
        Quaternion angle = Quaternion.Euler(j * 360f / 10, 0, 0);
        var direction = angle * Vector3.back * 50;
        digit.transform.localPosition += direction;
        digit.transform.localRotation *= angle;
      }

      void SetupDigitComponent(GameObject digit, int j)
      {
        var component = digit.GetComponent<Digit>();
        component.Value = j;
        digitSection.Digits.Add(component);
      }
    }
  }
}