using System.Collections.Generic;
using _Project.Scripts.Features.MoneyCounterView.Mono;
using UnityEngine;

namespace _Project.Scripts.Features.MoneyCounterView.Factory
{
  public interface IMoneyCounterFactory
  {
    List<DigitSection> Sections { get; set; }
    void SpawnBaseMoneyCounterPanel(Transform counterParent);
    void Clear();
    void ShowCounter(bool isActive);
  }
}