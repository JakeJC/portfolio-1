using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Features.MoneyCounterView.Factory;
using _Project.Scripts.Features.MoneyCounterView.Mono;
using _Project.Scripts.Services._Interfaces;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Features.MoneyCounterView.Logic
{
  public class MoneyCounter : IMoneyCounter
  {
    private IMoneyCounterFactory _moneyCounterFactory;

    public void Construct(IAssetProvider assetProvider)
    {
      _moneyCounterFactory = new MoneyCounterFactory(assetProvider);
    }

    public void Initialize(Transform counterParent)
    {
      _moneyCounterFactory.SpawnBaseMoneyCounterPanel(counterParent);

      for (int i = 0; i < _moneyCounterFactory.Sections.Count; i++)
      {
        RotateToDigit(_moneyCounterFactory.Sections[i], 0, true);
        _moneyCounterFactory.Sections[i].gameObject.SetActive(i == 0);
      }
    }

    public void SetCount(uint count, float delay = 0, bool withoutAnimation = false)
    {
      if (count >= 0)
        OnCountChange(count, delay, withoutAnimation);
    }

    public void Show(bool isActive) =>
      _moneyCounterFactory.ShowCounter(isActive);

    public void Clear() =>
      _moneyCounterFactory.Clear();

    private void OnCountChange(uint count, float delay, bool withoutAnimation = false)
    {
      if (delay == 0)
        Change(count, withoutAnimation);
      else
      {   
        float value = 0;
        DOTween.To(() => value, x => value = x, delay, delay).onComplete += () => Change(count, withoutAnimation);
      }
    }

    private void Change(uint count, bool withoutAnimation = false)
    {
      var s = count.ToString().Reverse();
      IEnumerable<char> enumerable = s.ToList();

      for (int i = 0; i < _moneyCounterFactory.Sections.Count; i++)
      {
        if (i >= 0 && i < enumerable.Count())
        {
          _moneyCounterFactory.Sections[i].gameObject.SetActive(true);
          RotateToDigit(_moneyCounterFactory.Sections[i], int.Parse(enumerable.ElementAt(i).ToString()), withoutAnimation);
        }
        else
          _moneyCounterFactory.Sections[i].gameObject.SetActive(false);
      }
    }

    private void RotateToDigit(DigitSection digitSection, int digit, bool withoutAnimation = false)
    {
      UpdateDegreeCounter();

      float targetAngle = -digitSection.DegreeCounter * 360f / 10;

      if (withoutAnimation)
        digitSection.transform.rotation = Quaternion.Euler(targetAngle, 0, 0);
      else
        digitSection.transform.DORotateQuaternion(Quaternion.Euler(targetAngle, 0, 0), 0.25f);

      digitSection.CurrentDigit = digitSection.Digits.Select(p => p.Value).ToList().IndexOf(digit);

      ActivateRange(digitSection, digitSection.CurrentDigit, 2);

      digitSection.PreviousDigit = digit;

      void UpdateDegreeCounter()
      {
        if (digitSection.PreviousDigit == 9 && digit == 0)
          digitSection.DegreeCounter += 1;
        else if (digitSection.PreviousDigit == 0 && digit == 9)
          digitSection.DegreeCounter -= 1;
        else
          digitSection.DegreeCounter += digit - digitSection.PreviousDigit;
      }
    }

    private void ActivateRange(DigitSection digitSection, int index, int neighbors)
    {
      foreach (Digit t in digitSection.Digits)
        t.gameObject.SetActive(false);

      for (int i = index - neighbors; i <= index + neighbors; i++)
      {
        int ind = i;

        if (i < 0)
          ind = i + 10;

        if (i > 9)
          ind = i - 10;

        digitSection.Digits[ind].gameObject.SetActive(true);
      }
    }
  }
}