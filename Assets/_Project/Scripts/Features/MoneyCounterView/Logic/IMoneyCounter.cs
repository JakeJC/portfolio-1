using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.MoneyCounterView.Logic
{
  public interface IMoneyCounter
  {
    void Construct(IAssetProvider assetProvider);
    void Initialize(Transform parent);
    void SetCount(uint count, float delay = 0, bool withoutAnimation = false);
    void Clear();
    void Show(bool isActive);
  }
}