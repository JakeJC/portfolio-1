using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.Features.MoneyCounterView.Mono
{
  public class DigitSection : MonoBehaviour
  {
    public List<Digit> Digits = new List<Digit>();

    public int PreviousDigit { get; set; }
    public int DegreeCounter { get; set; }
    public int CurrentDigit { get; set; }
  }
}