using UnityEngine;

namespace _Project.Scripts.Features.MoneyCounterView.Mono
{
  public class Digit : MonoBehaviour
  {
    public int Value { get; set; }
  }
}