using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.BateWindow
{
  public class BateWindowFactory
  {
    private const string WinScreenPath = "Features/Windows/Bate Window";
    private const string BaseCanvas = "Canvas";
    
    private readonly IAssetProvider _assetProvider;

    public BateWindowFactory(IAssetProvider assetProvider) => 
      _assetProvider = assetProvider;

    public BateWindow SpawnBateWindow()
    {
      Transform parent = GameObject.Find(BaseCanvas).transform;

      var winScreen = _assetProvider.GetResource<Features.BateWindow.BateWindow>(WinScreenPath);
      return Object.Instantiate(winScreen, parent);
    }
  }
}