namespace _Project.Scripts.Features.BateWindow
{
  public static class FloatExtension
  {
    public static bool CheckRange(this float num, float min, float max) => 
      num > min && num < max;

    public static bool CheckRangeMax(this float num, float min, float max) => 
      num > min && num <= max;

    public static bool CheckRangeMin(this float num, float min, float max) => 
      num >= min && num < max;
    
    public static bool CheckIncludedRange(this float num, float min, float max) => 
      num >= min && num <= max;
  }
}