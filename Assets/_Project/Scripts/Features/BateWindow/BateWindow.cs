using System;
using _Project.Scripts.Animation;
using _Project.Scripts.Core_Logic;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.BateWindow
{
  public class BateWindow : MonoBehaviour
  {
    private const int MinAngle = 80;
    private const int MaxAngle = -80;

    public Image Background;
    public Image StarLeft;
    public Image StarRight;
    public Image StarCenter;
    public Image LightLeft;
    public Image LightRight;
    public Image LightCenter;
    public Transform Pointer;
    public TextMeshProUGUI RewardText;
    public RectTransform RewardTextTransform;
    
    public CanvasGroup MiddleContainer;
    public Button LaunchBtn;
    public Button ClaimBtn;
    public Button NoBtn;

    public TMP_Text LaunchBtnText;
    public TMP_Text ClaimBtnText;
    public TMP_Text NoBtnText;

    public CanvasGroup LaunchBtnCanvasGroup;
    public CanvasGroup ClaimBtnCanvasGroup;
    public CanvasGroup NoBtnCanvasGroup;

    private Action _onLaunch;
    private Action _onClaim;
    private Action _onNo;

    private ILocaleSystem _localeSystem;
    
    public void Construct(ILocaleSystem localeSystem, Action onLaunch, Action onClaim, Action onNo)
    {
      _localeSystem = localeSystem;
      _onNo = onNo;
      _onClaim = onClaim;
      _onLaunch = onLaunch;

      LaunchBtn.onClick.AddListener(Launch);
      ClaimBtn.onClick.AddListener(Claim);
      NoBtn.onClick.AddListener(No);

      gameObject.SetActive(false);

      AssignAnimations();
      Localize();
    }

    public void OpenWindow()
    {
      Background.color = Background.color.SetA(0);
      StarCenter.color = StarCenter.color.SetA(0);
      StarRight.color = StarRight.color.SetA(0);
      StarLeft.color = StarLeft.color.SetA(0);
      MiddleContainer.alpha = 0;
      LaunchBtnCanvasGroup.alpha = 0;
      ClaimBtnCanvasGroup.alpha = 0;
      NoBtnCanvasGroup.alpha = 0;
      ClaimBtnCanvasGroup.gameObject.SetActive(false);
      NoBtnCanvasGroup.gameObject.SetActive(false);
      gameObject.SetActive(true);

      DOTween.Sequence()
        .Insert(0.0f, Background.DOFade(1f, 0.1f))
        .Insert(0.1f, StarCenter.DOFade(1f, 0.4f))
        .Insert(0.3f, StarRight.DOFade(1f, 0.3f))
        .Insert(0.3f, StarLeft.DOFade(1f, 0.3f))
        .Insert(0.2f, MiddleContainer.DOFade(1f, 0.3f))
        .Insert(0.5f, LaunchBtnCanvasGroup.DOFade(1f, 0.3f))
        // .Insert(0.5f, ClaimBtnCanvasGroup.DOFade(1f, 0.3f))
        // .Insert(0.6f, NoBtnCanvasGroup.DOFade(1f, 0.3f))
        ;
    }

    public void CloseWindow() =>
      gameObject.SetActive(false);

    public void SwitchButtons()
    {
      DOTween.Sequence()
        .Insert(0.0f, LaunchBtnCanvasGroup.DOFade(0f, 0.2f))
        .AppendCallback(() => LaunchBtn.gameObject.SetActive(false))
        .AppendCallback(() => ClaimBtn.gameObject.SetActive(true))
        .Insert(0.3f, ClaimBtnCanvasGroup.DOFade(1f, 0.3f))
        .InsertCallback(0.3f, () => NoBtn.gameObject.SetActive(true))
        .Insert(1.5f, NoBtnCanvasGroup.DOFade(1f, 0.3f))
        ;
    }

    public void SetAdButtonScale(float value) =>
      ClaimBtn.transform.localScale = new Vector3(value, value, value);

    public void SetRewardText(string rewardText) =>
      RewardText.text = rewardText;

    public void SetPointerRotation(float normalizedValue) =>
      Pointer.localRotation = Quaternion.Euler(0, 0, Mathf.Lerp(MinAngle, MaxAngle, normalizedValue));

    public void SetCounterRotation(float normalizedValue) =>
      RewardTextTransform.localPosition = new Vector3(RewardTextTransform.localPosition.x, Mathf.Lerp(-42.7f, 42.7f, normalizedValue), RewardTextTransform.localPosition.z);

    private void No() =>
      _onNo?.Invoke();

    private void Claim() =>
      _onClaim?.Invoke();

    private void Launch() =>
      _onLaunch?.Invoke();

    private void SetColor(Color color) => 
      RewardText.color = color;

    private void Localize()
    {
      LaunchBtnText.text = _localeSystem.GetText("Main", "Start");
      ClaimBtnText.text = _localeSystem.GetText("Main", "Take");
      NoBtnText.text = _localeSystem.GetText("Main", "NoThanks");
    }

    private void AssignAnimations()
    {
      // new ButtonAnimation(ClaimBtn.transform);
      new ButtonAnimation(NoBtn.transform);
    }

    public void SetSegmentLight(int currentSegment)
    {
      LightLeft.enabled = currentSegment == 0;
      LightRight.enabled = currentSegment == 2;
      LightCenter.enabled = currentSegment == 1;
    }
  }
}