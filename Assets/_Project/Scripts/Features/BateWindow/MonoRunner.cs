using System;
using System.Collections;
using UnityEngine;

namespace _Project.Scripts.Features.BateWindow
{
  public class MonoRunner : MonoBehaviour
  {
    private bool _inProcess;

    private Coroutine _randomProcess;
    private float _arrowSpeed;

    public void Launch(float processValue, float arrowSpeed, Action<float> onTick)
    {
      _arrowSpeed = arrowSpeed;
      if (_randomProcess == null)
        _randomProcess = StartCoroutine(Random(processValue, onTick));
    }

    public void End() => 
      _inProcess = false;

    IEnumerator Random(float processValue, Action<float> onTick)
    {
      _inProcess = true;
      
      float time = processValue;
      
      while (_inProcess)
      {
        time += _arrowSpeed * Time.deltaTime;
        
        processValue = Mathf.PingPong(time, 1);
        onTick?.Invoke(processValue);
        
        yield return new WaitForEndOfFrame();
      }

      onTick?.Invoke(processValue);
    }
  }
}