using System;
using System.Collections.Generic;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.NotLoadedWindow;
using _Project.Scripts.Services._Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.BateWindow
{
  public class BateWindowLogic
  {
    private const string BateWindowConfigPath = "Bate Window Config";

    private static readonly int[] Multiply = { 2, 3, 2 };
    private const int SegmentCount = 3;
    private const float ArrowSpeed = 1;

    private readonly IMoneyService _moneyService;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAdsService _ads;

    private readonly BateWindowFactory _bateWindowFactory;
    private BateWindow _bateWindow;

    private MonoRunner _monoRunner;

    private readonly uint _baseReward;
    private int _currentSegment;
    private uint _baseRewardMultiplier = 1;
    
    private  uint BaseReward => _baseReward * _baseRewardMultiplier;

    public int CurrentSegment
    {
      get => _currentSegment;
      set
      {
        if (value != _currentSegment)
        {
          _currentSegment = value;
          _onCurrentSegmentChange?.Invoke(_currentSegment);
        }
      }
    }

    private Action<int> _onCurrentSegmentChange;
    private readonly UnityAction _nextAction;

    private readonly BateWindowConfig _bateWindowConfig;
    private GirlData _currentGirlData;
    private MiniGameData _miniGameData;

    private readonly IAnalytics _analytics;
    private readonly IAssetProvider _assetProvider;
    private Action _onFinish;

    public bool IsRewardWatched { get; private set; }

    public BateWindowLogic(IAnalytics analytics, ILocaleSystem localeSystem, IAssetProvider assetProvider,
      IAdsService ads, IMoneyService moneyService,
      uint baseReward, UnityAction nextAction)
    {
      _assetProvider = assetProvider;
      _bateWindowConfig = Resources.Load<BateWindowConfig>(BateWindowConfigPath);

      _analytics = analytics;
      _baseReward = baseReward;
      _nextAction = nextAction;

      _ads = ads;
      _moneyService = moneyService;
      _localeSystem = localeSystem;

      _bateWindowFactory = new BateWindowFactory(assetProvider);
    }

    public void Open(GirlData currentGirlData, MiniGameData miniGameData, Action onFinish = null, bool autoLaunch = false, uint baseRewardMultiplier = 1) 
    {
      _onFinish = onFinish;
      _miniGameData = miniGameData;
      _currentGirlData = currentGirlData;

      _baseRewardMultiplier = baseRewardMultiplier;

      _analytics.Send("ev_byte_screen", new Dictionary<string, object>
      {
        { "game_id", _miniGameData.Id },
        { "character_id", _currentGirlData.Id },
      });

      CreateBateWindow();
      _bateWindow.OpenWindow();

      if (autoLaunch)
      {
        Launch();
        _bateWindow.ClaimBtnText.text = _localeSystem.GetText("Main", "MiniGames.Mul");
        _bateWindow.NoBtnText.text = $"{_localeSystem.GetText("Main", "MiniGames.Take")} {BaseReward}$";
      }
    }

    public void Clear()
    {
      if (_bateWindow)
        Object.Destroy(_bateWindow.gameObject);
    }

    private void CreateBateWindow()
    {
      if(_bateWindow)
        Object.DestroyImmediate(_bateWindow);
        
      _bateWindow = _bateWindowFactory.SpawnBateWindow();
      _bateWindow.Construct(_localeSystem, Launch, Complete, Deny);
    }

    private void Launch()
    {
      _bateWindow.SwitchButtons();
      LaunchPointer();
    }

    private void Complete()
    {
      _analytics.Send("btn_x2", new Dictionary<string, object>
      {
        { "game_id", _miniGameData.Id },
        { "character_id", _currentGirlData.Id },
      });

      _ads.Rewarded.ShowReward("x2", () =>
      {
        if (DestroyRunner())
          return;

        IsRewardWatched = true;

        ClaimReward();

        _bateWindow.CloseWindow();

        _nextAction?.Invoke();
        
        _onFinish?.Invoke();
        _onFinish = null;
      }, () =>
      {
        INotLoadedWindowLogic notLoadedWindowLogic = new NotLoadedWindowLogic(_localeSystem, _assetProvider);
        notLoadedWindowLogic.Open();
      });
    }

    private void Deny()
    {
      if (DestroyRunner())
        return;

      Debug.Log($"Bate Window Earn: {BaseReward}");
      
      _moneyService.Earn(BaseReward);
      _bateWindow.CloseWindow();
      _nextAction?.Invoke();
      
      _onFinish?.Invoke();
      _onFinish = null;
    }

    private bool DestroyRunner()
    {
      if (!_monoRunner)
        return true;

      _monoRunner.End();
      Object.Destroy(_monoRunner.gameObject);
      return false;
    }

    private void ClaimReward()
    {
      Debug.Log($"Bate Window Earn: {BaseReward * Multiply[CurrentSegment]}");
      
      _moneyService.Earn((uint)(BaseReward * Multiply[CurrentSegment]));
    }

    private void OnRandomWork(float value)
    {
      _bateWindow.SetPointerRotation(value);
      _bateWindow.SetCounterRotation(value);

      CurrentSegment = CalculateCurrentSegment(value, SegmentCount);
      _bateWindow.SetSegmentLight(CurrentSegment);

      _bateWindow.SetAdButtonScale(_bateWindowConfig.ButtonAnimation.Evaluate(value));
    }

    private int CalculateCurrentSegment(float processValue, int segmentCount)
    {
      float step = 1f / segmentCount;
      float baseStep = 0;

      for (int i = 0; i < segmentCount; i++)
      {
        if (processValue.CheckIncludedRange(baseStep, baseStep + step))
          return i;

        baseStep += step;
      }

      throw new Exception("Check Range Error");
    }

    private void LaunchPointer()
    {
      float startValue = 0.5f;
      float processValue = startValue;

      var rewardText = string.Join("\n", new[]
      {
        $"{(uint)(BaseReward * Multiply[0])}",
        $"{(uint)(BaseReward * Multiply[1])}",
        $"{(uint)(BaseReward * Multiply[2])}",
      });
      _bateWindow.SetRewardText(rewardText);
      _monoRunner = new GameObject("Runner").AddComponent<MonoRunner>();
      _monoRunner.Launch(processValue, ArrowSpeed, OnRandomWork);
    }
  }
}