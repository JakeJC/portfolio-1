using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace _Project.Scripts.Features.BateWindow
{
  public class SegmentAnimation : MonoBehaviour
  {
    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private Transform _glow;
    [SerializeField] private CanvasGroup _segmentGlow;
    
    public Color EffectColor;
    
    private TweenerCore<Vector3, Vector3, VectorOptions> _scale;

    private void Awake()
    {
      _glow.transform.localScale = Vector3.zero;
      _segmentGlow.alpha = 0;
    }

    public void Stop()
    {
      _particleSystem.Stop();
      
      KillTween();
      
      _scale = _glow.DOScale(0, 0.2f);
      _segmentGlow.alpha = 0;
    }

    public void Launch()
    {
      _particleSystem.Play();
      
      KillTween();
      
      _scale = _glow.DOScale(1, 0.2f);
      _segmentGlow.alpha = 1;
    }

    private void KillTween()
    {
      if (_scale == null)
        _scale.Kill();
    }
  }
}