using UnityEngine;

namespace _Project.Scripts.Features.BateWindow
{
  [CreateAssetMenu(fileName = "Bate Window Config", menuName = "Create/Bate Window Config", order = 0)]
  public class BateWindowConfig : ScriptableObject
  {
    public AnimationCurve ButtonAnimation;
  }
}