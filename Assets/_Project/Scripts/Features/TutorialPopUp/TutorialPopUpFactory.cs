using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.TutorialPopUp
{
    public class TutorialPopUpFactory
    {
        private const string PopUpPath = "Features/TutorialPopUp/Tutorial Pop Up";
        private const string BaseCanvas = "Canvas";
        
        private readonly IAssetProvider _assetProvider;

        public TutorialPopUpFactory(IAssetProvider assetProvider) => 
            _assetProvider = assetProvider;

        public TutorialPopUp CreatePopUp(ILocaleSystem localeSystem)
        {
            Transform parent = GameObject.Find(BaseCanvas).transform;
            
            var popUpPrefab = _assetProvider.GetResource<TutorialPopUp>(PopUpPath);
            var obj = Object.Instantiate(popUpPrefab, parent);
            obj.transform.SetAsLastSibling();
            obj.Header.Construct(localeSystem);
            obj.Message.Construct(localeSystem);
            obj.ButtonText.Construct(localeSystem);
            return obj;
        }
    }
}
