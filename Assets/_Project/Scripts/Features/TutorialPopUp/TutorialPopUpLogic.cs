using System.Collections.Generic;
using _Project.Scripts.Animation;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using _Project.Scripts.Services.Localization.Mono;
using AppacheAnalytics.Scripts.Interfaces;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.TutorialPopUp
{
  public class TutorialPopUpLogic
  {
    public static readonly string KeyTutorIsComplete = "tutorIsComplete";
    
    private readonly TutorialPopUpFactory _popUpFactory;
    private TutorialPopUp _popUpUI;
    private GameObject _spotlightedObject;
    private int _currentStep;
    private int _stepsCount;
    private List<TutorialData> _tutorialDatas;
    
    private readonly ILocaleSystem _localeSystem;
    private readonly IAnalytics _analytics;
    
    private IAudioService _audioService;

    public TutorialPopUpLogic(IAudioService audioService, ILocaleSystem localeSystem, IAssetProvider assetProvider, 
      IAnalytics analytics, List<TutorialData> tutorialDatas)
    {
      _audioService = audioService;
      _localeSystem = localeSystem;
      _analytics = analytics;
      
      _tutorialDatas = tutorialDatas;
      _currentStep = 0;
      _stepsCount = tutorialDatas.Count;
      
      _popUpFactory = new TutorialPopUpFactory(assetProvider);
      
      Create();
      Open();
    }

    private void SpotlightNextObject()
    {
      if (_currentStep >= _stepsCount)
      {
        _analytics.Send($"ev_tutor_finish");

        PlayerPrefs.SetInt(KeyTutorIsComplete, 1);
        Close();
        return;
      }

      if (_currentStep > 0)
      {
        _analytics.Send($"ev_tutor_start", new Dictionary<string, object>()
        {
          { "step", _currentStep + 1 }
        });
      }

      if (_spotlightedObject != null)
      {
        Object.Destroy(_spotlightedObject);
        _spotlightedObject = null;
      }
      
      _popUpUI.Header.SetNewKey(_tutorialDatas[_currentStep].HeaderKey);
      _popUpUI.Message.SetNewKey(_tutorialDatas[_currentStep].MessageKey);
      
      _spotlightedObject = Object.Instantiate(_tutorialDatas[_currentStep].SpotlightedObject, _tutorialDatas[_currentStep].SpotlightedObject.transform);
      
      _spotlightedObject.transform.localPosition = Vector3.zero;
      _spotlightedObject.transform.localScale = Vector3.one;
      _spotlightedObject.transform.parent = _popUpUI.SpotlightParent.transform;
      
      _popUpUI.Backlight.transform.position = _spotlightedObject.transform.position;
      _popUpUI.Backlight.transform.SetAsFirstSibling();
      _popUpUI.Backlight.DOFade(1, 0.5f);

      if (_spotlightedObject.TryGetComponent(out Button spotlightButton))
      {
        spotlightButton.onClick.RemoveAllListeners();
        spotlightButton.onClick.AddListener(SpotlightNextObject);

        var background = spotlightButton.transform.Find("img-btn-bg");
        if(background)
          Object.Destroy(background.gameObject);
        
        new ButtonAnimation(spotlightButton.transform);
        new ButtonSound(_audioService, spotlightButton.gameObject, AudioId.Karaoke_Button);
      }

      if (_currentStep > 0)
      {
        _popUpUI.Header.gameObject.SetActive(false);
        var rectTransform = _popUpUI.Message.GetComponent<RectTransform>();
        rectTransform .anchoredPosition = Vector2.zero;
      }

      _currentStep++;
      
      if (_currentStep >= _stepsCount)
        _popUpUI.ButtonText.SetNewKey("Complete");
    }

    private void Open()
    {
      Sequence sequence = DOTween.Sequence();
      sequence.AppendInterval(0.5f);
      sequence.Append(_popUpUI.Shadow.DOFade(1, 1f));
      sequence.Append(_popUpUI.PopUpWindow.DOFade(1, 1f).OnComplete((SpotlightNextObject)));
      
      _analytics.Send($"ev_tutor_start", new Dictionary<string, object>()
      {
        {"step", _currentStep + 1}
      });
    }

    private void Close()
    {
      _popUpUI.PopUpGroup.DOFade(0, 1f).OnComplete(Clear);
    }

    private void Clear()
    {
      if(_popUpUI)
        Object.Destroy(_popUpUI.gameObject);
    }

    private void Create()
    {
      _popUpUI = _popUpFactory.CreatePopUp(_localeSystem);
      
      _popUpUI.Shadow.alpha = 0;
      _popUpUI.PopUpWindow.alpha = 0;
      _popUpUI.Backlight.alpha = 0;
      
      _popUpUI.Header.SetNewKey(_tutorialDatas[_currentStep].HeaderKey);
      _popUpUI.Message.SetNewKey(_tutorialDatas[_currentStep].MessageKey);
      _popUpUI.ButtonText.SetNewKey("Next");
      
      new ButtonAnimation(_popUpUI.NextButton.transform);
      _popUpUI.NextButton.onClick.AddListener(SpotlightNextObject);
      
      new ButtonSound(_audioService, _popUpUI.NextButton.gameObject, AudioId.Karaoke_Button);
    }
  }
}
