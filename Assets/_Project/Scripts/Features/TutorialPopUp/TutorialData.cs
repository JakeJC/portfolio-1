using UnityEngine;

namespace _Project.Scripts.Features.TutorialPopUp
{
  public class TutorialData
  {
    public string HeaderKey;
    public string MessageKey;
    public GameObject SpotlightedObject;

    public TutorialData(string headerKey, string messageKey, GameObject spotlightedObject)
    {
      HeaderKey = headerKey;
      MessageKey = messageKey;
      SpotlightedObject = spotlightedObject;
    }
  }
}
