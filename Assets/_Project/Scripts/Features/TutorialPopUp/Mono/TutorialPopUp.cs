using _Project.Scripts.Services.Localization.Mono;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.TutorialPopUp
{
  public class TutorialPopUp : MonoBehaviour
  {
    [SerializeField] private LocalizedMainText _header;
    [SerializeField] private LocalizedMainText _message;
    [SerializeField] private LocalizedMainText _buttonText;
    [SerializeField] private CanvasGroup _popUpGroup;
    [SerializeField] private CanvasGroup _shadow;
    [SerializeField] private CanvasGroup _popUpWindow;
    [SerializeField] private CanvasGroup _spotlightParent;
    [SerializeField] private CanvasGroup _backLight;
    [SerializeField] private Button _nextButton;
    
    public LocalizedMainText Header => _header;
    public LocalizedMainText Message => _message;
    public LocalizedMainText ButtonText => _buttonText;
    public CanvasGroup PopUpGroup => _popUpGroup;
    public CanvasGroup Shadow => _shadow;
    public CanvasGroup PopUpWindow => _popUpWindow;
    public CanvasGroup SpotlightParent => _spotlightParent;
    public CanvasGroup Backlight => _backLight;
    public Button NextButton => _nextButton;
  }
}
