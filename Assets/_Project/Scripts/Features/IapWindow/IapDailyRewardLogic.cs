using System;
using System.Collections.Generic;
using System.Globalization;
using _Project.Scripts.Features.IapWindow.Data;
using _Project.Scripts.Features.IapWindow.Mono;
using _Project.Scripts.Features.MoneyCounterView.Logic;
using _Project.Scripts.Services._data;
using _Project.Scripts.Services._Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.IapWindow
{
  public class IapDailyRewardLogic
  {
    private const string TimerSaveKey = "IapDailyRewardLogic.TimerSaveKey";
    private const string SessionSaveKey = "IapDailyRewardLogic.SessionSaveKey";
    private const string LastTakenSessionSaveKey = "IapDailyRewardLogic.LastTakenSessionSaveKey";
    private const int SessionPeriod = 1;

    public static event Action<bool> OnUpdateView;

    private readonly IUiFactory _uiFactory;
    private readonly IMoneyService _moneyService;
    private readonly IAnalytics _analytics;
    private readonly IGameSaveSystem _gameSaveSystem;

    private static string Timer
    {
      get => PlayerPrefs.GetString(TimerSaveKey, DateTime.MinValue.ToString(CultureInfo.InvariantCulture));
      set => PlayerPrefs.SetString(TimerSaveKey, value);
    }

    private static int Session
    {
      get => PlayerPrefs.GetInt(SessionSaveKey, 0);
      set => PlayerPrefs.SetInt(SessionSaveKey, value);
    }

    private static int LastTaken
    {
      get => PlayerPrefs.GetInt(LastTakenSessionSaveKey, 0);
      set => PlayerPrefs.SetInt(LastTakenSessionSaveKey, value);
    }

    private static double CurrentPeriod
    {
      get
      {
        TimeSpan timeSpan = GetTimeSpan();

#if UNITY_EDITOR
        double currentPeriod = timeSpan.TotalMinutes;
#else
        double currentPeriod = timeSpan.TotalDays;
#endif

        return currentPeriod;
      }
    }

    public IapDailyRewardLogic(IUiFactory uiFactory, IMoneyService moneyService, IGameSaveSystem gameSaveSystem, IAnalytics analytics)
    {
      _uiFactory = uiFactory;
      _moneyService = moneyService;
      _analytics = analytics;
      _uiFactory = uiFactory;
      _moneyService = moneyService;
      _gameSaveSystem = gameSaveSystem;
      _analytics = analytics;

      OnUpdateView += UpdateButtons;
    }

    public void Cleanup() =>
      OnUpdateView -= UpdateButtons;

    public void InitUpdate() => 
      Update();

    public string GetTimerValue() =>
      (new TimeSpan(24, 0, 0) - GetTimeSpan()).ToString(@"hh\:mm\:ss");

    public void TakeDailyReward(IapDailyRewardData data)
    {
      LastTaken = Session;
      SaveTime();
      UpdateView();

      _moneyService.Earn((uint)data.Currency);

      if (data.Photo)
      {
        GameSaveData saves = _gameSaveSystem.Get();
        saves.UnlockedPhotoContent.Add(data.Photo.Id);
      }

      _analytics.Send("ev_present ", new Dictionary<string, object>()
      {
        { "day_number", LastTaken }
      });
    }

    private void UpdateButtons(bool value)
    {
      Increment();

      int session = Mathf.Clamp(Session, 0, _uiFactory.IapWindow.DailyRewardView.DailyRewardButtons.Length) - 1;
      var i = 0;
      foreach (IapDailyRewardButtonView button in _uiFactory.IapWindow.DailyRewardView.DailyRewardButtons)
      {
        if (i == session && value)
          button.Activate(true);
        else
          button.Activate(false);

        button.IsTaken(Mathf.Clamp(LastTaken, 0, _uiFactory.IapWindow.DailyRewardView.DailyRewardButtons.Length - 1));
        i++;
      }

      _uiFactory.IapWindow.DailyRewardView.ShowTimer(!value);
    }

    public static void CreateUpdater()
    {
      if (Object.FindObjectOfType<IapDailyRewardUpdater>() == null)
      {
        IapDailyRewardUpdater updater = new GameObject("IapDailyRewardUpdater").AddComponent<IapDailyRewardUpdater>();
        updater.Construct(Update);
      }
    }

    private static void Update()
    {
      Increment();
      UpdateView();
    }

    private static void Increment()
    {
      if (PlayerPrefs.HasKey(TimerSaveKey))
      {
        if (CurrentPeriod > SessionPeriod)
        {
          if (Mathf.Abs((int)CurrentPeriod - SessionPeriod) == 0)
          {
            if (LastTaken - Session == 0)
              SessionUp();
          }
          else
            ReloadRewardChain();
        }
      }
      else
      {
        SessionUp();
        SaveTime();
      }
    }

    private static void UpdateView() =>
      OnUpdateView?.Invoke(LastTaken == Session - 1);

    private static void ReloadRewardChain()
    {
      SaveTime();
      Session = 1;
      LastTaken = 0;

      Debug.Log("IapDailyRewardLogic.ReloadRewardChain");
    }

    private static void SessionUp()
    {
      Session++;

      Debug.Log("IapDailyRewardLogic.SessionUp");
    }

    private static TimeSpan GetTimeSpan()
    {
      DateTime dateSaved = DateTime.Parse(Timer, CultureInfo.InvariantCulture);
      return DateTime.Now - dateSaved;
    }

    private static void SaveTime() =>
      Timer = DateTime.Now.ToString(CultureInfo.InvariantCulture);
  }
}