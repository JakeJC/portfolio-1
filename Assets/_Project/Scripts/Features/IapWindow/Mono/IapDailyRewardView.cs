using System;
using System.Collections;
using _Project.Scripts.Animation;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.IapWindow.Mono
{
  public class IapDailyRewardView : MonoBehaviour
  {
    public RectTransform Content;
    public Button ButtonTake;
    public GameObject TimerPanel;
    public TextMeshProUGUI TimerText;

    [HideInInspector] public IapDailyRewardButtonView[] DailyRewardButtons;
    private Func<string> _getTimerValue;

    public void Construct(Func<string> getTimerValue)
    {
      ButtonTake.onClick.AddListener(OnButtonTake);
      
      _getTimerValue = getTimerValue;
      SetTimer();
      
      new ButtonAnimation(ButtonTake.transform);
    }

    public void OnEnable() => 
      StartCoroutine(UpdateTimer());

    private void OnDisable() => 
      StopCoroutine(UpdateTimer());

    public void ShowTimer(bool value)
    {
      TimerPanel.SetActive(value);
      ButtonTake.gameObject.SetActive(!value);
    }

    public void SetTimer() => 
      TimerText.text = _getTimerValue?.Invoke();

    private IEnumerator UpdateTimer()
    {
      while (true)
      {
        SetTimer();
        yield return new WaitForSeconds(1);
      }
    }

    private void OnButtonTake()
    {
      foreach (var rewardButton in DailyRewardButtons)
      {
        if (rewardButton.Button.interactable)
          rewardButton.Button.onClick?.Invoke();
      }
    }
  }
}