using System;
using System.Collections;
using UnityEngine;

namespace _Project.Scripts.Features.IapWindow.Mono
{
  public class IapDailyRewardUpdater : MonoBehaviour
  {
    private Action _onUpdate;

    public void Construct(Action onUpdate) => 
      _onUpdate = onUpdate;

    public void OnEnable() =>
      StartCoroutine(UpdateTimer());

    private void OnDisable() =>
      StopCoroutine(UpdateTimer());

    private IEnumerator UpdateTimer()
    {
      while (true)
      {
        _onUpdate?.Invoke();
        yield return new WaitForSeconds(1f);
      }
    }
  }
}