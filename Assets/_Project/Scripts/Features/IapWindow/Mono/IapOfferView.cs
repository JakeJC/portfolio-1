using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Animation;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Mono;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.IapWindow.Mono
{
  public class IapOfferView : MonoBehaviour
  {
    public Button CloseButton;
    public Button BuyButton;

    public CanvasGroup BackgroundGrp;
    public CanvasGroup MainGrp;
    public CanvasGroup BuyGrp;

    public TextMeshProUGUI BuyText;
    
    public void Construct(ILocaleSystem localeSystem, string cost, Action onClose, Action onBuy)
    {
      CloseButton.onClick.AddListener(delegate { onClose?.Invoke(); });
      BuyButton.onClick.AddListener(delegate { onBuy?.Invoke(); });
      
      new ButtonAnimation(BuyButton.transform);

      Localize(localeSystem, cost);
    }

    public void Open()
    {
      BackgroundGrp.alpha = 0;
      MainGrp.alpha = 0;
      BuyGrp.alpha = 0;
      BackgroundGrp.interactable = true;
      gameObject.SetActive(true);

      Sequence mySequence = DOTween.Sequence();
      mySequence.Append(BackgroundGrp.DOFade(1, 0.3f));
      mySequence.Append(MainGrp.DOFade(1, 0.7f));
      mySequence.Append(BuyGrp.DOFade(1, 1.3f));
    }

    public void Close()
    {
      BackgroundGrp.interactable = false;
      BackgroundGrp.DOFade(0, 0.5f).OnComplete(delegate { gameObject.SetActive(false); });
    }

    private void Localize(ILocaleSystem localeSystem, string cost)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Localize();
      });

      BuyText.text = $"{localeSystem.GetText("Main", "Buy")} {cost}";
    }
  }
}