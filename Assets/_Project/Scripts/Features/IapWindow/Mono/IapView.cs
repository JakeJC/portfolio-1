using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Animation;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.Localization.Mono;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.IapWindow.Mono
{
  public class IapView : MonoBehaviour
  {
    private const float Duration = .3f;
    public Button BackButton;

    public CanvasGroup CanvasGroup;
    public CanvasGroup DailyRewardGroup;
    public CanvasGroup IapGroup;

    public Transform SmallContentParent;
    public Transform LargeContentParent;

    public IapDailyRewardView DailyRewardView;
    
    private Action _onOpenWindow;

    public void Construct(ILocaleSystem localeSystem, Action onOpenWindow)
    {
      _onOpenWindow = onOpenWindow;
      BackButton.onClick.AddListener(CloseWindow);
      CreateAnimation();

      Localize(localeSystem);
    }

    public void OpenWindow()
    {
      _onOpenWindow?.Invoke();
      gameObject.SetActive(true);
      CanvasGroup.DOFade(1f, Duration);
      CanvasGroup.blocksRaycasts = true;

      IapGroup.alpha = 0;
      IapGroup.DOFade(1f, Duration).SetDelay(0.3f);
      
      if (DailyRewardGroup)
      {
        DailyRewardGroup.alpha = 0;
        DailyRewardGroup.DOFade(1f, Duration).SetDelay(0.1f);
      }
    }

    private void CloseWindow()
    {
      CanvasGroup.blocksRaycasts = false;
      CanvasGroup.DOFade(0f, Duration)
        .OnComplete(() => gameObject.SetActive(false));
    }

    private void CreateAnimation()
    {
      new ButtonAnimation(BackButton.transform);
    }
    
    public void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Localize();
      });
    }
  }
}