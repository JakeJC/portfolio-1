using System;
using _Project.Scripts.Features.IapWindow.Data;
using _Project.Scripts.Services._Interfaces;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.IapWindow.Mono
{
  public class IapDailyRewardButtonView : MonoBehaviour
  {
    public Button Button;
    public TextMeshProUGUI Title;
    public TextMeshProUGUI Description;
    public Image Icon;
    public Image Glow;
    public Image Garland;
    public Image Darker;
    public RectTransform ScalablePanel;
    public Canvas SortingCanvas;
    public ParticleSystem ParticleSystem;

    private int _day;

    private float _defaultScale;

    public void Construct(ILocaleSystem localeSystem, IapDailyRewardData data, Action<IapDailyRewardData> onTake, int day)
    {
      _day = day;
      Description.text = $"{data.Currency}$";
      Button.onClick.AddListener(delegate
      {
        onTake?.Invoke(data);

        ParticleSystem.gameObject.SetActive(false);
        ParticleSystem.gameObject.SetActive(true);
      });

      Title.text = $"{localeSystem.GetText("Main", "IAP.DailyRewardDay")} {day}";

      if (data.Photo != null)
        Garland.gameObject.SetActive(true);

      Icon.sprite = data.Icon;
      Icon.SetNativeSize();

      _defaultScale = ScalablePanel.localScale.x;
    }

    public void Activate(bool value)
    {
      Button.interactable = value;
      SortingCanvas.overrideSorting = value;
      
      if (value)
      {
        ScalablePanel.DOScale(Vector3.one, 0.5f).OnStart(delegate
          {
            Glow.gameObject.SetActive(true);
            Glow.DOFade(1, 0);
          })
          .OnComplete(delegate
          {
            Darker.gameObject.SetActive(false);
          });
      }
      else
        ScalablePanel.DOScale(Vector3.one * _defaultScale, 0.5f)
          .OnComplete(delegate
          {
            Glow.DOFade(1, 0)
              .OnComplete(delegate { Glow.DOFade(0, 0.3f).OnComplete(delegate
              {
                Glow.gameObject.SetActive(false);
              }); });
          });
    }

    public void IsTaken(int lastTaken)
    {
      Darker.gameObject.SetActive(true);

      if (lastTaken < _day)
        Darker.gameObject.SetActive(false);
    }
  }
}