using System;
using System.Collections.Generic;
using _Project.Scripts.Features.IapWindow.Data;
using _Project.Scripts.Services._data;
using _Project.Scripts.Services._Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.Purchasing;

namespace _Project.Scripts.Features.IapWindow
{
  public class IapRewards
  {
    private const string NoAds = "noAds";
    private const string AddMoney100 = "gold100";
    private const string AddMoney10000 = "coinspack";
    private const string BikiniPack = "bikiniPack";
    private const string CosplayPack = "cosplayPack";
    private const string PinUpPack = "pinUpPack";

    private const string LisaPack = "lisapack";
    private const string JoyPack = "joypack";
    private const string Gold100 = "gold100";
    private const string MoneyPack = "moneypack";
    private const string BigMoneyPack = "bigmoneypack";

    private readonly IAdsService _adsService;
    private readonly IMoneyService _moneyService;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IapItemData[] _iapItemsData;
    private readonly IAnalytics _analytics;

    private readonly Dictionary<string, Action> _rewards = new Dictionary<string, Action>();

    public IapRewards(IAdsService adsService, IMoneyService moneyService, IGameSaveSystem gameSaveSystem,
      IRemote remote, IapItemData[] iapItemsData, IAnalytics analytics)
    {
      _adsService = adsService;
      _moneyService = moneyService;
      _gameSaveSystem = gameSaveSystem;
      _iapItemsData = iapItemsData;
      _analytics = analytics;

      if (remote.ShouldEnableFeatureTest())
      {
        _rewards.Add(LisaPack, () => OpenContent(LisaPack));
        _rewards.Add(JoyPack, delegate
        {
          OpenContent(JoyPack);
          AddMoney(10000);
        });
        _rewards.Add(Gold100, () => AddMoney(100));
        _rewards.Add(MoneyPack, () => AddMoney(20000));
        _rewards.Add(BigMoneyPack, () => AddMoney(50000));
      }
      else
      {
        _rewards.Add(NoAds, TurnOnNoAds);
        _rewards.Add(AddMoney100, () => AddMoney(100));
        _rewards.Add(AddMoney10000, () => AddMoney(10000));
        _rewards.Add(BikiniPack, () => Soon(1));
        _rewards.Add(CosplayPack, () => Soon(2));
        _rewards.Add(PinUpPack, () => Soon(3));
      }
    }

    public void GetReward(string productId, ProductType type, bool restore = false)
    {
      Debug.Log($"IAP GetReward IsRestore:{restore}"); 
      
      if (type == ProductType.NonConsumable)
      {
        if (!_gameSaveSystem.Get().UnlockedIapContent.Contains(productId))
        {
          _gameSaveSystem.Get().UnlockedIapContent.Add(productId);
          _gameSaveSystem.Save();
        }
      }

      _rewards[productId].Invoke();

      if(restore == false)
        SendAnalytics(productId);
    }

    private void SendAnalytics(string productId)
    {
      foreach (IapItemData data in _iapItemsData)
      {
        if (data.ProductId == productId && string.IsNullOrEmpty(data.AnalyticsId ) == false)
        {
          Debug.Log($"IAP SendAnalytics:{data.ProductId}");
          _analytics.Send("ev_purchase", new Dictionary<string, object>()
          {
            { "purchase_type", data.AnalyticsId } 
          });
        }
      }
    }

    private void Soon(uint skinNumber) =>
      Debug.Log("Soon + " + skinNumber);

    private void OpenContent(string productId)
    {
      GameSaveData saves = _gameSaveSystem.Get();

      foreach (IapItemData data in _iapItemsData)
      {
        if (data.ProductId == productId)
        {
          Debug.Log("OpenContent + " + data.ProductId);

          foreach (var galleryData in data.UnlockGalleryDatas)
            saves.UnlockedPhotoContent.Add(galleryData.Id);

          foreach (var jewelryData in data.UnlockJewelryItemDatas)
            saves.UnlockedPassiveContent.Add(jewelryData.Id);
        }
      }

      _gameSaveSystem.Save();
    }

    private void TurnOnNoAds() =>
      _adsService.DisableAds();

    private void AddMoney(uint income)
    {
      _moneyService.Earn(income);
    }
  }
}