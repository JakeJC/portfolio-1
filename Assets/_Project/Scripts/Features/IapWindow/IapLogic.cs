using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using _Project.Scripts.Features.IapWindow.Data;
using _Project.Scripts.Features.IapWindow.Mono;
using _Project.Scripts.Features.IapWindow.Serialized;
using _Project.Scripts.Features.NotLoadedWindow;
using _Project.Scripts.Features.ScrollView;
using _Project.Scripts.Features.ScrollView.Serialized;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote;
using AppacheRemote.Scripts.Interfaces;
using Google.MiniJSON;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.Purchasing;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.IapWindow
{
  public class IapLogic : IStoreListener
  {
    private struct IapOfferData
    {
      public IapOfferView View;
      public Func<Transform, IapOfferView> OnCreate;
    }

    public const string OfferLisaPackId = "lisapack";
    public const string OfferJoyPackId = "joypack";

    private const string IapItemDataPath = "Features/Data/IapItemData";

    private IStoreController _mStoreController;

    private readonly IUiFactory _uiFactory;
    private readonly IAdsService _adsService;
    private readonly IMoneyService _moneyService;
    private readonly ILocaleSystem _localeSystem;
    private readonly IUnlockService _unlockService;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IAudioService _audioService;
    private readonly IRemote _remote;
    private readonly IAssetProvider _assetProvider;

    private readonly IapRewards _iapRewards;
    private readonly IapItemData[] _iapItemsData;
    private readonly List<ScrollViewIapItemButton> _iapItemButtons = new List<ScrollViewIapItemButton>();

    private readonly IapDailyRewardLogic _dailyRewardLogic;

    private Dictionary<string, IapOfferData> _offerDatas;
    private Action<bool> _onOfferPurchase;

    public IapLogic(IAssetProvider assetProvider, IAdsService adsService, IUiFactory uiFactory, ILocaleSystem localeSystem,
      IUnlockService unlockService, IMoneyService moneyService, IGameSaveSystem gameSaveSystem, IAudioService audioService, IRemote remote, IAnalytics analytics)
    {
      _assetProvider = assetProvider;
      _uiFactory = uiFactory;
      _adsService = adsService;
      _moneyService = moneyService;
      _localeSystem = localeSystem;
      _unlockService = unlockService;
      _gameSaveSystem = gameSaveSystem;
      _audioService = audioService;
      _remote = remote;

      _iapItemsData = assetProvider.GetAllResources<IapItemData>(IapItemDataPath + (_remote.ShouldEnableFeatureTest() ? "-AB" : string.Empty));

      _iapRewards = new IapRewards(_adsService, _moneyService, _gameSaveSystem, remote, _iapItemsData, analytics);

      if (remote.ShouldEnableFeatureTest())
      {
        _dailyRewardLogic = new IapDailyRewardLogic(uiFactory, _moneyService, _gameSaveSystem, analytics);
        uiFactory.SpawnIapWindow(null, delegate(IapDailyRewardData data)
        {
          _dailyRewardLogic.TakeDailyReward(data);
          uiFactory.IapWindow.DailyRewardView.SetTimer();
        }, _localeSystem);
        
        uiFactory.IapWindow.DailyRewardView.Construct(_dailyRewardLogic.GetTimerValue);
        _dailyRewardLogic.InitUpdate();
      }
      else
        uiFactory.SpawnIapWindow(null, null, _localeSystem);

      FillOfferDatas();

      InitializePurchasing();

      void FillOfferDatas()
      {
        _offerDatas = new Dictionary<string, IapOfferData>
        {
          {
            OfferLisaPackId, new IapOfferData
            {
              View = null,
              OnCreate = uiFactory.CreateOfferLisaMiniBikini
            }
          },
          {
            OfferJoyPackId, new IapOfferData
            {
              View = null,
              OnCreate = uiFactory.CreateOfferJoy
            }
          },
        };
      }
    }

    public void OpenIapOffer(string id, Transform parent, Action<bool> onOfferPurchase, Action onAlreadyUnlocked)
    {
      if (IsUnlocked(_iapItemsData.FirstOrDefault(x => x.ProductId == id))) 
      {
        onAlreadyUnlocked?.Invoke();
        return;
      }

      IapOfferData offerData = _offerDatas[id];

      if (offerData.View == null)
      {
        offerData.View = offerData.OnCreate.Invoke(parent);

        offerData.View.Construct(_localeSystem, GetPrice(_iapItemsData.FirstOrDefault(x => x.ProductId == id)),
          offerData.View.Close, delegate
          {
            _onOfferPurchase = delegate(bool result)
            {
              if (result)
                offerData.View.Close();

              onOfferPurchase?.Invoke(result);
            };
            OpenBuying(id);
          });
      }

      offerData.View.Open();
    }

    private void CreateIapContent()
    {
      foreach (IapItemData item in _iapItemsData)
      {
        ScrollViewIapItemButton scrollViewItemButton = item.ButtonSize == IapButtonSize.Small
          ? _uiFactory.CreateIapItemSmallButton(_uiFactory.IapWindow.SmallContentParent)
          : _uiFactory.CreateIapItemLargeButton(_uiFactory.IapWindow.LargeContentParent);

        scrollViewItemButton.Construct(_localeSystem, _unlockService,
          new ScrollViewItemData()
          {
            Id = item.ProductId, NameKey = item.Name, AdditionalNameKey = item.NameAdditional, DescriptionKey = item.Description, Icon = item.Icon,
            DescriptionAdditional = item.DescriptionAdditional, DescriptionIcon = item.DescriptionIcon
          },
          () => { OpenBuying(item.ProductId); }, _audioService, String.Empty, _remote);

        scrollViewItemButton.SetLock(item.UnlockType);
        scrollViewItemButton.SetCost(GetPrice(item));

        if (IsUnlocked(item) && item.ProductType == ProductType.NonConsumable)
        {
          scrollViewItemButton.PurchaseOn(true);
          scrollViewItemButton.SetLockButton(false);
        }

        _iapItemButtons.Add(scrollViewItemButton);
      }

      _uiFactory.IapWindow.Localize(_localeSystem);
    }

    private void InitializePurchasing()
    {
      var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

      for (int i = 0; i < _iapItemsData.Length; i++)
        builder.AddProduct(_iapItemsData[i].ProductId, _iapItemsData[i].ProductType);

      UnityPurchasing.Initialize(this, builder);
    }

    private void RestoreNonConsumable(string id) 
    {
      IapItemData iapItemsData = _iapItemsData.FirstOrDefault(item => item.ProductId == id);
      
      if (iapItemsData is { ProductType: ProductType.NonConsumable }) 
      {
        Debug.Log($"IAP TryToRestoreNonConsumable:{id} HasReceipt:{GetProduct(id).hasReceipt} IsUnlocked:{IsUnlocked(iapItemsData)}");
        if (GetProduct(id).hasReceipt && !IsUnlocked(iapItemsData))
        {
          _iapRewards.GetReward(id, iapItemsData.ProductType, true);
          Debug.Log($"IAP RestoreNonConsumables:{id}");
        }
      }
    }

    private void OpenBuying(string productId)
    {
      Debug.Log($"OnClick:{productId}");

      IapItemData item = _iapItemsData.FirstOrDefault(item => item.ProductId == productId);

      if (item != null)
      {
        if (item.UnlockType == UnlockIapType.Advertising)
        {
          _adsService.Rewarded.ShowReward(item.ProductId, () => { _iapRewards.GetReward(productId, item.ProductType); }, () =>
          {
            INotLoadedWindowLogic notLoadedWindowLogic = new NotLoadedWindowLogic(_localeSystem, _assetProvider);
            notLoadedWindowLogic.Open();
          });
          return;
        }
      }

      if (Application.isEditor)
      {
        Product product = GetProduct(productId);
        GetIapItem(product)?.PurchaseOn();

        IapItemData first = _iapItemsData.FirstOrDefault(item => item.ProductId == productId);
        _iapRewards.GetReward(productId, first.ProductType);
        _onOfferPurchase?.Invoke(true);
        _onOfferPurchase = null;
        return;
      }

      _mStoreController.InitiatePurchase(productId);
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
      Debug.Log("[TEST] In-App Purchasing successfully initialized");
      _mStoreController = controller;

      foreach (IapItemData data in _iapItemsData)
        RestoreNonConsumable(data.ProductId);
      
      CreateIapContent();
    }

    public void OnInitializeFailed(InitializationFailureReason error) =>
      Debug.Log($"In-App Purchasing initialize failed: {error}");

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
      Product product = args.purchasedProduct;

      GetIapItem(product)?.PurchaseOn(); 
      
      IapItemData iapItemsData = _iapItemsData.FirstOrDefault(item => item.ProductId == product.definition.id);
      if (product.definition.type == ProductType.NonConsumable) 
      {
        if (!IsUnlocked(iapItemsData)) 
          _iapRewards.GetReward(product.definition.id, product.definition.type);
      }
      else
        _iapRewards.GetReward(product.definition.id, product.definition.type);

      _onOfferPurchase?.Invoke(true);
      _onOfferPurchase = null;

      OnProcessPurchase(args);

      Debug.Log($"Purchase Complete - Product: {product.definition.id}");
      return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
      Debug.Log($"Purchase failed - Product: '{product.definition.id}', PurchaseFailureReason: {failureReason}");

      _onOfferPurchase?.Invoke(false);
      _onOfferPurchase = null;
    }

    public void Clear()
    {
      if (_uiFactory.IapWindow) 
        Object.Destroy(_uiFactory.IapWindow.gameObject);

      _dailyRewardLogic?.Cleanup();
    }

    private void OnProcessPurchase(PurchaseEventArgs purchaseEventArgs)
    {
      decimal price = purchaseEventArgs.purchasedProduct.metadata.localizedPrice;
      var lPrice = decimal.ToDouble(price);
      string currencyCode = purchaseEventArgs.purchasedProduct.metadata.isoCurrencyCode;

      var wrapper = Json.Deserialize(purchaseEventArgs.purchasedProduct.receipt) as Dictionary<string, object>; // https://gist.github.com/darktable/1411710
      if (null == wrapper)
      {
        return;
      }

      var payload = (string)wrapper["Payload"]; // For Apple this will be the base64 encoded ASN.1 receipt
      string productId = purchaseEventArgs.purchasedProduct.definition.id;

#if UNITY_ANDROID

      var gpDetails = Json.Deserialize(payload) as Dictionary<string, object>;
      var gpJson = (string)gpDetails["json"];
      var gpSig = (string)gpDetails["signature"];
#endif
    }

    private bool IsUnlocked(IapItemData item) =>
      _gameSaveSystem.Get().UnlockedIapContent.Contains(item.ProductId);

    private string GetPrice(IapItemData item) => 
      GetProduct(item.ProductId)?.metadata.localizedPriceString;

    private ScrollViewIapItemButton GetIapItem(Product product) =>
      _iapItemButtons.FirstOrDefault(item => item.GetId() == product.definition.id && product.definition.type == ProductType.NonConsumable);

    private Product GetProduct(string productId) => 
      _mStoreController?.products.WithID(productId);
  }
}