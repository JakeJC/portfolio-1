using UnityEngine;

namespace _Project.Scripts.Features.IapWindow.Serialized
{
  public enum IapButtonSize
  {
    Small,
    Large
  }
}