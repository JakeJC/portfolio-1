using _Project.Scripts.Features.Data;
using UnityEngine;

namespace _Project.Scripts.Features.IapWindow.Data
{
  [CreateAssetMenu(fileName = "Iap Daily Reward Data", menuName = "IAP/Daily Reward/Data", order = 0)]
  public class IapDailyRewardData : ScriptableObject
  {
    public int Currency;
    public GalleryData Photo;
    public Sprite Icon;
  }
}