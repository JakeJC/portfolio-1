using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.IapWindow.Serialized;
using _Project.Scripts.Features.SecretaryScreen.Data;
using UnityEngine;
using UnityEngine.Purchasing;

namespace _Project.Scripts.Features.IapWindow.Data
{
  [CreateAssetMenu(fileName = "Iap Item", menuName = "ScriptableObjects/Iap/Iap Item Data", order = 0)]
  public class IapItemData : ScriptableObject
  {
    public string ProductId;
    public UnlockIapType UnlockType;
    public IapButtonSize ButtonSize;
    public string Name;
    public string NameAdditional;
    public string Description;
    public string DescriptionAdditional;
    public Sprite DescriptionIcon;
    public Sprite Icon;
    public ProductType ProductType = ProductType.NonConsumable;

    public string AnalyticsId;
    
    public GalleryData[] UnlockGalleryDatas;
    public JewelryItemData[] UnlockJewelryItemDatas;
  }
}