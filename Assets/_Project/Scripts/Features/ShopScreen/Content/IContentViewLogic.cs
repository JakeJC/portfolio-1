namespace _Project.Scripts.Features.ShopScreen.Content
{
  public interface IContentViewLogic
  {
    void Display();
  }
}