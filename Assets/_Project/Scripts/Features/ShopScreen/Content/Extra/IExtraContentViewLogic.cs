using _Project.Scripts.Features.Data;
using _Project.Scripts.Services._Interfaces;

namespace _Project.Scripts.Features.ShopScreen.Content.Extra
{
  public interface IExtraContentViewLogic
  {
    void Construct(IGameSaveSystem gameSaveSystem, IAssetProvider assetProvider);
    ExtraLookData GetCurrentLook();
  }
}