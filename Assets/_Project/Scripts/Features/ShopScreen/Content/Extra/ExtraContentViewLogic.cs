using System.Linq;
using _Project.Scripts.Features.Data;
using _Project.Scripts.Features.ShopScreen.Serialized;
using _Project.Scripts.Services._Interfaces;
using Common;

namespace _Project.Scripts.Features.ShopScreen.Content.Extra
{
  public class ExtraContentViewLogic : IService, IExtraContentViewLogic
  {
    private const string ShopItemsPath = "Features/Data/ShopData/Shop Items";
    private const string ExtraLookItemsPath = "Features/Data/ShopData/Extra Look Items";

    private IGameSaveSystem _gameSaveSystem;
    private IAssetProvider _assetProvider;

    public void Construct(IGameSaveSystem gameSaveSystem, IAssetProvider assetProvider)
    {
      _assetProvider = assetProvider;
      _gameSaveSystem = gameSaveSystem;
    }

    public ExtraLookData GetCurrentLook()
    {
      ExtraLookData[] extraLookData = _assetProvider.GetAllResources<ExtraLookData>(ExtraLookItemsPath);

      UpgradeData[] upgradeData = _assetProvider.GetAllResources<UpgradeData>(ShopItemsPath);
      upgradeData = upgradeData.Where(p => p.ItemType == ShopItemType.Look).ToArray();

      var currentData = upgradeData.FirstOrDefault(p => _gameSaveSystem.Get().ChosenUpgradeContent.Contains(p.Id));

      if (currentData == null)
        currentData = upgradeData.FirstOrDefault();

      return extraLookData.FirstOrDefault(p => p.SkinId == currentData.DisplayData.FirstOrDefault());
    }
  }
}