using Spine;
using Spine.Unity;
using UnityEngine;

namespace _Project.Scripts.Features.ShopScreen.Content
{
  public class SkinCombiner
  {
    private const string SlotName = "microphone";
    
    private SkeletonGraphic _skeletonGraphic;

    public void Construct(SkeletonGraphic skeletonGraphic) => 
      _skeletonGraphic = skeletonGraphic;

    public void ChangeSkin(string skinName, string microphoneName) => 
      CreateCombinedSkin(skinName, microphoneName);

    private void CreateCombinedSkin(string skinName, string microphoneName) 
    {
      if (_skeletonGraphic == null)
      {
        Debug.LogWarning("_skeletonGraphic is null");
        return;
      }
      
      Skeleton skeleton = _skeletonGraphic.Skeleton;

      int slotIndex = skeleton.FindSlot(SlotName).Data.Index;
      Attachment micro = skeleton.Data.FindSkin(microphoneName).GetAttachment(slotIndex, SlotName);

      Skin newSkin = new Skin("Combined Skin");
      newSkin.AddSkin(skeleton.Data.FindSkin(skinName));
      newSkin.SetAttachment(slotIndex, SlotName, micro);
        
      skeleton.SetSkin(newSkin);
      skeleton.SetSlotsToSetupPose();
      _skeletonGraphic.LateUpdate();
    }
  }
}