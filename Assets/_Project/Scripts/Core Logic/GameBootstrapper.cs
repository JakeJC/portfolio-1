using System;
using System.Collections.Generic;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Core_Logic.StatesMachine.States;
using _Project.Scripts.Features.Splashscreen;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Timer.Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Logic;
using AppacheRemote.Scripts.Interfaces;
using FlurrySDK;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Core_Logic
{
  public class GameBootstrapper : MonoBehaviour
  {
    private IAssetProvider _assetProvider;
    private ICurrentScreen _screenManager;
    
    private IAnalytics _analytics;
    private IRemote _remote;
    private IAdsService _adsService;
    private ITimerController _timerController;
    
    private IGameStateMachine _gameStateMachine;
    private SplashLogic _splashLogic;

    private void Awake()
    {
      Application.targetFrameRate = 120;
      LaunchWithSplash(LaunchGameStateMachine);
    }

    [Inject]
    public void Construct(
      IAssetProvider assetProvider,
      IRemote remote, 
      IAdsService adsService,
      IAnalytics analytics,
      ITimerController timerController,
      IGameStateMachine gameStateMachine, 
      ICurrentScreen screenManager)
    {
      _adsService = adsService;
      _remote = remote;
      _assetProvider = assetProvider;
      _analytics = analytics;
      _gameStateMachine = gameStateMachine;
      _timerController = timerController;    
      
      _screenManager = screenManager;
      _screenManager.SetCurrentScreen(Screens.ScreenNames.Splash);
      
      _timerController.LaunchService();
    }

    private void LaunchWithSplash(Action initializeGame)
    {
      _splashLogic = new SplashLogic(_assetProvider);
      _splashLogic.Launch(initializeGame);

      _remote.OnFetch += InitializeServices;
      _remote.Initialize();
    }

    private void InitializeServices() => 
      InitializeRemote();

    private void InitializeRemote()
    {
      _remote = new AppacheRemote.Remote();
      _remote.OnFetch += () =>
      {
        InitializeAnalytics();
        InitializeCrashlytics();
      };
      _remote.Initialize();
    }

    private void InitializeAnalytics()
    {
      _analytics.OnInitialize += InitializeAds;
      _analytics.Initialize();
    }

    private void InitializeAds()
    {
      SendRemoteEvents();
      
      _adsService.Initialize(_remote.GetPlacementType(), () =>
      {
        _adsService.Banner.OnBannerLoad += () => { _adsService.Banner.HideBanner(); };
        _adsService.Banner.HideBanner();
        
        _splashLogic.Complete();
      });
    }

    private void SendRemoteEvents()
    {
      Flurry.UserProperties.Add("AB_remote_config", _remote.GetPlacementType().ToString());

      _analytics.Send("AB_remote_config", new Dictionary<string, object>()
      {
        {"group", _remote.GetPlacementType()}
      }, true);

      if (_analytics.IsFirstLaunch)
        _analytics.Send($"v{Application.version.Replace(".", "_")}", true);
    }

    private void InitializeCrashlytics()
    {
      Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
      {
        var dependencyStatus = task.Result;
        if (dependencyStatus == Firebase.DependencyStatus.Available)
        {
          Firebase.FirebaseApp app = Firebase.FirebaseApp.DefaultInstance;
        }
        else
          Debug.LogError($"Could not resolve all Firebase dependencies: {dependencyStatus}");
      });
    }

    private void LaunchGameStateMachine()
    {
      if (_remote.ShouldEnableFeatureTest())
      {
        _gameStateMachine.Enter<LoadGameServicesState>();
        return;
      }

      _adsService.CurrentInterstitial.ShowInterstitial(Screens.ScreenNames.Home, 
        _gameStateMachine.Enter<LoadGameServicesState>, 
        _gameStateMachine.Enter<LoadGameServicesState>, true);
    }
  }
}