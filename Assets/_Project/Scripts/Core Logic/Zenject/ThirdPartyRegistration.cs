using System;
using AppacheAds;
using AppacheAds.Scripts.Interfaces;
using AppacheAds.Scripts.Logger;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Logic;
using AppacheRemote;
using AppacheRemote.Scripts.Interfaces;
using Zenject;

namespace _Project.Scripts.Core_Logic.Zenject
{
  public class ThirdPartyRegistration : MonoInstaller
  {
    public override void InstallBindings()
    {
      Container.Bind<ISessionManager>().To<SessionManager>().AsSingle();
      Container.Bind<ICurrentScreen>().To<ScreenManager>().AsSingle();
      Container.Bind<IAdsLogger>().To<AdsLogger>().AsSingle();

#if UNITY_EDITOR
      Container.Bind<IRemote>().To<RemoteDummy>().AsSingle();   
#else
      Container.Bind<IRemote>().To<Remote>().AsSingle();
#endif
      Container.Bind<IAnalytics>().To<AppacheAnalytics.Analytics>().AsSingle();
#if TEST_DIALOG
      Container.Bind<IAdsService>().To<AdsDummy>().AsSingle();
#else
      Container.Bind<IAdsService>().To<AdsService>().AsSingle();
#endif
    }
  }
}