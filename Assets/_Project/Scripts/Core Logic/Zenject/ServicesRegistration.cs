using _Modules._InApps.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.AssetProvider;
using _Project.Scripts.Services.Money;
using _Project.Scripts.Services.Unlocker;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.GameMetaProvider;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.SaveSystem;
using _Project.Scripts.Services.Timer;
using _Project.Scripts.Services.Timer.Interfaces;
using _Project.Scripts.Timer;
using _Project.Scripts.Timer.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Logic;
using Zenject;

namespace _Project.Scripts.Core_Logic.Zenject
{
  public class ServicesRegistration : MonoInstaller
  {
    public override void InstallBindings()
    {      
      BindSingleService<IAssetProvider, AssetProvider>();
      BindSingleService<IAudioService, AudioService>();
      BindSingleService<ILocaleSystem, LocaleSystem>();
      BindSingleService<IMoneyService, MoneyService>();
      BindSingleService<IUnlockService, UnlockService>();
      BindSingleService<IGameSaveSystem, GameSaveSystem>();      
      BindSingleService<ITimerController, TimerController>();
      BindSingleService<IGameMetaProvider, GameMetaProvider>();
    }

    private void BindSingleService<T, T2>() where T2 : T => 
      Container.Bind<T>().To<T2>().AsSingle();
  }
}