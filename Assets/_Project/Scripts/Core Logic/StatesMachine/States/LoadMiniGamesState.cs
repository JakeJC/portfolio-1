using System.Threading.Tasks;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Features.MiniGamesScreen;
using _Project.Scripts.Features.StoriesScreen;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Core_Logic.StatesMachine.States
{
  public class LoadMiniGamesState : IState
  {
    private readonly IRemote _remote;
    private readonly IAnalytics _analytics;
    private readonly IAdsService _adsService;
    private readonly IMoneyService _moneyService;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;
    private readonly IUnlockService _unlockService;
    private readonly ICurrentScreen _currentScreen;
    private readonly IAssetProvider _assetProvider;
    private readonly IGameSaveSystem _gameSaveSystem;

    private IGameStateMachine _gameStateMachine;

    private MiniGamesLogic _miniGamesLogic;
    
    private LoadMiniGamesState(IAssetProvider assetProvider, ICurrentScreen currentScreen, IGameSaveSystem gameSaveSystem,
      IAdsService adsService, IAnalytics analytics, ILocaleSystem localeSystem, IAudioService audioService, IRemote remote,
      IUnlockService unlockService, IMoneyService moneyService)
    {
      _remote = remote;
      _analytics = analytics;
      _adsService = adsService;
      _moneyService = moneyService;
      _localeSystem = localeSystem;
      _audioService = audioService;
      _unlockService = unlockService;
      _assetProvider = assetProvider;
      _currentScreen = currentScreen;
      _gameSaveSystem = gameSaveSystem;
    }
    
    public void Construct(IGameStateMachine gameStateMachine) =>
      _gameStateMachine = gameStateMachine;

    public void Enter()
    {
      _miniGamesLogic = new MiniGamesLogic(_gameStateMachine, _assetProvider, _currentScreen, _gameSaveSystem, _adsService, 
        _analytics, _localeSystem, _audioService, _remote, _unlockService, _moneyService);
      _miniGamesLogic.CreateMiniGamesView();
    }

    public void Exit() => 
      _miniGamesLogic.ClearMiniGamesView();

    public Task AsyncEnter() => 
      throw new System.NotImplementedException();
  }
}