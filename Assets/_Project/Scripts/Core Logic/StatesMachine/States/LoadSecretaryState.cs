using System;
using System.Threading.Tasks;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Features.SecretaryScreen;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.Timer.Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;

namespace _Project.Scripts.Core_Logic.StatesMachine.States
{
  public class LoadSecretaryState : IState
  {
    private readonly IRemote _remote;
    private readonly IAnalytics _analytics;
    private readonly IAdsService _adsService;
    private readonly IMoneyService _moneyService;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;
    private readonly IUnlockService _unlockService;
    private readonly ICurrentScreen _currentScreen;
    private readonly IAssetProvider _assetProvider;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly ITimerController _timerController;

    private IGameStateMachine _gameStateMachine;
    
    private SecretaryLogic _secretaryLogic;

    private LoadSecretaryState(IAssetProvider assetProvider, ICurrentScreen currentScreen, IGameSaveSystem gameSaveSystem,
      IAdsService adsService, IAnalytics analytics, ILocaleSystem localeSystem, IAudioService audioService, IRemote remote,
      IUnlockService unlockService, IMoneyService moneyService, ITimerController timerController)
    {
      _remote = remote;
      _analytics = analytics;
      _adsService = adsService;
      _moneyService = moneyService;
      _localeSystem = localeSystem;
      _audioService = audioService;
      _unlockService = unlockService;
      _assetProvider = assetProvider;
      _currentScreen = currentScreen;
      _gameSaveSystem = gameSaveSystem;
      _timerController = timerController;
    }

    public void Construct(IGameStateMachine gameStateMachine) =>
      _gameStateMachine = gameStateMachine;

    public void Enter()
    {
      _secretaryLogic = new SecretaryLogic(_gameStateMachine, _assetProvider, _currentScreen, _gameSaveSystem, 
        _adsService, _analytics, _localeSystem, _audioService, _remote, _unlockService, _moneyService, _timerController);
      _secretaryLogic.CreateSecretaryView();
    }

    public void Exit() =>
      _secretaryLogic.ClearSecretaryView();

    public Task AsyncEnter() =>
      throw new NotImplementedException();
  }
}