namespace _Project.Scripts.Core_Logic.Screens
{
  public static class ScreenNames
  {
    public const string Splash = "splash";
    public const string Privacy = "privacy";
    public const string Home = "home";
    public const string Secretary = "secretary";
    public const string Shop = "shop";
    public const string MiniGames = "games";
    public const string Gallery = "gallery";
    public const string Stories = "stories";
    public const string Result = "result";
    public const string Customization = "customization";
  }
}