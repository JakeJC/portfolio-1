﻿using UnityEngine;

namespace _Project.Scripts.Core_Logic
{
  public static class ColorExtensions
  {
    public static Color SetA(this Color color, float a) =>
      new Color(color.r, color.g, color.b, a);
  }
}