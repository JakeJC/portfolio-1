using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Animation
{
  public class BarFollower : MonoBehaviour
  {  
    public RectTransform RectTransform;
    public RectTransform BarTransform;
    
    public Image Fill;

    void Update() =>
      SetPosition();

    private void SetPosition()
    {
      Vector2 bottom = new Vector2(RectTransform.anchoredPosition.x, -BarTransform.rect.height/2);
      Vector2 top = new Vector2(RectTransform.anchoredPosition.x, BarTransform.rect.height/2);

      Vector2 target = Vector2.Lerp(bottom, top, Fill.fillAmount);
      RectTransform.anchoredPosition = target;
    }
  }
}