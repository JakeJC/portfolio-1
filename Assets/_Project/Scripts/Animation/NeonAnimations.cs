﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Animation
{
  public static class NeonAnimations
  {
    public static System.Func<Image, Tween> ImageBlinkingRandom()
    {
      var variants = new System.Func<Image, Tween>[]
      {
        target => target.DOFade(0f, 0.07f).SetLoops(2, LoopType.Yoyo),
        target => target.DOFade(0f, 0.08f).SetLoops(6, LoopType.Yoyo),
        target => target.DOFade(0f, 0.06f).SetLoops(8, LoopType.Yoyo).SetEase(Ease.Linear),
      };
      var randomIndex = UnityEngine.Random.Range(0, variants.Length);
      var randomVariant = variants[randomIndex];
      return randomVariant;
    }

    public static System.Func<CanvasGroup, Tween> CanvasGroupBlinkingRandom()
    {
      var variants = new System.Func<CanvasGroup, Tween>[]
      {
        target => target.DOFade(0f, 0.07f).SetLoops(2, LoopType.Yoyo),
        target => target.DOFade(0f, 0.08f).SetLoops(6, LoopType.Yoyo),
        target => target.DOFade(0f, 0.06f).SetLoops(8, LoopType.Yoyo).SetEase(Ease.Linear),
      };
      var randomIndex = UnityEngine.Random.Range(0, variants.Length);
      var randomVariant = variants[randomIndex];
      return randomVariant;
    }

    public static System.Func<Image, Tween> ImageFadingRandom()
    {
      var variants = new System.Func<Image, Tween>[]
      {
        target => target.DOFade(0f, 0.08f).SetLoops(5, LoopType.Yoyo),
        target => target.DOFade(0f, 0.07f).SetLoops(7, LoopType.Yoyo).SetEase(Ease.Linear),
      };
      var randomIndex = Random.Range(0, variants.Length);
      var randomVariant = variants[randomIndex];
      return randomVariant;
    }
  }
}