#if UNITY_EDITOR
using Sirenix.OdinInspector;
using TMPro;
using UnityEditor;
using UnityEngine;

namespace _Project.Fonts
{
  public class CreateFont : MonoBehaviour
  {
    public Font font;

    public string fontAssetPath = "Assets/Font.asset";
    
    [Button]
    public void Create()
    {
      var fontAsset = TMP_FontAsset.CreateFontAsset(font);
      
      fontAsset.atlasPopulationMode = AtlasPopulationMode.Static;
      fontAsset.material.name = "Font " + " Material";
      fontAsset.atlasTexture.name = "Font " + " Atlas";
             
      AssetDatabase.DeleteAsset(fontAssetPath);
      AssetDatabase.CreateAsset(fontAsset, fontAssetPath);
      AssetDatabase.AddObjectToAsset(fontAsset.material, fontAsset);
      AssetDatabase.AddObjectToAsset(fontAsset.atlasTexture, fontAsset);
    }
  }
}
#endif