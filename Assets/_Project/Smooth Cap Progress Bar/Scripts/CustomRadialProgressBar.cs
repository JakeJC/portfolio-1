﻿using UnityEngine;

namespace _Project.Smooth_Cap_Progress_Bar.Scripts
{
  public class CustomRadialProgressBar : CustomProgressBar
  {
    public RectTransform FillStart;
    public RectTransform FillStartContainer;

    [Range(0, 1)] public float startValue = 0.001f;
    public CanvasGroup[] MasksCanvasGroups;

    protected override void Update()
    {
      base.Update();

      foreach (CanvasGroup masksCanvasGroup in MasksCanvasGroups) 
        masksCanvasGroup.alpha = Fill.fillAmount < startValue ? 0 : 1;
    }

    protected override void FillProgressBar(float value)
    {
      float fillAmount = value / 100.0f;
      Fill.fillAmount = fillAmount;
      
      float angle = fillAmount * 360;
      FillStartContainer.localEulerAngles = new Vector3(0, 0, -angle);
      FillStart.localEulerAngles = new Vector3(0, 0, angle);
    }
  }
}