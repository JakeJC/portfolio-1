using UnityEngine;
using UnityEngine.UI;

namespace _Project.Smooth_Cap_Progress_Bar.Scripts
{
  public abstract class CustomProgressBar : MonoBehaviour
  {
    [Range(0, 100)] public float FillValue;
    public Image Fill;
    protected abstract void FillProgressBar(float value);
    
    protected virtual void Update() => 
      FillProgressBar(FillValue);
  }
}