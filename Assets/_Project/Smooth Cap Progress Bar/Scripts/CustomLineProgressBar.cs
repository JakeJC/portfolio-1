using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Smooth_Cap_Progress_Bar.Scripts
{
  public class CustomLineProgressBar : CustomProgressBar
  {
    public CanvasGroup ProgressBarGroup;
    public RectTransform FillRect;
    public RectTransform FillMaskedRect;
    public RectTransform Mask;

    [MinMaxSlider(0, 100)] 
    public Vector2 MinMaxFillForMask;
    
    protected override void FillProgressBar(float value)
    {
      ProgressBarGroup.alpha = value < MinMaxFillForMask.x ? 0 : 1;
      
      float fillAmount = value / 100.0f;
      Fill.fillAmount = fillAmount;

      Vector3 maskPosition = Mask.anchoredPosition;
      maskPosition.x = FillRect.rect.width * (Mathf.Clamp(value, MinMaxFillForMask.x, MinMaxFillForMask.y) / 100);
      Mask.anchoredPosition = maskPosition;
      
      FillMaskedRect.position = FillRect.position;
    }
  }
}