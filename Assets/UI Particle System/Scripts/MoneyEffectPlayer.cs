using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace UI_Particle_System.Scripts
{
  public class MoneyEffectPlayer : EffectPlayer
  {
    public GameObject MoneyPrefab;

    [Range(0, 50)] public int MoneyAmount = 10;
    [Range(0, 10)] public float CleanupDelay = 1.5f;
    [Range(0, 10)] public float SpawnDelayMin = 0.1f;
    [Range(0, 10)] public float SpawnDelayMax = 0.7f;

    public Image Vignette;
    [Range(0, 10)] public float VignetteDuration;

    public override void Init(Action action = null)
    {
      base.Init(action);

      SetVignetteDefault();
      Vignette.DOFade(1, VignetteDuration).OnComplete(delegate { Vignette.DOFade(0, VignetteDuration); });

      StartCoroutine(Effect());
    }

    IEnumerator Effect()
    {
      for (int i = 0; i < MoneyAmount; i++)
      {
        yield return new WaitForSeconds(Random.Range(SpawnDelayMin, SpawnDelayMax));
        GameObject money = Instantiate(MoneyPrefab, EffectParent.transform);

        money.SetActive(true);
      }

      yield return new WaitForSeconds(CleanupDelay);
      Cleanup();
    }

    private void SetVignetteDefault()
    {
      Color defaultColor = Vignette.color;
      defaultColor.a = 0;
      Vignette.color = defaultColor;
    }
  }
}