using System;
using UnityEngine;

namespace UI_Particle_System.Scripts
{
  public abstract class EffectPlayer : MonoBehaviour
  {
    public GameObject EffectParent;
    [Range(0, 10)] public float ActionDelay = 1;
    private Action _action;

    public virtual void Init(Action action = null)
    {
      EffectParent.SetActive(true);
      _action = action;
      
      Invoke(nameof(InvokeAction), ActionDelay);
    }

    public void Cleanup() => 
      DestroyImmediate(gameObject);

    private void InvokeAction() => 
      _action?.Invoke();
  }
}