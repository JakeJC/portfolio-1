using System;
using PathCreation;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace UI_Particle_System.Scripts
{
  public class UIPathFollower : MonoBehaviour
  {
    public PathCreator pathCreator;
    public EndOfPathInstruction endOfPathInstruction;
    public float MaxSpeed = 100;
    public Image Image;
    public AnimationCurve AlphaCurve;
    public AnimationCurve SizeCurve;
    public AnimationCurve SpeedCurve;
    public AnimationCurve RotationCurve;
    public float MaxImageRot = 100;
    public float ImageOffset = 0.1f;
    private float _distanceTravelled;


    private void Awake()
    {
      Vector2 localPos = Image.transform.localPosition;
      Image.transform.localPosition = localPos + Vector2.one * Random.Range(-ImageOffset, ImageOffset);
    }

    private void Start()
    {
      if (pathCreator != null)
        pathCreator.pathUpdated += OnPathChanged;
    }

    private void Update()
    {
      if (pathCreator != null)
      {
        float progress = pathCreator.path.GetClosestTimeOnPath(transform.position);

        _distanceTravelled += SpeedCurve.Evaluate(progress) * MaxSpeed * Time.deltaTime;

        Color imgColor = Image.color;
        imgColor.a = AlphaCurve.Evaluate(progress);
        Image.color = imgColor;

        Image.transform.localEulerAngles = new Vector3(0, MaxImageRot * RotationCurve.Evaluate(progress), 0);

        transform.localScale = SizeCurve.Evaluate(progress) * Vector3.one;

        Vector3 dest = pathCreator.path.GetPointAtDistance(_distanceTravelled, endOfPathInstruction);
        transform.right = dest - transform.position;
        transform.position = dest;
      }
    }

    private void OnPathChanged() =>
      _distanceTravelled = pathCreator.path.GetClosestDistanceAlongPath(transform.position);
  }
}