using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI_Particle_System.Scripts
{
  public class OpenEffectPlayer : EffectPlayer 
  {
    public Image Vignette;
    [Range(0, 10)] public float VignetteDuration;
    
    [Range(0, 10)] public float CleanupDelay = 1.5f;

    public override void Init(Action action = null)
    {
      base.Init(action);
      
      SetVignetteDefault();
      Vignette.DOFade(1, VignetteDuration).OnComplete(delegate
      {
        Vignette.DOFade(0, VignetteDuration);
      });
      
      Invoke(nameof(Cleanup), CleanupDelay);
    }

    private void SetVignetteDefault()
    {
      Color defaultColor = Vignette.color;
      defaultColor.a = 0;
      Vignette.color = defaultColor;
    }
  }
}