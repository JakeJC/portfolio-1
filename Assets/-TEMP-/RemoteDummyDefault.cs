using System;
using System.Threading.Tasks;
using AppacheRemote.Scripts.Data;
using AppacheRemote.Scripts.Interfaces;

public class RemoteDummyDefault : IRemote
{
  public event Action OnFetch;

  public bool ShouldEnableFeatureTest() =>
    false;

  public bool ShouldEnableFeatureTest2() =>
    false;

  public Task Initialize()
  {
    OnFetch?.Invoke();
    return Task.CompletedTask;
  }

  public PlacementType GetPlacementType() =>
    PlacementType.@default;

  public string GetPrivacyText() => 
    string.Empty;
}
